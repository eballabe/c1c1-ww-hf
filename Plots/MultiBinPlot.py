#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
import os, string, pickle, copy
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess
import argparse
from math import sqrt

#no list accepted
parser = argparse.ArgumentParser(description='Setting plot parameters')

parser.add_argument('-v','--variable',  default='lep1pT',                   action='store', type=str, help='variable')
parser.add_argument('-r','--region',    default='CR_Dib_DM100',             action='store', type=str, help='region')
parser.add_argument('-w','--workspace', default='Plots/VR_reversedMETsig/VR_DF0J_reversedMETsig/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root', action='store', type=str, help='workspace_name')
parser.add_argument('-o','--outputDir', default='Plots/VR_reversedMETsig/VR_DF0J_reversedMETsig/MultiBinPlots', action='store', type=str, help='outputDir')
parser.add_argument('-b','--doBlind',  default=False, action='store_true',   help='doBlind')
parser.add_argument('--doPreselection',  default=False, action='store_true',   help='doPreselection')
parser.add_argument('--doCRVR',  default=False, action='store_true',   help='doCRVR')
parser.add_argument('--doSRDF',  default=False, action='store_true',   help='doSRDF')
parser.add_argument('--doSRSF',  default=False, action='store_true',   help='doSRSF')
parser.add_argument('--doVRSRDF',  default=False, action='store_true',   help='doVRSRDF')
parser.add_argument('--doVRSRSF',  default=False, action='store_true',   help='doVRSRSF')
parser.add_argument('--doBeforeFit',  default=False, action='store_true',   help='doBeforeFit')
parser.add_argument('--doAfterFit',  default=False, action='store_true',   help='doAfterFit')
parser.add_argument('--addSignal',  default=False, action='store_true',   help='addSignal')
parser.add_argument('-s','--scale',    default='log',             action='store', type=str, help='scale: log or linear')


args = parser.parse_args()

variable = args.variable
region = args.region
wsfilename = args.workspace
outputDir = args.outputDir
doBlind = args.doBlind
doPreselection = args.doPreselection
doCRVR = args.doCRVR
doSRDF = args.doSRDF
doSRSF = args.doSRSF
doVRSRDF = args.doVRSRDF
doVRSRSF = args.doVRSRSF
doBeforeFit = args.doBeforeFit
doAfterFit = args.doAfterFit
addSignal = args.addSignal
scale = args.scale

doBlind = False

doSignificance = False
doRatioWtTtbar = False

cutLeft = False
cutRight = True

PlotsForPaper = True

Signal_cuts={
"DF0J_preselection":"isSF==0 && njet==0 && nbjet==0",
 "SF0J_preselection":"isSF==1 && njet==0 && nbjet==0",
"CR_Dib":"njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && BDTDeltaM100_90 > 0.2 && BDTDeltaM100_90 <= 0.65 &&  BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1 && ( ( isSF == 0) ||  ( isSF == 1 && BDTothersDeltaM100_90 < 0.01 )   ) ",
"CR_Top":"njet==1 && nbjet==1 && METsig > 8 && MT2 > 50 && ( (isSF==0 && BDTDeltaM100_90 > 0.5 && BDTDeltaM100_90 <= 0.7) || (isSF==1 && BDTDeltaM100_90 > 0.7 && BDTDeltaM100_90 <= 0.75 && BDTothersDeltaM100_90 < 0.01) )",

"VR_Dib_DF0J":"njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && isSF == 0 && BDTDeltaM100_90 > 0.65 && BDTDeltaM100_90 <= 0.81 && BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1",
"VR_Dib_SF0J":"njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && isSF == 1 && BDTDeltaM100_90 > 0.65 && BDTDeltaM100_90 <= 0.77 && BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1 && BDTothersDeltaM100_90 < 0.01",
"VR_Top_DF1J":"njet==1 && nbjet==1 && METsig > 8 && MT2 > 50 && isSF== 0 && BDTDeltaM100_90 > 0.7  && BDTDeltaM100_90 <= 1.0",
"VR_Top_SF1J":"njet==1 && nbjet==1 && METsig > 8 && MT2 > 50 && isSF== 1 && BDTDeltaM100_90 > 0.75 && BDTDeltaM100_90 <= 1.0 && BDTothersDeltaM100_90 < 0.01",
"VR_Top_DF0J":"njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && isSF== 0 && BDTDeltaM100_90 > 0.5 && BDTDeltaM100_90 <= 0.81 && BDTVVDeltaM100_90 <= 0.15",
"VR_Top_SF0J":"njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && isSF== 1 && BDTDeltaM100_90 > 0.5 && BDTDeltaM100_90 <= 0.77 && BDTVVDeltaM100_90 <= 0.15 && BDTothersDeltaM100_90 < 0.01",

"SRD_DF0J_81":"isSF==0 & njet==0 & nbjet==0 & METsig > 8 & MT2 > 50 & BDTDeltaM100_90 > 0.81",
"SRD_SF0J_77":"isSF==1 & njet==0 & nbjet==0 & METsig > 8 & MT2 > 50 & BDTDeltaM100_90 > 0.77 & BDTothersDeltaM100_90 <= 0.01",

"VR_Dib_DF0J_SR_DF0J":"njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && ( (isSF == 0 && BDTDeltaM100_90 > 0.65 && BDTDeltaM100_90 <= 0.81 && BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1)  || (isSF == 0 && BDTDeltaM100_90 > 0.81) )",
"VR_Dib_SF0J_SR_SF0J":"njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && ( (isSF == 1 && BDTDeltaM100_90 > 0.65 && BDTDeltaM100_90 <= 0.77 && BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1 && BDTothersDeltaM100_90 < 0.01) || (isSF == 1 && BDTDeltaM100_90 > 0.77 && BDTothersDeltaM100_90 < 0.01) )"
                     }

if addSignal == True:
    Signal_Tree_File = '/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/no_syst/C1C1.root'
    Signal_cut = Signal_cuts[region]
    Signal100_10_Tree_Name = 'C1C1_100_10'
    Signal125_25_Tree_Name = 'C1C1_125_25'
    Signal150_50_Tree_Name = 'C1C1_150_50'




if doPreselection == True:
# For preselction plots
    Plots = {  'MET'   : {'PlotNBins' : 10, 'PlotBinLow' : 0,  'PlotBinHigh' : 250,  'titleX' : 'E_{T}^{miss} [GeV]'},
           'METsig'   : {'PlotNBins' : 20, 'PlotBinLow' : 5,  'PlotBinHigh' : 25,   'titleX' : 'E_{T}^{miss} significance'},
           'lep1pT'   : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 200., 'titleX' : 'Leading lepton p_{T} [GeV]'},
           'lep2pT'   : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 200., 'titleX' : 'Subleading lepton p_{T} [GeV]'},
           'njet'        : {'PlotNBins' : 3,  'PlotBinLow' : 0., 'PlotBinHigh' : 3.,   'titleX' : 'Number of jets'},
           'nbjet'      : {'PlotNBins' : 3,  'PlotBinLow' : 0., 'PlotBinHigh' : 3.,   'titleX' : 'Number of b-jets'},
           'mll'         : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 200,  'titleX' : 'm(#font[12]{ll}) [GeV]'},
           'MT2'       : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 250., 'titleX' : 'm_{T2} [GeV]'},
           'cosTstar' : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,   'titleX' : '|cos \\theta*_{#font[12]{ll}}|'},
           'DPhib'    : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 3.1416,'titleX' : '|\\Delta\\phi_{boost}|'},
           'dPhiMETL1'    : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 3.1416,'titleX' : '\\Delta\\phi(#font[12]{l_{1}},E_{T}^{miss})'},
           'dPhiMETL2'    : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 3.1416,'titleX' : '\\Delta\\phi(#font[12]{l_{2}},E_{T}^{miss})'},
           'BDTDeltaM100_90'      : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1., 'titleX' : 'BDT-signal'},
           'BDTVVDeltaM100_90'    : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1., 'titleX' : 'BDT-VV'},
           'BDTtopDeltaM100_90'   : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 0.5, 'titleX' : 'BDT-top'},
           'BDTothersDeltaM100_90': {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 0.1, 'titleX' : 'BDT-others'},
           'BDTDeltaM30'          : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1., 'titleX' : 'BDT-signal'},
           'BDTVVDeltaM30'        : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1., 'titleX' : 'BDT-VV'},
           'BDTtopDeltaM30'       : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 1., 'titleX' : 'BDT-top'},
           'BDTothersDeltaM30'    : {'PlotNBins' : 50, 'PlotBinLow' : 0., 'PlotBinHigh' : 1., 'titleX' : 'BDT-others'},
        }
elif doCRVR == True:
# dictionary for plotting cosmetics 
    Plots = {   'MET'                   : {'PlotNBins' : 30, 'PlotBinLow' : 0,  'PlotBinHigh' : 300,    'titleX' : 'E_{T}^{miss} [GeV]'},
            'METsig'                : {'PlotNBins' : 17, 'PlotBinLow' : 8,  'PlotBinHigh' : 25,     'titleX' : 'E_{T}^{miss} significance'},
            'lep1pT'                : {'PlotNBins' : 30, 'PlotBinLow' : 0., 'PlotBinHigh' : 200.,   'titleX' : 'Leading lepton p_{T} [GeV]'},
            'lep2pT'                : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 200.,   'titleX' : 'Subleading lepton p_{T} [GeV]'},
            'njet'                  : {'PlotNBins' : 3,  'PlotBinLow' : 0., 'PlotBinHigh' : 3.,     'titleX' : 'Number of jets'},
            'nbjet'                 : {'PlotNBins' : 3,  'PlotBinLow' : 0., 'PlotBinHigh' : 3.,     'titleX' : 'Number of b-jets'},
            'mll'                   : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 300,    'titleX' : 'm(#font[12]{ll}) [GeV]'},
            'MT2'                   : {'PlotNBins' : 8, 'PlotBinLow' : 50., 'PlotBinHigh' : 250.,   'titleX' : 'm_{T2} [GeV]'},
            'cosTstar'              : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : '|cos \\theta_{#font[12]{ll}}*|'},
            'DPhib'                 : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 3.1416, 'titleX' : '\\Delta\\phi_{boost}'},
            'BDTDeltaM100_90'       : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : 'BDT-signal'},
            'BDTVVDeltaM100_90'     : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : 'BDT-VV'},
            'BDTtopDeltaM100_90'    : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 0.5,     'titleX' : 'BDT-top'},
            'BDTothersDeltaM100_90' : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 0.1,     'titleX' : 'BDT-others'},
            'BDTDeltaM30'           : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : 'BDT-signal'},
            'BDTVVDeltaM30'         : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : 'BDT-VV'},
            'BDTtopDeltaM30'        : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : 'BDT-top'},
            'BDTothersDeltaM30'     : {'PlotNBins' : 50, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : 'BDT-others'},
            }
    if region == 'CR_Dib':
        Plots['BDTVVDeltaM100_90']['PlotNBins'] = 12
        Plots['BDTVVDeltaM100_90']['PlotBinLow'] = 0.2
        Plots['BDTVVDeltaM100_90']['PlotBinHigh'] = 0.8
    if region == 'CR_Top':
        Plots['BDTtopDeltaM100_90']['PlotNBins'] = 7
        Plots['BDTtopDeltaM100_90']['PlotBinLow'] = 0.
        Plots['BDTtopDeltaM100_90']['PlotBinHigh'] = 0.35
    if region == 'VR_Dib_DF0J':
        Plots['BDTVVDeltaM100_90']['PlotNBins'] = 5
        Plots['BDTVVDeltaM100_90']['PlotBinLow'] = 0.2
        Plots['BDTVVDeltaM100_90']['PlotBinHigh'] = 0.3
    if region == 'VR_Top_DF1J':
        Plots['BDTtopDeltaM100_90']['PlotNBins'] = 5
        Plots['BDTtopDeltaM100_90']['PlotBinLow'] = 0.
        Plots['BDTtopDeltaM100_90']['PlotBinHigh'] = 0.2
    if region == 'VR_Top_SF1J':
        Plots['cosTstar']['PlotNBins'] = 6
        Plots['cosTstar']['PlotBinLow'] = 0.
        Plots['cosTstar']['PlotBinHigh'] = 0.6
    if region == 'VR_Top_SF0J':
        Plots['BDTtopDeltaM100_90']['PlotNBins'] = 5
        Plots['BDTtopDeltaM100_90']['PlotBinLow'] = 0.05
        Plots['BDTtopDeltaM100_90']['PlotBinHigh'] = 0.3

elif  doSRDF == True:
    Plots = { 
            'BDTDeltaM100_90'       : {'PlotNBins' : 10, 'PlotBinLow' : 0.81, 'PlotBinHigh' : 0.86,     'titleX' : 'BDT-signal'},
            }
elif  doSRSF == True:
    Plots = { 
            'BDTDeltaM100_90'       : {'PlotNBins' : 8, 'PlotBinLow' : 0.77, 'PlotBinHigh' : 0.81,     'titleX' : 'BDT-signal'},
            }

elif  doVRSRDF == True:
    Plots = { 
            'BDTDeltaM100_90'       : {'PlotNBins' : 22, 'PlotBinLow' : 0.65, 'PlotBinHigh' : 0.87,     'titleX' : 'BDT-signal'},
            'MT2'                   : {'PlotNBins' : 9, 'PlotBinLow' : 50., 'PlotBinHigh' : 140.,   'titleX' : 'm_{T2} [GeV]'},
            }
elif  doVRSRSF == True:
    Plots = { 
            'BDTDeltaM100_90'       : {'PlotNBins' : 17, 'PlotBinLow' : 0.65, 'PlotBinHigh' : 0.82,     'titleX' : 'BDT-signal'},
            'MT2'                   : {'PlotNBins' : 7, 'PlotBinLow' : 50., 'PlotBinHigh' : 120.,   'titleX' : 'm_{T2} [GeV]'},
            }
else:
    raise Exception("No Plots dictionary selected - BREAK")
            

def main():

  # Where's the pickle file?
    pickleFilename = wsfilename.replace( "BkgOnly_combined_BasicMeasurement_model_afterFit.root", "MyYieldsTable_{}.pickle".format(variable)   )

    # Used as plot title
    region = ""

    # Samples to stack on top of eachother in each region
    samples = "VV,ttbar,Wt,FNP,Zjets,Zttjets,VVV,other"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    if not os.path.exists(pickleFilename):
        print ("pickle filename %s does not exist" % pickleFilename)
        print ("will proceed to run yieldstable again")
        
        # Run YieldsTable.py with all regions and samples requested, -P command is MANDATORY for multibin plot
        cmd = "YieldsTable.py -b -c %s -s %s -w %s -P -o %s" % (",".join(regionList), samples, wsfilename, pickleFilename.replace(".pickle",".tex"))
        print (cmd)
        subprocess.call(cmd, shell=True)

    if not os.path.exists(pickleFilename):
        print ("pickle filename %s still does not exist" % pickleFilename)
        return
    
    regionList = []
    NBins = Plots[variable]['PlotNBins']
    for bin in range(NBins):
        regionList.append("{}_{}_bin{}".format(region,variable,bin))


    # Open the pickle and make the multibin plot
    MymakePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)


# Build a dictionary that remaps region names
def renameRegions():

    myRegionDict = {}


    NBins = Plots[variable]['PlotNBins']
    for bin in range(NBins):
        myRegionDict["{}_{}_bin{}".format(region,variable,bin)] = "{}".format(bin)


    # Remap region names using the old name as index, e.g.:
    myRegionDict[region] = region
    myRegionDict["CR_Dib"] = "CR-VV"
    myRegionDict["CR_Top"] = "CR-top"

    myRegionDict["VR_Dib_DF0J"] = "VR_VV_DF0J"
    myRegionDict["VR_Dib_SF0J"] = "VR_VV_SF0J"
    myRegionDict["VR_Top_DF1J"] = "VR_Top_DF1J"
    myRegionDict["VR_Top_SF1J"] = "VR_Top_SF1J"
    myRegionDict["VR_Top_DF0J"] = "VR_Top_DF0J"
    myRegionDict["VR_Top_SF0J"] = "VR_Top_SF0J"

    myRegionDict["VR_DF0J_reversedMETsig"] = "VR_DF0J_reversedMETsig"

    myRegionDict["SRD_DF0J_81"] = "SR-DF"
    myRegionDict["SRD_SF0J_77"] = "SR-SF"

    myRegionDict["ttbar"] = "t#bar{t}"
    myRegionDict["Zjets"] = "Z(ee/#mu#mu)+jets"
    myRegionDict["Zttjets"] = "Z(#tau#tau)+jets"

    if PlotsForPaper == True:
        myRegionDict["VR_Dib_DF0J"] = "VR-VV-DF"
        myRegionDict["VR_Dib_SF0J"] = "VR-VV-SF"
        myRegionDict["VR_Top_DF1J"] = "VR-top-DF"
        myRegionDict["VR_Top_SF1J"] = "VR-top-SF"
        myRegionDict["VR_Top_DF0J"] = "VR-top0J-DF"
        myRegionDict["VR_Top_SF0J"] = "VR-top0J-SF"

        myRegionDict["VR_Dib_DF0J_SR_DF0J"] = "VR-VV-DF + SR-DF"
        myRegionDict["VR_Dib_SF0J_SR_SF0J"] = "VR-VV-SF + SR-SF"
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]
    
    regionList = ["{}_{}".format(region,variable)] # here bins are not specified

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("SLWR") != -1: return kBlue+3
    if name.find("SLTR") != -1: return kBlue+3
    if name.find("SR") != -1: return kOrange
    if name.find("SLVR")  != -1: return kOrange
    if name.find("SSloose")  != -1: return kOrange
    if name.find("SS_") != -1: return kRed 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if sample == "FNP":        return TColor.GetColor('#dadaeb')
    if sample == "VV":         return TColor.GetColor('#fe9929')
    if sample == "ttbar":      return TColor.GetColor('#0868ac')
    if sample == "Wt":         return TColor.GetColor('#4e97fdfc')

    if PlotsForPaper == True:
        if sample == "Zjets":      return TColor.GetColor('#41ab5d')
        if sample == "Zttjets":    return TColor.GetColor('#41ab5d')
        if sample == "VVV":        return TColor.GetColor('#41ab5d')
        if sample == "other":      return TColor.GetColor('#41ab5d')
    elif PlotsForPaper == False:
        if sample == "Zjets":      return TColor.GetColor('#41ab5d')
        if sample == "Zttjets":    return TColor.GetColor('#c7e9c0')
        if sample == "VVV":        return TColor.GetColor('#fec49f')
        if sample == "other":      return TColor.GetColor('#9ecae1')

    else:
        print ("cannot find color for sample (",sample,")")
    return 1



def PoissonError(obs):
    """
    Return the Poisson uncertainty on a number

    @param obs The number to calculate the uncertainty for
    reference from ATLAS Statistics Forum: http://www.pp.rhul.ac.uk/~cowan/atlas/ErrorBars.pdf
    """
    posError = TMath.ChisquareQuantile(1. - (1. - 0.68)/2. , 2.* (obs + 1.)) / 2. - obs
    negError = obs - TMath.ChisquareQuantile((1. - 0.68)/2., 2.*obs) / 2
    symError = abs(posError-negError)/2.
    return (posError,negError,symError)



def MymakePullPlot(pickleFilename, regionList, samples, renamedRegions, outputPrefix, doBlind=True, outDir=outputDir,plotSignificance="",):

    try:
        picklefile = open(pickleFilename,'rb')
    except IOError:
        print ("Cannot open pickle %s, continuing to next" % pickleFilename)
        return

    mydict = pickle.load(picklefile)

    results1 = []

    for region in mydict["names"]:
        index = mydict["names"].index(region)
        try:
            nbObs = mydict["nobs"][index]
        except:
            nbObs = 0

        if doAfterFit == True:
            nbExp = mydict["TOTAL_FITTED_bkg_events"][index]
            nbExpEr = mydict["TOTAL_FITTED_bkg_events_err"][index]

        elif doBeforeFit == True:
            nbExp = mydict["TOTAL_MC_EXP_BKG_events"][index]
            nbExpEr = mydict["TOTAL_MC_EXP_BKG_err"][index]
        else:
            print('please choose before/after fit')

        pEr = PoissonError(nbObs)
        totEr = sqrt(nbExpEr*nbExpEr+pEr[2]*pEr[2])
        totErDo = sqrt(nbExpEr*nbExpEr+pEr[1]*pEr[1])
        totErUp = sqrt(nbExpEr*nbExpEr+pEr[0]*pEr[0])

        pEr_pull = PoissonError(nbExp)
        totEr_pull = sqrt(nbExpEr*nbExpEr+pEr_pull[2]*pEr_pull[2])
        totErDo_pull = sqrt(nbExpEr*nbExpEr+pEr_pull[1]*pEr_pull[1])
        totErUp_pull = sqrt(nbExpEr*nbExpEr+pEr_pull[0]*pEr_pull[0])
            
        plot_pulls = True
        if plot_pulls == True:
            #print ("plot pull = (obs-exp)/err in the bottom panel")
            if (nbObs-nbExp) > 0 and totErUp_pull != 0:
                pull = (nbObs-nbExp)/totErUp_pull
            if (nbObs-nbExp) <= 0 and totErDo_pull != 0:
                pull = (nbObs-nbExp)/totErDo_pull
            #print (pull)
            if -0.02 < pull < 0: pull = -0.02 ###ATT: ugly
            if 0 < pull < 0.02:  pull = 0.02 ###ATT: ugly

        nbExpComponents = []
        for sam in samples.split(","):
            if doAfterFit == True:
                nbExpComponents.append((sam, mydict["Fitted_events_"+sam][index] ))
            elif doBeforeFit == True:
                nbExpComponents.append((sam, mydict["MC_exp_events_"+sam][index] ))

        results1.append((region,pull,nbObs,nbExp,nbExpEr,totEr,nbExpComponents))

    #pull
    if plotSignificance =="":
        if scale == "log":
            MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, 0.12)
        elif scale == "linear":
            MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, 0.12, logy=False)
    else:
        if scale == "log":
            MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, minimum = 0.05)
        elif scale == "linear":
            MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, minimum = 0.05, logy=False)
    # return the results array in case you want to use this somewhere else
    return results1






def MakeHist(regionList, renamedRegions, results, hdata, hbkg, hbkgUp, hbkgDown, graph_bkg, graph_bkg2, graph_bkg3, graph_data, graph_pull, hbkgComponents):
    max = 0
    min = 99999999999.
    for counter in range(len(regionList)):#loop over all the regions
        nObs = 0
        nExp = 0
        nExpEr = 0
        nExpTotEr = 0
        nExpStatEr = 0
        nExpStatErUp = 0
        nExpStatErDo = 0
        nExpTotErUp = 0
        nExpTotErDo = 0
        pull = 0

        name = regionList[counter].replace(" ","")
        if name in renamedRegions.keys():
            name = renamedRegions[name]

        for info in results: #extract the information
            if regionList[counter] in info[0]:
                nObs = info[2]
                nExp = info[3]
                nExpEr = info[4]

                if nExp>0:
                    nExpStatEr = sqrt(nExp)
                pEr = PoissonError(nObs)
                nExpStatErUp = pEr[0]
                nExpStatErDo = pEr[1]

                if name.find("CR") < 0:
                    nExpTotEr = sqrt(nExpEr*nExpEr)
                    nExpTotErUp = sqrt(nExpEr*nExpEr)
                    nExpTotErDo = sqrt(nExpEr*nExpEr)
                else:
                    nExpTotEr = nExpEr
        
                if (nObs-nExp) >= 0 and nExpTotErUp != 0:
                    pull = (nObs-nExp)/nExpTotErUp
                if (nObs-nExp) <= 0 and nExpTotErDo != 0:
                    pull = (nObs-nExp)/nExpTotErDo
                if nObs == 0 and nExp == 0:
                    pull = 0
                    nObs = -100
                    nPred = -100
                    nExpEr = 0
                    nExpTotEr = 0
                    nExpStatEr = 0
                    nExpStatErUp = 0
                    nExpStatErDo = 0
                    nExpTotErUp = 0
                    nExpTotErDo = 0

                #bkg components
                compInfo = info[6]
                for i in range(len(compInfo)):
                    hbkgComponents[i].SetBinContent(counter+1,compInfo[i][1])

                break

        if nObs>max: max = nObs
        if nExp+nExpTotErUp > max: max = nExp+nExpTotErUp
        if nObs<min and nObs != 0: min = nObs
        if nExp<min and nExp != 0: min = nExp

        bin_width = (Plots[variable]['PlotBinHigh']-Plots[variable]['PlotBinLow'])/Plots[variable]['PlotNBins']


        graph_bkg.SetPoint(counter, hbkg.GetBinCenter(counter+1), nExp)
        graph_bkg.SetPointError(counter, bin_width, bin_width, nExpStatErDo, nExpStatErUp)

        graph_bkg2.SetPoint(counter, hbkg.GetBinCenter(counter+1), nExp)
        graph_bkg2.SetPointError(counter, bin_width/2., bin_width/2., nExpEr, nExpEr)

        graph_bkg3.SetPoint(counter, hbkg.GetBinCenter(counter+1), nExp)
        graph_bkg3.SetPointError(counter, bin_width/2, bin_width/2, 0, 0)

        if  nObs > 0: # remove this condition to also show upper limits data bars in bins with 0 observed events
            graph_data.SetPoint(counter, hbkg.GetBinCenter(counter+1), nObs)
            graph_data.SetPointError(counter, 0., 0., PoissonError(nObs)[1], PoissonError(nObs)[0])

        if not nObs > 0:
            nObs = 0
        

        print("{}  nObs={}, nObsErUp={}, nObsErDown= {}, nExp = {}, nExpEr = {}".format(info[0],  nObs,PoissonError(nObs)[0], PoissonError(nObs)[1], nExp, nExpEr))


        graph_pull.SetPoint(counter, hbkg.GetBinCenter(counter+1), pull)
        graph_pull.SetPointError(counter, 0., 0, 0, 0)

        if doBlind==False:
            hdata.SetBinContent(counter+1, nObs)
            hdata.SetBinError(counter+1, sqrt(nObs))

        hbkg.SetBinContent(counter+1, nExp)
        hbkg.SetBinError(counter+1, nExpEr)

        hbkgUp.SetBinContent(counter+1, nExp+nExpTotErUp)
        hbkgDown.SetBinContent(counter+1, nExp-nExpTotErDo)


    return



# Methods from PullPlotUtils
def MyMakeHistPullPlot(samples, regionList, outFileNamePrefix, hresults, renamedRegions, doBlind, outDir=outputDir, minimum=0.1, maximum=None, logy=True):
    print ("========================================", outFileNamePrefix)
    ROOT.gStyle.SetOptStat(0000);
    Npar=len(regionList)


    hdata = TH1F(outFileNamePrefix, outFileNamePrefix, Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']);
    hdata.GetYaxis().SetTitle("Events")
    hdata.GetYaxis().SetTitleSize( 0.065 )
    hdata.GetYaxis().SetTitleOffset( 0.8 )
    hdata.GetXaxis().SetLabelSize( 0.06 )
    hdata.GetYaxis().SetLabelSize( 0.05 )
    hdata.SetMarkerStyle(20)
    hdata.SetMarkerColor(kBlack)

    hbkg = TH1F(" ", " ", Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']);
    hbkg.GetYaxis().SetTitle("Events")
    if region == 'CR_Dib' and variable == 'MT2':
        hbkg.GetYaxis().SetTitle("Events / 25 GeV")
    elif region == 'CR_Dib' and variable == 'BDTVVDeltaM100_90':
        hbkg.GetYaxis().SetTitle("Events / 0.05")
    elif region == 'CR_Top' and variable == 'METsig':
        hbkg.GetYaxis().SetTitle("Events / 1")
    elif region == 'CR_Top' and variable == 'BDTtopDeltaM100_90':
        hbkg.GetYaxis().SetTitle("Events / 0.05")
    elif region == 'VR_Dib_DF0J' and variable == 'BDTVVDeltaM100_90':
        hbkg.GetYaxis().SetTitle("Events / 0.02")
    elif region == 'VR_Dib_SF0J' and variable == 'DPhib':
        hbkg.GetYaxis().SetTitle("Events / 0.314")
    elif region == 'VR_Top_DF1J' and variable == 'BDTtopDeltaM100_90':
        hbkg.GetYaxis().SetTitle("Events / 0.04")
    elif region == 'VR_Top_SF1J' and variable == 'cosTstar':
        hbkg.GetYaxis().SetTitle("Events / 0.1")
    elif region == 'VR_Top_DF0J' and variable == 'mll':
        hbkg.GetYaxis().SetTitle("Events / 30 GeV")
    elif region == 'VR_Top_SF0J' and variable == 'BDTtopDeltaM100_90':
        hbkg.GetYaxis().SetTitle("Events / 0.05")
    elif region == 'SRD_DF0J_81' and variable == 'BDTDeltaM100_90':
        hbkg.GetYaxis().SetTitle("Events / 0.005")
    elif region == 'SRD_SF0J_77' and variable == 'BDTDeltaM100_90':
        hbkg.GetYaxis().SetTitle("Events / 0.005")
    elif region == 'VR_Dib_DF0J_SR_DF0J' and variable == 'BDTDeltaM100_90':
        hbkg.GetYaxis().SetTitle("Events / 0.01")
    elif region == 'VR_Dib_DF0J_SR_DF0J' and variable == 'MT2':
        hbkg.GetYaxis().SetTitle("Events / 10 GeV")
    elif region == 'VR_Dib_SF0J_SR_SF0J' and variable == 'BDTDeltaM100_90':
        hbkg.GetYaxis().SetTitle("Events / 0.01")
    elif region == 'VR_Dib_SF0J_SR_SF0J' and variable == 'MT2':
        hbkg.GetYaxis().SetTitle("Events / 10 GeV")
    hbkg.GetYaxis().SetTitleSize( 0.065 )
    hbkg.GetYaxis().SetTitleOffset( 0.8 )
    hbkg.GetXaxis().SetLabelSize( 0.06 )
    hbkg.GetYaxis().SetLabelSize( 0.05 )
    hbkg.SetLineColor(1)
    hbkg.SetLineWidth(2)

    hbkgUp = TH1F("hbkgUp", "hbkgUp", Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']);
    hbkgUp.SetLineStyle(0)
    hbkgUp.SetLineWidth(0)

    hbkgDown = TH1F("hbkgDown", "hbkgDown", Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']);
    hbkgDown.SetLineStyle(0)
    hbkgDown.SetLineWidth(0)

    hbkgComponents = []
    samples.replace(" ","") #remove spaces, and split by comma => don't split by ", "
    for sam in samples.split(","):
        h = TH1F("hbkg"+sam, "hbkg"+sam, Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        if PlotsForPaper == True:
            if sam in ["Zjets","Zttjets","VVV","other"]:
                h.SetLineColor(TColor.GetColor('#41ab5d'))
            else:
                h.SetLineWidth(0)
        if PlotsForPaper == False:
            h.SetLineWidth(0)
        h.SetFillColor(getSampleColor(sam))
        hbkgComponents.append(h)

    graph_bkg = TGraphAsymmErrors(Plots[variable]['PlotNBins'])
    graph_bkg.SetFillColor(1)
    graph_bkg2 = TGraphAsymmErrors(Plots[variable]['PlotNBins'])
    graph_bkg2.SetFillColor(1)
    graph_bkg2.SetLineWidth(2)
    graph_bkg2.SetFillStyle(3004)

    graph_bkg3 = TGraphAsymmErrors(Plots[variable]['PlotNBins'])
    graph_bkg3.SetFillColor(kCyan+1)
    graph_data = TGraphAsymmErrors(Plots[variable]['PlotNBins'])
    graph_data.SetFillColor(kCyan+1)

    graph_pull = TGraphAsymmErrors(Npar)

    MakeHist(regionList,  renamedRegions,  hresults, hdata, hbkg, hbkgDown, hbkgUp, graph_bkg, graph_bkg2, graph_bkg3, graph_data, graph_pull, hbkgComponents)

    myleg = TLegend(0.39,0.56,0.62,0.85)
    myleg.SetNColumns(2)
    if addSignal == False:
        myleg = TLegend(0.5,0.55,0.8,0.85)
        myleg.SetNColumns(2)
    myleg.SetBorderSize(0)
    myleg.SetFillStyle(0)
    myleg.SetTextSize(0.056);
    myleg.AddEntry(graph_data, renamedRegions.get("data", "Data"), "ep")
    myleg.AddEntry(graph_bkg2, renamedRegions.get("bkg2", "SM"), "fl")
    # add background in same order as above
    for h, sample in zip(hbkgComponents, samples.split(",")):
      if PlotsForPaper == True:
          if sample not in ["Zjets","Zttjets","VVV","other"]:
              myleg.AddEntry(h, renamedRegions.get(sample, sample), "f")
          if sample == "Zjets":
              myleg.AddEntry(h, "Others", "f")
      else:
          myleg.AddEntry(h, renamedRegions.get(sample, sample), "f")

    c = TCanvas("c"+outFileNamePrefix, outFileNamePrefix, 600, 400);
    upperPad = ROOT.TPad("upperPad", "upperPad", 0.001, 0.30, 0.995, 0.995)
    lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.001, 0.001, 0.995, 0.30)


    upperPad.SetFillColor(0);
    upperPad.SetBorderMode(0);
    upperPad.SetBorderSize(2);
    upperPad.SetTicks()
    upperPad.SetTopMargin   ( 0.1 );
    upperPad.SetRightMargin ( 0.11 );
    upperPad.SetBottomMargin( 0.0025 );
    upperPad.SetLeftMargin( 0.12 );
    upperPad.SetFrameBorderMode(0);
    upperPad.SetFrameBorderMode(0);
    if logy: upperPad.SetLogy()
    upperPad.Draw()
    
    #lowerPad.SetGridx();
    #lowerPad.SetGridy();
    #lowerPad.SetFillColor(0);
    lowerPad.SetBorderMode(0);
    lowerPad.SetBorderSize(2);
    #lowerPad.SetTickx(25);
    #lowerPad.SetTicky(25);
    lowerPad.SetTopMargin   ( 0.003 );
    lowerPad.SetRightMargin ( 0.11 );
    lowerPad.SetBottomMargin( 0.45 );
    lowerPad.SetLeftMargin( 0.12 );
    lowerPad.Draw()

    c.SetFrameFillColor(ROOT.kWhite)

    upperPad.cd()

    if logy != False:
        if minimum: hbkg.SetMinimum(minimum)
        if (region == 'DF0J_preselection' or region == 'SF0J_preselection'):
            hbkg.SetMaximum(hdata.GetMaximum()*10000)
        elif region == 'CR_Top' and variable == 'BDTtopDeltaM100_90':
            hbkg.SetMaximum(hdata.GetMaximum()*800)
        elif region == 'VR_Dib_DF0J' and variable == 'BDTVVDeltaM100_90':
            hbkg.SetMaximum(hdata.GetMaximum()*700)
        elif region == 'VR_Dib_SF0J' and variable == 'DPhib':
            hbkg.SetMaximum(hdata.GetMaximum()*300)
        elif region == 'VR_Top_DF1J' and variable == 'BDTtopDeltaM100_90':
            hbkg.SetMaximum(hdata.GetMaximum()*1000)
        elif region == 'VR_Top_DF0J' and variable == 'mll':
            hbkg.SetMaximum(hdata.GetMaximum()*500)
        elif region == 'VR_Dib_DF0J_SR_DF0J' and variable == 'MT2' and addSignal == True:
            hbkg.SetMaximum(hdata.GetMaximum()*500) 
        elif region == 'VR_Dib_SF0J_SR_SF0J' and variable == 'MT2':
            hbkg.SetMaximum(hdata.GetMaximum()*300) 
        elif region == 'VR_Dib_SF0J_SR_SF0J' and variable == 'BDTDeltaM100_90' and addSignal == True:
            hbkg.SetMaximum(hdata.GetMaximum()*300) 
        else:
            hbkg.SetMaximum(hdata.GetMaximum()*100)

    if logy == False:
        hbkg.SetMaximum(hdata.GetMaximum()*1.7) 
        hbkg.SetMinimum(0.001) # to avoid an halved 0 appearing
        hbkg.GetYaxis().SetMaxDigits(3)
        if region == 'VR_Dib_SF0J' and variable == 'cosTstar':
            hbkg.SetMaximum(hdata.GetMaximum()*2) 
        elif region == 'VR_Dib_SF0J' and variable == 'DPhib':
            hbkg.SetMaximum(hdata.GetMaximum()*3.2) 
        elif region == 'VR_Top_SF0J' and (variable == 'MET' or variable == 'lep1pT'  or variable == 'lep2pT'):
            hbkg.SetMaximum(hdata.GetMaximum()*2.8)    
        elif region == 'VR_Top_SF0J' and (variable == 'METsig' or variable == 'cosTstar' or variable == 'DPhib'):
            hbkg.SetMaximum(hdata.GetMaximum()*2.3) 
        elif region == 'VR_Top_SF0J' and (variable == 'mll' or variable == 'MT2' or variable ==  'BDTDeltaM100_90' or variable ==  'BDTVVDeltaM100_90' or variable == 'BDTtopDeltaM100_90' or variable == 'BDTothersDeltaM100_90'):
            hbkg.SetMaximum(hdata.GetMaximum()*2.) 


    hbkg.Draw("hist")
    hbkgUp.Draw("hist,same")
    hbkgDown.Draw("hist,same")

    myleg.Draw()
    stack = THStack("stack","stack")
    for h in reversed(hbkgComponents):
        stack.Add(h)
    stack.Draw("same")

    graph_data.SetMarkerStyle(20)
    graph_data.Draw("E0,P,Z")
    graph_bkg2.Draw("2")


    hbkg.Draw("hist,same,axis")

    if addSignal:

        d1 = ROOT.RDataFrame(Signal100_10_Tree_Name, Signal_Tree_File)
        d2 = ROOT.RDataFrame(Signal125_25_Tree_Name, Signal_Tree_File)
        d3 = ROOT.RDataFrame(Signal150_50_Tree_Name, Signal_Tree_File)

        h1 = d1.Filter(Signal_cut).Define("weight","WeightEvents*xsec*WeightLumi*WeightEventsPU*WeightEventselSF*WeightEventsmuSF*WeightEvents_trigger_single*WeightEventsJVT*WeightEventsbTag*138950").Histo1D(("hsignal","hsignal",Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'],Plots[variable]['PlotBinHigh']),variable,"weight")
        h2 = d2.Filter(Signal_cut).Define("weight","WeightEvents*xsec*WeightLumi*WeightEventsPU*WeightEventselSF*WeightEventsmuSF*WeightEvents_trigger_single*WeightEventsJVT*WeightEventsbTag*138950").Histo1D(("hsignal","hsignal",Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'],Plots[variable]['PlotBinHigh']),variable,"weight")
        h3 = d3.Filter(Signal_cut).Define("weight","WeightEvents*xsec*WeightLumi*WeightEventsPU*WeightEventselSF*WeightEventsmuSF*WeightEvents_trigger_single*WeightEventsJVT*WeightEventsbTag*138950").Histo1D(("hsignal","hsignal",Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'],Plots[variable]['PlotBinHigh']),variable,"weight")

        hsignal_100_10 = TH1F("h_100_10", "h_100_10", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        hsignal_125_25 = TH1F("h_125_25", "h_125_25", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        hsignal_150_50 = TH1F("h_150_50", "h_150_50", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])

        for i in range(Npar):
            if i == Npar-1: #Last bin
                hsignal_100_10.SetBinContent(i+1,h1.GetBinContent(i+1)+h1.GetBinContent(i+2)) # catch overflow
                hsignal_125_25.SetBinContent(i+1,h2.GetBinContent(i+1)+h2.GetBinContent(i+2)) # catch overflow
                hsignal_150_50.SetBinContent(i+1,h3.GetBinContent(i+1)+h3.GetBinContent(i+2)) # catch overflow
            else:
                hsignal_100_10.SetBinContent(i+1,h1.GetBinContent(i+1))
                hsignal_125_25.SetBinContent(i+1,h2.GetBinContent(i+1))
                hsignal_150_50.SetBinContent(i+1,h3.GetBinContent(i+1))

        hsignal_100_10.SetLineWidth(4)
        hsignal_125_25.SetLineWidth(4)
        hsignal_150_50.SetLineWidth(4)

        hsignal_100_10.SetLineStyle(2)
        hsignal_125_25.SetLineStyle(5)
        hsignal_150_50.SetLineStyle(4)

        hsignal_100_10.SetLineColor(50) #TColor.GetColor('#b00000')
        hsignal_125_25.SetLineColor(TColor.GetColor('#663300')) #TColor.GetColor('#84ff26')
        hsignal_150_50.SetLineColor(12) #TColor.GetColor('#ff26d4')

        hsignal_100_10.Draw("same")
        hsignal_125_25.Draw("same")
        hsignal_150_50.Draw("same")

        graph_data.Draw("E0,P,Z") # redraw on top of sig

        myleg2 = TLegend(0.615,0.60,0.775,0.86)
        myleg2.SetNColumns(1)
        myleg2.SetBorderSize(0)
        myleg2.SetFillStyle(0)
        myleg2.SetTextSize(0.0435)
        myleg2.AddEntry(hsignal_100_10, "m(#tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = (100, 10) GeV", "l")
        myleg2.AddEntry(hsignal_125_25, "m(#tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = (125, 25) GeV", "l")
        myleg2.AddEntry(hsignal_150_50, "m(#tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = (150, 50) GeV", "l")
        myleg2.Draw()

    lumiText = TLatex()
    lumiText.SetNDC()
    lumiText.SetTextAlign( 11 )
    lumiText.SetTextFont( 42 )
    lumiText.SetTextSize( 0.06 )
    lumiText.SetTextColor( 1 )
    lumiText.DrawLatex(0.15,0.8, "#bf{#it{ATLAS}} Internal")
    lumiText.DrawLatex(0.15, 0.74, "#sqrt{{s}}=13 TeV, {} fb^{{-1}}".format(getattr(MyMakeHistPullPlot,'luminosity',139)))
    #lumiText.SetTextSize( 0.05 )   change size if needed
    lumiText.DrawLatex(0.15, 0.68, "{}".format(renamedRegions[region]))

    lowerPad.cd()


    if doSignificance == True:

        hSignificance     = TH1F("hSignificance",    "", Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        hSignificance_int = TH1F("hSignificance_int","", Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])


        for i in range(Npar):
            
            SM = 0.
            SM_err = 0.
            Signal = 0.
            SM = graph_bkg2.GetPointY(i)
            SM_err = graph_bkg2.GetErrorY(i)
            Signal = hsignal.GetBinContent(i+1)

            SM_int = 0.
            SM_int_err = 0.
            Signal_int = 0.

            if cutLeft == True:
                my_range = range(0, i+1)
            elif cutRight == True:
                my_range = range(i, Npar)

            for k in my_range:           
                SM_int += graph_bkg2.GetPointY(k)
                SM_int_err += graph_bkg2.GetErrorY(k)*graph_bkg2.GetErrorY(k)
                Signal_int += hsignal.GetBinContent(k+1)
                #print("k =  {},        SM_int_err = {}    ".format( k+1, sqrt(SM_int_err)))
            SM_int_err = sqrt(SM_int_err)
            if ( (SM_int!=0) and not (Signal_int == 0 and SM_int < 8.) ):
                hSignificance_int.SetBinContent(i+1,atlas_significance(Signal_int+SM_int, SM_int, SM_int_err ))
                print('   BIN: {} - cumulative Signal = {}, cumulative SM = {}, cumulative SMerr = {}, cumulative Z - {}'.format(i+1,Signal_int,SM_int, SM_int_err, atlas_significance(Signal_int+SM_int, SM_int, SM_int_err )))
            else:
                print('   BIN: {} - cumulative Signal = {}, cumulative SM = {}, cumulative SMerr = {}, cumulative Z - {}'.format(i+1,Signal_int,SM_int_err, SM_int,0.))
                hSignificance_int.SetBinContent(i+1,0.)     


            #print("BIN =  {},     Signal_int={},     SM_int   = {},       SM_int_err = {}".format( i+1, Signal_int,  SM_int,  SM_int_err))
            print('########')

        hSignificance_int.SetLineColor(ROOT.kGreen)

        hSignificance_int.GetXaxis().SetTitle(Plots[variable]['titleX'])
        hSignificance_int.GetXaxis().SetTitleSize( 0.12 )
        hSignificance_int.GetXaxis().SetTitleOffset( 1.3 )

        if cutLeft == True:
            hSignificance_int.GetYaxis().SetTitle("Cut left Z")
        elif cutRight == True:
            hSignificance_int.GetYaxis().SetTitle("Cut right Z")
        #hSignificance_int.GetYaxis().SetTitle("Cut right Z")
        hSignificance_int.GetYaxis().SetTitleSize( 0.12 )
        hSignificance_int.GetYaxis().SetTitleOffset( 0.4 )
        hSignificance_int.GetXaxis().SetLabelSize( 0.1 )
        hSignificance_int.GetYaxis().SetLabelSize( 0.08 )

        hSignificance_int.Draw()


    elif doRatioWtTtbar:
        print('Considering Wt = {} and ttbar = {}'.format(hbkgComponents[3],hbkgComponents[2]))

        hRatio = TH1F("hRatio","S/B",Npar, 0.,Npar)
        hRatio.GetXaxis().SetLimits(0., 0.2)

        # Ratio=Wt/ttbar, DeltaRatio/Ratio = DeltaWt/Wt + Deltattbar/ttbar  ->  DeltaRatio = DeltaWt/ttbar + Deltattbar*Wt/(ttbar**2)

        for i in range(1, Npar+1):
            Wt = hbkgComponents[3].GetBinContent(i)
            errWt = hbkgComponents[3].GetBinError(i)
            ttbar = hbkgComponents[2].GetBinContent(i)
            errttbar = hbkgComponents[2].GetBinError(i)

            
            if ttbar>0.01:
               #print("i={}, Wt={}, ttbar={}, ratio={}, errRatio={}".format(i,Wt,ttbar,Wt/ttbar,(   (errWt/ttbar) + (errttbar*Wt/(ttbar**2))   )))
               hRatio.SetBinContent(i,Wt/ttbar)
               hRatio.SetBinError(i, (errWt/ttbar) + (errttbar*Wt/(ttbar**2)) )
            else:
               #print("i={}, Wt={}, ttbar={}".format(i,Wt,ttbar))
               continue

        hRatio.SetMarkerStyle(20)

        hRatio.GetXaxis().SetTitle(Plots[variable]['titleX'])
        hRatio.GetXaxis().SetTitleSize( 0.12 )
        hRatio.GetXaxis().SetTitleOffset( 1.3 )
        #hRatio.GetXaxis().SetNdivisions( 409 )

        hRatio.SetMinimum(0.)
        hRatio.SetMaximum(0.7)


        hRatio.GetYaxis().SetTitle("Wt/ttbar")
        hRatio.GetYaxis().SetTitleSize( 0.12 )
        hRatio.GetYaxis().SetTitleOffset( 0.4 )
        hRatio.GetXaxis().SetLabelSize( 0.1 )
        hRatio.GetYaxis().SetLabelSize( 0.08 )
        hRatio.Draw("E0")
         

    else:
        hShadow = TH1F("hShadow","",Npar, Plots[variable]['PlotBinLow'],Plots[variable]['PlotBinHigh'])
        from array import array
        positions = array('f')
        ratios = array('f')
        exl = array('f')
        exh = array('f')
        eyl = array('f')
        eyh = array('f')

        for i in range(1, Npar+1):
            xSM = stack.GetStack().Last().GetBinContent(i)
            errxSM = graph_bkg2.GetErrorY(i-1)
            if xSM != 0 and i != Npar+1:

                bkg = hbkg.GetBinContent(i)
                data = hdata.GetBinContent(i)

                if data > 0: # remove this condition to also show upper limits data bars in bins with 0 observed events
                    ratio = data / bkg
                    positions.append(hbkg.GetBinCenter(i))

                    ratios.append(ratio)
                    exl.append(0.)
                    exh.append(0.)
                    eyl.append(PoissonError(data)[1]/bkg)
                    eyh.append(PoissonError(data)[0]/bkg)

            if xSM != 0 and i != Npar+1:
                hShadow.SetBinContent(i,1) 
                hShadow.SetBinError(i,errxSM/xSM)

        graphRatio = TGraphAsymmErrors(len(positions),positions,ratios,exl,exh,eyl,eyh)
        graphRatio.SetMinimum(0.)
        graphRatio.SetMaximum(2.)

        graphRatio.SetMarkerStyle(20)

        hShadow.SetLineColor(0)
        hShadow.SetFillColor(1)
        hShadow.SetFillStyle(3004)
        hShadow.SetLineColor(1)
        hShadow.SetMarkerSize(0)
        hShadow.SetMarkerColor(0)

        hShadow.GetXaxis().SetTitle(Plots[variable]['titleX'])
        hShadow.GetXaxis().SetTitleSize( 0.15 )
        hShadow.GetXaxis().SetTitleOffset( 1. )

        hShadow.GetXaxis().SetTickSize( 0.1 )

        hShadow.GetYaxis().SetTitle("Data/SM")
        hShadow.GetYaxis().SetTitleSize( 0.125 )
        #hShadow.GetXaxis().SetNdivisions( 505 )
        hShadow.GetYaxis().SetNdivisions( 505 )
        hShadow.GetYaxis().SetTitleOffset( 0.3 )
        hShadow.GetXaxis().SetLabelSize( 0.12 )
        hShadow.GetYaxis().SetLabelSize( 0.12 )
        
        hShadow.GetYaxis().SetRangeUser(0.1,2.2)

        hShadow.Draw("E2")
        graphRatio.Draw("E0,P,Z")
        
        firstbin = hShadow.GetXaxis().GetFirst()
        lastbin = hShadow.GetXaxis().GetLast()
        xmax = hShadow.GetXaxis().GetBinUpEdge(lastbin) 
        xmin = hShadow.GetXaxis().GetBinLowEdge(firstbin) 

        l = TLine(xmin, 1., xmax, 1.)
        l2 = TLine(xmin, 0.5, xmax, 0.5)
        l3 =  TLine(xmin, 1.5, xmax, 1.5)
        l4 =  TLine(xmin, 2., xmax, 2.)
        l5 =  TLine(xmin, 2.5, xmax, 2.5)
        l.SetLineWidth(1)
        l.SetLineStyle(2)
        l2.SetLineStyle(3)
        l3.SetLineStyle(3)
        l4.SetLineStyle(3)
        l5.SetLineStyle(3)

        l.Draw()
        l2.Draw()
        l3.Draw()
        l4.Draw()
        l5.Draw()

    
    # SR-DF
    if doSRDF==True:
        box = TBox(0.858,-0.3,0.862,0.03)
        box.SetFillColor(0)
        box.Draw()
 
    # SR-SF
    if doSRSF==True:   
        box = TBox(0.808,-0.3,0.812,0.03)
        box.SetFillColor(0)
        box.Draw()

    if doSRDF==True or doSRSF==True:    
        binfix = TLatex()
        binfix.SetNDC()
        binfix.SetTextAlign( 11 )
        binfix.SetTextFont( 42 )
        binfix.SetTextSize( 0.1 )
        binfix.SetTextColor( 1 )
        binfix.DrawLatex(0.88,0.36, "1")
    

    if doBlind:
        #c.Print(os.path.join(outDir, "{}_blindSR.eps".format(variable)))
        #c.Print(os.path.join(outDir, "{}_blindSR.png".format(variable)))
        #c.Print(os.path.join(outDir, "{}_blindSR.pdf".format(variable)))
        if scale == "log":
            c.Print(os.path.join(outDir, "{}_{}_LogScale__blindSR.pdf".format(region,variable)))
        elif scale == "linear":
            c.Print(os.path.join(outDir, "{}_{}_LinearScale__blindSR.pdf".format(region,variable)))
    else:
        #c.Print(os.path.join(outDir, "{}.eps".format(variable)))
        #c.Print(os.path.join(outDir, "{}.png".format(variable)))
        #c.Print(os.path.join(outDir, "{}.pdf".format(variable)))
        if scale == "log":
            c.Print(os.path.join(outDir, "{}_{}_LogScale.pdf".format(region,variable)))
        elif scale == "linear":
            c.Print(os.path.join(outDir, "{}_{}_LinearScale.pdf".format(region,variable)))

    return






###############
def atlas_significance(nbObs, nbExp, nbExpEr):
    factor1 = nbObs*log( (nbObs*(nbExp+nbExpEr**2))/(nbExp**2+nbObs*nbExpEr**2) )
    factor2 = (nbExp**2/nbExpEr**2)*log( 1 + (nbExpEr**2*(nbObs-nbExp))/(nbExp*(nbExp+nbExpEr**2)) )

    if nbObs < nbExp:
        pull  = -sqrt(2*(factor1 - factor2))
    else:
        pull  = sqrt(2*(factor1 - factor2))
    return (pull)




####  Code for "Plotting the Differences Between Data and Expectation"
### https://arxiv.org/abs/1111.2062
### Code from HistFitter/python/pValue.py
### Georgios Choudalakis and Diego Casadei



import math

def pValuePoissonError(nObs,   E, V):
    print ("obs",nObs)
    if E<=0 or V<=0:
        print ("ERROR in pValuePoissonError(): expectation and variance must be positive. ")
        print  ("Returning 0.5")

    B = E/V
    A = E*B

    if A>100:  # need to use logarithms

        stop=nObs
        if nObs>E :
            stop = stop-1

    #/ NB: must work in log-scale otherwise troubles!
        logProb = A*math.log(B/(1+B))
        sum=math.exp(logProb) # P(n=0)
        for u in range(1, stop+1):
            logProb += math.log((A+u-1)/(u*(1+B)))
            sum += math.exp(logProb)

        if nObs>E:  # excess
            return 1-sum
        else:  # deficit
            return sum
        
    else :
        # Recursive formula: P(nA,B) = P(n-1A,B) (A+n-1)/(n*(1+B))
        p0 = pow(B/(1+B),A) # P(0A,B)
        nExp = A/B
        if nObs>nExp :# excess
            pLast = p0
            sum = p0
            for k in range(1, nObs):
                p = pLast * (A+k-1) / (k*(1+B))
	# cout << Form("Excess: P(%d%8.5g) = %8.5g and sum = %8.5g",k-1,nExp,pLast,sum) << " -> "
                sum = sum + p
                pLast = p
	# cout << Form("P(%d%8.5g) = %8.5g and sum = %8.5g",k,nExp,pLast,sum) << endl      
            return 1-sum
        else :# deficit
            pLast = p0
            sum = p0
            for k in range(1, nObs+1):
	# cout << Form("Deficit: P(%d%8.5g) = %8.5g and sum = %8.5g",k-1,nExp,pLast,sum) << " -> "
                p = pLast * (A+k-1) / (k*(1+B))
                sum += p
                pLast = p
            return sum


def pja_normal_quantile( p): 

    a = [ -3.969683028665376e+01,     2.209460984245205e+02,     -2.759285104469687e+02,     1.383577518672690e+02,     -3.066479806614716e+01,    2.506628277459239e+00]
    b = [ -5.447609879822406e+01,     1.615858368580409e+02,     -1.556989798598866e+02,     6.680131188771972e+01,     -1.328068155288572e+01, ]
  # b = [ b(1) -> b[0] ,                 b(2) ,                        b(3),                        b(4),                    b(5) -> b[4] ]
    c = [-7.784894002430293e-03,      -3.223964580411365e-01,    -2.400758277161838e+00,     -2.549732539343734e+00,     4.374664141464968e+00,    2.938163982698783e+00,]
  # c = [c(1) -> c[0],                   c(2),                         c(3),                        c(4),                         c(5),                c(6) -> c[5]   ]
    d = [7.784695709041462e-03,        3.224671290700398e-01,     2.445134137142996e+00,      3.754408661907416e+00, ]
  # d = [ d(1) -> d[0],                  d(2),                         d(3),                        d(4) -> d[3] ]

  # Define break-points.
    p_low  = 0.02425
    p_high = 1 - p_low

  # output value
    x=0

  # Rational approximation for lower region.
    if 0 < p and p < p_low:
        q = math.sqrt(-2*math.log(p))
        x = (((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) / ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1)

  # Rational approximation for central region.
    elif p_low <= p and p <= p_high:
        q = p - 0.5
        r = q*q
        x = (((((a[0]*r+a[1])*r+a[2])*r+a[3])*r+a[4])*r+a[5])*q / (((((b[0]*r+b[1])*r+b[2])*r+b[3])*r+b[4])*r+1)
  # Rational approximation for upper region.
    elif p_high < p and p < 1:
        q = math.sqrt(-2*math.log(1-p))
        x = -(((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) / ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1)

    return x


def  pValueToSignificance( p, excess): # excess: bool, False if deficit
  if p<0 or p>1:
    print ("ERROR: p-value must belong to [0,1] but input value is ", p)
    return 0

  if excess:
    return pja_normal_quantile(1-p)
  else:
    return pja_normal_quantile(p)


def arxiv_significance(nbObs, nbExp, nbExpEr):
    #calculates significance from https://arxiv.org/abs/1111.2062
    print ("plot significance in the bottom panel")
    pValue = pValuePoissonError(int(nbObs), nbExp, nbExpEr*nbExpEr)
    print ("pval:", pValue)
    if pValue < 0.5:
        pull = pValueToSignificance(pValue, nbObs>nbExp )
        print (pull)
    else:
        pull = 0.0001
        print ("pull at zero!")
    return pull

##############################################



if __name__ == "__main__":
    main()


