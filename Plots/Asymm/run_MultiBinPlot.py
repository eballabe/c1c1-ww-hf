#!/usr/bin/env python
import os

#variables_to_plot = ['MET','METsig','lep1pT','lep2pT','mll','MT2','cosTstar','DPhib','BDTDeltaM100_90','BDTVVDeltaM100_90','BDTtopDeltaM100_90','BDTothersDeltaM100_90']
#variables_to_plot = ['MET','METsig','lep1pT','lep2pT','mll','MT2','cosTstar','DPhib','dPhiMETL1','dPhiMETL2'] #preselection
variables_to_plot=['MET']

#regions = ['CR_Dib','CR_Top','VR_Dib_DF0J','VR_Dib_SF0J','VR_Top_DF1J','VR_Top_SF1J','VR_Top_DF0J','VR_Top_SF0J']
#regions = ['DF0J_preselection'] # separate: need signal
#regions = ['SF0J_preselection'] # separate: need signal
regions = ['CR_Dib']

#Log Scale
for region in regions:
    if os.path.exists('Plots/%s/LogScale'%region) == False:
        os.system('mkdir Plots/%s/LogScale'%region)
for variable in variables_to_plot:
    for region in regions:
        os.system('python /users2/ballaben/c1c1-ww-hf/Plots/Asymm/MultiBinPlot_Asymm.py -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s log -o Plots/%s/LogScale'%(variable,region,region,region))

#Linear Scale
for region in regions:
    if os.path.exists('Plots/%s/LinearScale'%region) == False:
        os.system('mkdir Plots/%s/LinearScale'%region)
for variable in variables_to_plot:
    for region in regions:
        os.system('python /users2/ballaben/c1c1-ww-hf/Plots/Asymm/MultiBinPlot_Asymm.py -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s linear -o Plots/%s/LinearScale'%(variable,region,region,region))





#HERE BLINDED (-b flag)

'''
regions = ['SR_DF0J_81_8125','SR_DF0J_8125_815','SR_DF0J_815_8175','SR_DF0J_8175_82','SR_DF0J_82_8225','SR_DF0J_8225_825','SR_DF0J_825_8275','SR_DF0J_8275_83',
'SR_DF0J_83_8325','SR_DF0J_8325_835','SR_DF0J_835_8375','SR_DF0J_8375_84','SR_DF0J_84_845','SR_DF0J_845_85','SR_DF0J_85_86','SR_DF0J_86',
'SR_SF0J_77_775','SR_SF0J_775_78','SR_SF0J_78_785','SR_SF0J_785_79','SR_SF0J_79_795','SR_SF0J_795_80','SR_SF0J_80_81','SR_SF0J_81']

regions = ['SR_DF0J_81_8125','SR_DF0J_8125_815']


for region in regions:
    os.system('mkdir Plots/%s/MultiBinPlots'%region)

for variable in variables_to_plot:
    for region in regions:
        os.system('python MultiBinPlot.py -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o Plots/%s/MultiBinPlots'%(variable,region,region,region))
'''




#HERE BLINDED (-b flag)

'''
regions = ['SR_DF0J_81_8125','SR_DF0J_8125_815','SR_DF0J_815_8175','SR_DF0J_8175_82','SR_DF0J_82_8225','SR_DF0J_8225_825','SR_DF0J_825_8275','SR_DF0J_8275_83',
'SR_DF0J_83_8325','SR_DF0J_8325_835','SR_DF0J_835_8375','SR_DF0J_8375_84','SR_DF0J_84_845','SR_DF0J_845_85','SR_DF0J_85_86','SR_DF0J_86',
'SR_SF0J_77_775','SR_SF0J_775_78','SR_SF0J_78_785','SR_SF0J_785_79','SR_SF0J_79_795','SR_SF0J_795_80','SR_SF0J_80_81','SR_SF0J_81']
'''
'''
regions = ['VR_DF0J_reversedMETsig','VR_SF0J_reversedMETsig']
'''
'''
for region in regions:
    os.system('mkdir Plots/%s/MultiBinPlots'%region)
'''
'''
for variable in variables_to_plot:
    for region in regions:
        os.system('python MultiBinPlot.py -v %s -r %s -w Plots/VR_reversedMETsig/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o Plots/VR_reversedMETsig/%s/MultiBinPlots'%(variable,region,region,region))
'''





'''
variables_to_plot = ['BDTDeltaM100_90']
regions = ['VR_Dib_DF0J','VR_Dib_SF0J','VR_Top_DF0J','VR_Top_SF0J','VR_Top_DF1J','VR_Top_SF1J']

for region in regions:
    os.system('mkdir Plots/CRVR_BDTs/%s/MultiBinPlots'%region)

for variable in variables_to_plot:
    for region in regions:
        os.system('python MultiBinPlot.py -v %s -r %s -w Plots/CRVR_BDTs/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o Plots/CRVR_BDTs/%s/MultiBinPlots'%(variable,region,region,region))
'''

