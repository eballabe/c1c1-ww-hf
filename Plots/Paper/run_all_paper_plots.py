import os

os.system('python run_MultiBinPlot.py --ChannelToPlot CR_Dib --VariableToPlot MT2')
os.system('python run_MultiBinPlot.py --ChannelToPlot CR_Dib --VariableToPlot BDTVVDeltaM100_90')

os.system('python run_MultiBinPlot.py --ChannelToPlot CR_Top --VariableToPlot METsig')
os.system('python run_MultiBinPlot.py --ChannelToPlot CR_Top --VariableToPlot BDTtopDeltaM100_90')

os.system('python run_MultiBinPlot.py --ChannelToPlot VR_Dib_DF0J --VariableToPlot BDTVVDeltaM100_90')
os.system('python run_MultiBinPlot.py --ChannelToPlot VR_Dib_SF0J --VariableToPlot DPhib')

os.system('python run_MultiBinPlot.py --ChannelToPlot VR_Top_DF1J --VariableToPlot BDTtopDeltaM100_90')
os.system('python run_MultiBinPlot.py --ChannelToPlot VR_Top_SF1J --VariableToPlot cosTstar')

os.system('python run_MultiBinPlot.py --ChannelToPlot VR_Top_DF0J --VariableToPlot mll')
os.system('python run_MultiBinPlot.py --ChannelToPlot VR_Top_SF0J --VariableToPlot BDTtopDeltaM100_90')

os.system('python run_MultiBinPlot.py --ChannelToPlot SRD_DF0J_81 --VariableToPlot BDTDeltaM100_90')
os.system('python run_MultiBinPlot.py --ChannelToPlot SRD_SF0J_77 --VariableToPlot BDTDeltaM100_90')

