#!/usr/bin/env python
import os

regions = ['CR_Dib','CR_Top','VR_Dib_DF0J','VR_Dib_SF0J','VR_Top_DF1J','VR_Top_SF1J','VR_Top_DF0J','VR_Top_SF0J']
#regions = ['DF0J_preselection','SF0J_preselection'] # separate: need signal
#regions = ['SRD_DF0J_81','SRD_SF0J_77'] 
regions = ['VR_Dib_DF0J'] #test

variables_to_plot = ['MET','METsig','lep1pT','lep2pT','mll','MT2','cosTstar','DPhib','BDTDeltaM100_90','BDTVVDeltaM100_90','BDTtopDeltaM100_90','BDTothersDeltaM100_90']
#variables_to_plot = ['MET','METsig','lep1pT','lep2pT','mll','MT2','cosTstar','DPhib','dPhiMETL1','dPhiMETL2'] #preselection
#variables_to_plot = ['BDTDeltaM100_90'] 
variables_to_plot=['BDTVVDeltaM100_90'] #test

scale = 'log' #linear

useparser = True
if useparser == True:
    from optparse import OptionParser
    myInputParser = OptionParser()
    myInputParser.add_option("--VariableToPlot", default='lep1pT', help='Variable to plot')
    myInputParser.add_option("--ChannelToPlot", default='CR_Dib', help='Channel to plot')
    (options, args) = myInputParser.parse_args()
    variables_to_plot= []
    variables_to_plot= options.VariableToPlot.split(",")
    regions= []
    regions= options.ChannelToPlot.split(",")



for region in regions:
    if os.path.exists('MyPlots') == False:
        os.system('mkdir MyPlots')

    if region == 'DF0J_preselection' or region == 'SF0J_preselection':
        for variable in variables_to_plot: # --doPreselection --doBeforeFit
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doPreselection --doBeforeFit -v %s -r %s -w %s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s %s -o MyPlots/ --addSignal'%(variable,region,region,scale))

    elif region == 'CR_Dib' or region == 'CR_Top' :
        for variable in variables_to_plot: # --doCRVR --doAfterFit but without signal
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doCRVR --doAfterFit -v %s -r %s -w %s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s %s -o MyPlots/'%(variable,region,region,scale))

    elif region == 'VR_Dib_DF0J' or region == 'VR_Dib_SF0J' or region == 'VR_Top_DF1J' or region == 'VR_Top_SF1J' or region == 'VR_Top_DF0J' or region == 'VR_Top_SF0J':
        for variable in variables_to_plot: # --doCRVR --doAfterFit
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doCRVR --doAfterFit -v %s -r %s -w %s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s %s -o MyPlots/ --addSignal'%(variable,region,region,scale))

    elif region == 'SRD_DF0J_81':
        for variable in variables_to_plot: # --doSRDF --doAfterFit
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doSRDF --doAfterFit -v %s -r %s -w %s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s %s -o MyPlots/ --addSignal'%(variable,region,region,scale))

    elif region == 'SRD_SF0J_77':
        for variable in variables_to_plot: # --doSRSF --doAfterFit
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doSRSF --doAfterFit -v %s -r %s -w %s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s %s -o MyPlots/ --addSignal'%(variable,region,region,scale))

    else:
        raise Exception("Missing region configuration")

