import os

preselection_channels=['DF0J_preselection','SF0J_preselection']
CRVR_channels=['CR_Dib','CR_Top','VR_Dib_DF0J','VR_Dib_SF0J','VR_Top_DF0J','VR_Top_DF1J','VR_Top_SF0J','VR_Top_SF1J']
SR_channels=['SRD_DF0J_81','SRD_SF0J_77']


for channel in CRVR_channels:
    os.system('mkdir Plots/{}'.format(channel))

for channel in CRVR_channels:
    f = open("Plots/{}/command_to_run.py".format(channel), "w")
    f.write("import os \n\
cmd = \'nohup HistFitter.py -twfx -F bkg -D \"before,after\" -u \"--doBkgOnly --doSyst --split 100  --CRonlyFit --doValidationPlots --PlotDictionary CRVR --ChannelToPlot {} --VariableToPlot MET,METsig,lep1pT,lep2pT,mll,MT2,cosTstar,DPhib,BDTDeltaM100_90,BDTVVDeltaM100_90,BDTtopDeltaM100_90,BDTothersDeltaM100_90\" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_BkgOnly_plots.out &\' \n\
os.system(cmd)".format(channel))
    f.close()



for channel in preselection_channels:
    os.system('mkdir Plots/{}'.format(channel))

for channel in preselection_channels:
    f = open("Plots/{}/command_to_run.py".format(channel), "w")
    f.write("import os \n\
cmd = \'nohup HistFitter.py -twxf -F bkg -D \"before,after\" -u \"--doBkgOnly --doSyst --split 100 --CRonlyFit --doValidation --PlotDictionary preselection --ChannelToPlot {} --VariableToPlot lep1pT,lep2pT,MET,METsig,mll,MT2,cosTstar,dPhiMETL1,dPhiMETL2,DPhib\" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_BkgOnly_plots.out &\' \n\
os.system(cmd)".format(channel))
    f.close()




for channel in SR_channels:
    os.system('mkdir Plots/{}'.format(channel))

for channel in SR_channels:
    f = open("Plots/{}/command_to_run.py".format(channel), "w")
    f.write("import os \n\
cmd = \'nohup HistFitter.py -twxf -F bkg -D \"before,after\" -u \"--doBkgOnly --doSyst --split 100 --CRonlyFit --doValidation --PlotDictionary SRDF --ChannelToPlot {} --VariableToPlot BDTDeltaM100_90\" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_BkgOnly_plots.out &\' \n\
os.system(cmd)".format(channel))
    f.close()
