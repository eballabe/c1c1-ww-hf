#!/usr/bin/env python
import os

regions = ['CR_Dib','CR_Top','VR_Dib_DF0J','VR_Dib_SF0J','VR_Top_DF1J','VR_Top_SF1J','VR_Top_DF0J','VR_Top_SF0J']
#regions = ['DF0J_preselection','SF0J_preselection'] # separate: need signal
#regions = ['SRD_DF0J_81','SRD_SF0J_77'] 
regions = ['VR_Top_SF1J'] #test

variables_to_plot = ['MET','METsig','lep1pT','lep2pT','mll','MT2','cosTstar','DPhib','BDTDeltaM100_90','BDTVVDeltaM100_90','BDTtopDeltaM100_90','BDTothersDeltaM100_90']
#variables_to_plot = ['MET','METsig','lep1pT','lep2pT','mll','MT2','cosTstar','DPhib','dPhiMETL1','dPhiMETL2'] #preselection
#variables_to_plot = ['BDTDeltaM100_90'] 
variables_to_plot=['cosTstar'] #test

for region in regions:
    if os.path.exists('MyPlots') == False:
        os.system('mkdir MyPlots')

    if region == 'DF0J_preselection' or region == 'SF0J_preselection':
        for variable in variables_to_plot: # --doPreselection --doBeforeFit
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doPreselection --doBeforeFit -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s log -o MyPlots/'%(variable,region,region))
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doPreselection --doBeforeFit -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s linear -o MyPlots/'%(variable,region,region))

    elif region == 'CR_Dib' or region == 'CR_Top' or region == 'VR_Dib_DF0J' or region == 'VR_Dib_SF0J' or region == 'VR_Top_DF1J' or region == 'VR_Top_SF1J' or region == 'VR_Top_DF0J' or region == 'VR_Top_SF0J':
        for variable in variables_to_plot: # --doCRVR --doAfterFit
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doCRVR --doAfterFit -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s log -o MyPlots/'%(variable,region,region))
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doCRVR --doAfterFit -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s linear -o MyPlots/'%(variable,region,region))

    elif region == 'SRD_DF0J_81':
        for variable in variables_to_plot: # --doSRDF --doAfterFit
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doSRDF --doAfterFit -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s log -o MyPlots/'%(variable,region,region))
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doSRDF --doAfterFit -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s linear -o MyPlots/'%(variable,region,region))

    elif region == 'SRD_SF0J_77':
        for variable in variables_to_plot: # --doSRSF --doAfterFit
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doSRSF --doAfterFit -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s log -o MyPlots/'%(variable,region,region))
            os.system('python /users2/ballaben/c1c1-ww-hf/Plots/MultiBinPlot.py --doSRSF --doAfterFit -v %s -r %s -w Plots/%s/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -s linear -o MyPlots/'%(variable,region,region))

    else:
        raise Exception("Missing region configuration")

