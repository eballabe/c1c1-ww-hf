# C1C1-WW-HF
Repository containing the HistFitter configuration file for the C1C1-WW 2nd Wave Analysis (C1C1-WW-fit.py). More compressed scenario is not used anymore.

# Bkg-Only Fits with Systematics
- BkgOnly-Fit Less-Compressed Scenario 
<pre> nohup HistFitter.py -twxf -F bkg -u"--doBkgOnly --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_BkgOnly_withSyst_DM100 & </pre>
- BkgOnly-Fit More-Compressed Scenario
<pre> nohup HistFitter.py -twxf -F bkg -u"--doBkgOnly --doSyst --split 30" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_BkgOnly_withSyst_DM30 & </pre>

# Exclusion Fits
- Exclusion Fit for e.g. 100_10 point reading trees for backgrounds
<pre> nohup HistFitter.py -twxf -F excl -u"--doExclusion --doSyst --point 100_10" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Exclusion_withSyst_100_10 & </pre>
- Exclusion Fit for e.g. 100_10 point reading cached histograms for backgrounds
<pre> nohup HistFitter.py -wxf -F excl -u"--doExclusion --doSyst --doBackupCacheFile --point 100_10" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Exclusion_withSyst_100_10 & </pre>
- p-value (limit in exclusion fit) for e.g. 100_10 point 
<pre> nohup HistFitter.py -p -F excl -u"--doExclusion --doSyst --point 100_10" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_plimit_withSyst_100_10 & </pre>
- p-value (limit in exclusion fit) from script for all points
<pre> python script_to_compute_pval.py --make_hypotests </pre>
- Exclusion contour
<pre> python script_to_compute_pval.py --hadd_hypotests </pre>
<pre> python script_to_compute_pval.py --generate_JSON_pval </pre>
<pre> harvestToContours.py -i json/combined_hypotest_Nominal__1_harvest_list.json -x m0 -y m12 -l None --xMin 100 --xMax 250 --yMin 1 --yMax 150 </pre>
<pre> contourPlotterExample.py -i outputGraphs.root -g observed </pre>

# Discovery Fit workspace creation
- Discovery Fit Less-Compressed Scenario 
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_DF0J_81_SF0J_77 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_DF0J_81 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_DF0J_82 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_DF0J_83 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_DF0J_84 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_DF0J_85 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_SF0J_77 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_SF0J_78 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_SF0J_79 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --discoveryRegion SRD_SF0J_80 --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM100 & </pre>

- Discovery Fit More-Compressed Scenario 
<pre> nohup HistFitter.py -twxf -F disc -u"--doDiscovery --doSyst --split 30" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Discovery_DM30 & </pre>


# Discovery Fit upper limits with script
<pre> python script_to_run_upperlimits_discovery_fit.py -r $REGION </pre>



# Corr Matrix for BkgOnly Fit 
- Less Compressed
<pre> nohup HistFitter.py -D corrMatrix -F bkg -u"--doBkgOnly --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_CorrPlot & </pre>
- More Compressed
<pre> nohup HistFitter.py -D corrMatrix -F bkg -u"--doBkgOnly --doSyst --split 30" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_CorrPlot & </pre>


# Diagnostic Syst for BkgOnly Fit 
- Less Compress
<pre> nohup HistFitter.py -D systematics -F bkg -u"--doBkgOnly --doSyst --split 100" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_BkgSystPlots & </pre>

- More Compressed
<pre> nohup HistFitter.py -D systematics -F bkg -u"--doBkgOnly --doSyst --split 30" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_BkgSystPlots & </pre>


# Make Plots for BkgOnly: this creates root files for the channels
- Enter in Plots folder and create inside a folder with the same name
<pre> cd Plots </pre>
<pre> mkdir Plots </pre>


- Run script to write HF commands to run plots
<pre> python write_commands.py </pre>

- Run script to make plots with fancy cosmetics
<pre> python run_MultiBinPlot.py </pre>

# Model dependent ULs - Grey numbers on top of the exclusion contours in 2D
- Produce upperlimit root files
<pre> python script_to_compute_pval.py --make_upperlimits </pre>
- Hadd the upper limits root files
<pre> python script_to_compute_pval.py --hadd_upperlimits </pre>
- Convert the combined root file into a JSON
<pre> python script_to_compute_pval.py --generate_JSON_uls </pre>
- Convert the ULs on mu to ULs on xsec
<pre> python  script_transform_ULs.py </pre>
- Generate an outputGraphs.root with ULs
<pre> python script_to_compute_pval.py --make_harvest_uls </pre>
- Give it a different name
<pre> mv outputGraphs.root outputGraphs_withULs.root </pre>
- Show the ULs on the xsec as the grey values on top of the exclusion contours
<pre> python contourPlotterExample.py -i outputGraphs.root -g UL -iUL outputGraphs_withULs.root </pre>

# Model dependent ULs - 1D scan
When running the upper limits, the xsec sigma variation is removed from the fit parameters. To produce the theory variations, switch fixXsec to False and manually scale the cross section the the nominal, up and down cases. Set by hand the ULs on mu in UpperLimit_DM100.C in UpperLimitsSignalStrength folder and run contourPlotterExample.py.


