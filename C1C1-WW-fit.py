#  Charginos through Ws 2nd wave analysis  (eric.ballabene@cern.ch)

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from ROOT import TCanvas

from copy import deepcopy

from optparse import OptionParser 
myUserArgs = configMgr.userArg.split(' ')
myInputParser = OptionParser()
myInputParser.add_option("--doBkgOnly", action='store_true', default=False, help='Bkg Only Fit')
myInputParser.add_option("--doExclusion", action='store_true', default=False, help='Exclusion model-dependent Fit - set automatically by default')
myInputParser.add_option("--doDiscovery", action='store_true', default=False, help='Discovery model-independent fit')
myInputParser.add_option("--doSyst", action='store_true', default=False, help='Do systematics')
myInputParser.add_option("-s", "--split", dest='split', default='0', help="DeltaM (Mass splitting) to run Bkg/Validation.")
myInputParser.add_option("--doBackupCacheFile", action='store_true', default=False, help='BackupCacheFile')
myInputParser.add_option("--doValidationPlots", action='store_true', default=False, help='Validation plots')
myInputParser.add_option("--VariableToPlot", default='lep1pT', help='Variable to plot')
myInputParser.add_option("--ChannelToPlot", default='CR_Dib', help='Channel to plot')
myInputParser.add_option("--PlotDictionary", default='preselection', help='Dictionary of the plots setting bins')
myInputParser.add_option("--doTestSystematics", action='store_true', default=False, help='Apply a test systematics')
myInputParser.add_option('--point', dest = 'point', default = '0', help = "Specify a signal point")
myInputParser.add_option('--discoveryRegion', dest = 'discoveryRegion', default = '', help = "Discovery region where the disc fit is run. By default, performed in all discovery SRs")
myInputParser.add_option("--CRonlyFit", action='store_true', default=False, help='Bkg Only Fit in only CRs')
(options, args) = myInputParser.parse_args(myUserArgs)
doSyst = options.doSyst
doBkgOnly = options.doBkgOnly
doExclusion = options.doExclusion
doDiscovery= options.doDiscovery
doValidationPlots= options.doValidationPlots
VariableToPlot= []
VariableToPlot= options.VariableToPlot.split(",")
ChannelToPlot= []
ChannelToPlot= options.ChannelToPlot.split(",")
PlotDictionary= options.PlotDictionary
doTestSystematics = options.doTestSystematics
whichPoint = options.point
split= options.split
discoveryRegion = []
if options.discoveryRegion != "":
   discoveryRegion=options.discoveryRegion.split(",")
doBackupCacheFile = options.doBackupCacheFile
CRonlyFit = options.CRonlyFit
print(options,args)


#If no fit is selected as additional argument, an exclusion fit runs by default
if doBkgOnly == False and doDiscovery == False:
    doExclusion = True

if doBkgOnly == True:
    fit_type = 'BkgOnly'
    print("********Bkg Only Fit********")
elif doExclusion == True:
    fit_type = 'Exclusion'
    print("********Exclusion Fit********")
elif doDiscovery == True:
    fit_type = 'Discovery'
    print("********Discovery Fit********")

if doSyst == True:
    syst_type = 'withSyst'
    print("********Including Systematics********")
elif doSyst == False:
    syst_type = 'noSyst'
    print("********Excluding Systematics********")
    
if split == '30':
    print("***** Selected DM30 from myInputParser*******")
elif split == '100':
    print("***** Selected DM100 from myInputParser*******")
else:
    print("***** NO DM selected ******")


if doBackupCacheFile == True:
    print("*****Using BackupCacheFile*******")

if CRonlyFit == True and doBkgOnly != True:
    raise Exception("*****BAD CHOICE - BREAK*******")
    

# Force SR to be treated as SR also in background fit
#configMgr.keepSignalRegionType = True

# From HF tag v.0.65 - normalized histos from the cachefile are rebuilt, unless forceNorm is set to False
configMgr.forceNorm = True

useStat = True
configMgr.statErrThreshold = 0.05

# First define HistFactory attributes
if doBkgOnly == True and split == '30':
    configMgr.analysisName = fit_type + "_" + syst_type  + "_"  + whichPoint + "_"+ split
elif doDiscovery == True and split == '30':
    configMgr.analysisName = fit_type + "_" + syst_type  + "_" + whichPoint + "_"+ split
else:
    configMgr.analysisName = fit_type + "_" + syst_type + "_" + whichPoint
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"

#configMgr.histCacheFile = "data/BkgOnly_noSyst_0.root"

## Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 1. # Luminosity of input TTree after weighting
lumi = 138950.
configMgr.outputLumi = lumi # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")

configMgr.blindSR = False
configMgr.blindCR = False
configMgr.blindVR = False

configMgr.ReduceCorrMatrix = True

## setting the parameters of the hypothesis test
configMgr.nTOYs=5000 #5000 default, 20000 better
configMgr.fixSigXSec=True  # fix SigXSec: 0, +/-1sigma 
configMgr.calculatorType=2 # 2=asymptotic calculator, 0=frequentist calculator using toys
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20      # number of values scanned of signal-strength for upper-limit determination of signal strength.

## set scan range for the upper limit
#configMgr.scanRange = (0., 1.)
#configMgr.scanRange = (0., 100.) for SRD_SF0J

## Suffix of nominal tree
if doSyst == True:
    configMgr.nomName = "_WEIGHTS"
elif doSyst == False:
    configMgr.nomName = "_WEIGHTS"


#configMgr.prun = True
#configMgr.prunThreshold = 0.01


weights = ["WeightEvents","xsec","WeightLumi","WeightEventsPU","WeightEventselSF","WeightEventsmuSF","WeightEvents_trigger_single","WeightEventsJVT","WeightEventsbTag"]
configMgr.weights = (weights)


# All grid points 
sigSamples_grid=["100_1","100_10","100_20","100_30","100_40","100_50","100_60","100_70","100_80","100_90","110_20","125_1","125_25","125_35","125_45","125_55","125_65","125_75","125_85","125_95","125_105","125_115","130_20","140_20","140_40","150_1","150_25","150_40","150_50","150_60","150_70","150_80","150_90","150_100","150_110","150_120","150_130","150_140","160_1","160_30","175_1","175_25","175_50","175_75","175_85","175_95","175_105","175_115","175_125","175_135","175_145","175_155","175_165","200_1","200_25","200_50","200_75","200_100","200_110","200_120","200_130","200_140","200_150","200_160","200_170","200_180","200_190","225_100","225_125","250_125","250_150","250_160","250_170","250_180","250_190","250_200","250_210","250_220","250_230","250_240","300_125","300_150","300_175","300_200","300_220","300_230","300_240","350_125","350_150","350_175","350_200","400_100","400_125","400_150","400_200"]

#SigSamples ordered for Different Mass (DM) splitting
sigSamples_DM10  =["100_90",            "125_115",            "150_140","175_165","200_190",                  "250_240"]
sigSamples_DM20  =["100_80",            "125_105",            "150_130","175_155","200_180",                  "250_230"]
sigSamples_DM30  =["100_70",            "125_95",            "150_120","175_145","200_170",                  "250_220"]
sigSamples_DM40  =["100_60",            "125_85",            "150_110","175_135","200_160",                  "250_210"]
sigSamples_DM50  =["100_50",            "125_75",            "150_100","175_125","200_150",                  "250_200"]
sigSamples_DM60  =["100_40",            "125_65",              "150_90","175_115","200_140",                  "250_190","300_240"]
sigSamples_DM70  =["100_30",            "125_55",              "150_80","175_105","200_130",                  "250_180","300_230"]
sigSamples_DM80  =["100_20",            "125_45",              "150_70","175_95"  ,"200_120",                  "250_170","300_220"]
sigSamples_DM90  =["100_10",  "110_20", "125_35",              "150_60","175_85"  ,"200_110",                  "250_160"]
sigSamples_DM100=["100_1"  ,            "125_25",  "140_20", "150_50","175_75"  ,"200_100","225_125", "250_150","300_200"]
sigSamples_DM110=                     ["130_20",         "150_40"]
sigSamples_DM120=                     ["140_20"]
sigSamples_DM125=                  ["125_1" ,  "150_25","175_50"  , "200_75","225_100", "250_125","300_175"]
sigSamples_DM130=                     ["160_30"]
sigSamples_DM150=                                   ["150_1"  ,"175_25"  , "200_50",                                    "300_150", "350_200"]
sigSamples_DM160=                                        ["160_1"]
sigSamples_DM175=                                                   ["175_1"   , "200_25",                                    "300_125", "350_175"]
sigSamples_DM200=                                                                    ["200_1"   ,                                                      "350_150","400_200"]
sigSamples_DM225=                                                                                                                                           ["350_125"]
sigSamples_DM250=                                                                                                                                                            ["400_150"]
sigSamples_DM275=                                                                                                                                                            ["400_125"]
sigSamples_DM300=                                                                                                                                                            ["400_100"]

sigSamples_BRtoCorrect=["350_125","350_150","350_175","350_200","400_100","400_125","400_150","400_200"]

#### Signal theory uncertainty provided for three set of points
sigSamples_SignalTheorySyst125_25=["100_1","100_10","100_20","100_30","100_40","100_50","100_60","100_70","100_80","100_90","110_20","125_1","125_25","125_35","125_45","125_55","125_65","125_75","125_85","125_95","125_105","125_115","130_20","140_20","140_40"]
sigSamples_SignalTheorySyst150_60=["150_1","150_25","150_40","150_50","150_60","150_70","150_80","150_90","150_100","150_110","150_120","150_130","150_140","160_1","160_30"]
sigSamples_SignalTheorySyst175_50=["175_1","175_25","175_50","175_75","175_85","175_95","175_105","175_115","175_125","175_135","175_145","175_155","175_165","200_1","200_25","200_50","200_75","200_100","200_110","200_120","200_130","200_140","200_150","200_160","200_170","200_180","200_190","225_100","225_125","250_125","250_150","250_160","250_170","250_180","250_190","250_200","250_210","250_220","250_230","250_240","300_125","300_150","300_175","300_200","300_220","300_230","300_240","350_125","350_150","350_175","350_200","400_100","400_125","400_150","400_200"]


#### Uncertainty on the xsec to produce theory bands
XsecUnc = {}
XsecUnc['100'] = 518.613/11611.9
XsecUnc['110'] = 0.0464758
XsecUnc['125'] = 249.469/5090.52
XsecUnc['130'] = 0.0498373
XsecUnc['140'] = 0.0514004
XsecUnc['150'] = 138.156/2612.31
XsecUnc['160'] = 0.0541918
XsecUnc['175'] = 83.2672/1482.42
XsecUnc['200'] = 53.7411/902.569
XsecUnc['225'] = 36.0699/579.564
XsecUnc['250'] = 25.3131/387.534
XsecUnc['300'] = 13.4438/190.159
XsecUnc['350'] = 7.75261/102.199
XsecUnc['400'] = 4.7276/58.6311
sigSamples_C1C1_100=["100_1","100_10","100_20","100_30","100_40","100_50","100_60","100_70","100_80","100_90"]
sigSamples_C1C1_110=["110_20"]
sigSamples_C1C1_125=["125_1","125_25","125_35","125_45","125_55","125_65","125_75","125_85","125_95","125_105","125_115"]
sigSamples_C1C1_130=["130_20"]
sigSamples_C1C1_140=["140_20","140_40"]
sigSamples_C1C1_150=["150_1","150_25","150_40","150_50","150_60","150_70","150_80","150_90","150_100","150_110","150_120","150_130","150_140"]
sigSamples_C1C1_160=["160_1","160_30"]
sigSamples_C1C1_175=["175_1","175_25","175_50","175_75","175_85","175_95","175_105","175_115","175_125","175_135","175_145","175_155","175_165"]
sigSamples_C1C1_200=["200_1","200_25","200_50","200_75","200_100","200_110","200_120","200_130","200_140","200_150","200_160","200_170","200_180","200_190"]
sigSamples_C1C1_225=["225_100","225_125"]
sigSamples_C1C1_250=["250_125","250_150","250_160","250_170","250_180","250_190","250_200","250_210","250_220","250_230","250_240"]
sigSamples_C1C1_300=["300_125","300_150","300_175","300_200","300_220","300_230","300_240"]
sigSamples_C1C1_350=["350_125","350_150","350_175","350_200"]
sigSamples_C1C1_400=["400_100","400_125","400_150","400_200"]


# Setting 'split' parameter for the exclusion fit according to whichpoint is set. 
# NOTE: for the BkgOnly you need to specify it from inputParser. 
if doExclusion == True:
    if whichPoint in sigSamples_DM10 or whichPoint in sigSamples_DM20 or whichPoint in sigSamples_DM30 or whichPoint in sigSamples_DM40 or whichPoint in sigSamples_DM50:
        split='30'
    else:
        split='100'

if doBackupCacheFile == True:
    configMgr.useCacheToTreeFallback = True # enable the fallback to trees
    configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
    if split=='30':
        configMgr.histBackupCacheFile =  "data/BkgOnly_withSyst_0_30.root" # the data file of your previous fit (= the backup cache file)
    else:
        configMgr.histBackupCacheFile =  "/users2/ballaben/c1c1-ww-hf/unblinded_results/BkgOnlyFit/data/BkgOnly_withSyst_0.root"



############################################################
#      SRs
############################################################

SR_DF0J_preselection = 'isSF==0 & njet==0 & nbjet==0 & METsig > 8 & MT2 > 50 & ' #lep1pT > 27 GeV already applied
SR_SF0J_preselection = 'isSF==1 & njet==0 & nbjet==0 & METsig > 8 & MT2 > 50 & '  #lep1pT > 27 GeV already applied

configMgr.cutsDict['SR_DF0J_81_8125'] =   SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.81 & BDTDeltaM100_90 <= 0.8125'
configMgr.cutsDict['SR_DF0J_8125_815'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.8125 & BDTDeltaM100_90 <= 0.815'
configMgr.cutsDict['SR_DF0J_815_8175'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.815 & BDTDeltaM100_90 <= 0.8175'
configMgr.cutsDict['SR_DF0J_8175_82'] =   SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.8175 & BDTDeltaM100_90 <= 0.82'
configMgr.cutsDict['SR_DF0J_82_8225'] =   SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.82 & BDTDeltaM100_90 <= 0.8225'
configMgr.cutsDict['SR_DF0J_8225_825'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.8225 & BDTDeltaM100_90 <= 0.825'
configMgr.cutsDict['SR_DF0J_825_8275'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.825 & BDTDeltaM100_90 <= 0.8275'
configMgr.cutsDict['SR_DF0J_8275_83'] =   SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.8275 & BDTDeltaM100_90 <= 0.83'
configMgr.cutsDict['SR_DF0J_83_8325'] =   SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.83 & BDTDeltaM100_90 <= 0.8325'
configMgr.cutsDict['SR_DF0J_8325_835'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.8325 & BDTDeltaM100_90 <= 0.835'
configMgr.cutsDict['SR_DF0J_835_8375'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.835 & BDTDeltaM100_90 <= 0.8375'
configMgr.cutsDict['SR_DF0J_8375_84'] =   SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.8375 & BDTDeltaM100_90 <= 0.84'
configMgr.cutsDict['SR_DF0J_84_845'] =     SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.84 & BDTDeltaM100_90 <= 0.845'
configMgr.cutsDict['SR_DF0J_845_85'] =     SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.845 & BDTDeltaM100_90 <= 0.85'
configMgr.cutsDict['SR_DF0J_85_86'] =       SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.85 & BDTDeltaM100_90 <= 0.86'
configMgr.cutsDict['SR_DF0J_86'] =             SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.86'

configMgr.cutsDict['SR_SF0J_77_775'] =     SR_SF0J_preselection + 'BDTDeltaM100_90 > 0.77 & BDTDeltaM100_90 <= 0.775 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SR_SF0J_775_78'] =     SR_SF0J_preselection + 'BDTDeltaM100_90 > 0.775 & BDTDeltaM100_90 <= 0.78 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SR_SF0J_78_785'] =     SR_SF0J_preselection + 'BDTDeltaM100_90 > 0.78 & BDTDeltaM100_90 <= 0.785 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SR_SF0J_785_79'] =     SR_SF0J_preselection + 'BDTDeltaM100_90 > 0.785 & BDTDeltaM100_90 <= 0.79 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SR_SF0J_79_795'] =     SR_SF0J_preselection + 'BDTDeltaM100_90 > 0.79 & BDTDeltaM100_90 <= 0.795 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SR_SF0J_795_80'] =     SR_SF0J_preselection + 'BDTDeltaM100_90 > 0.795 & BDTDeltaM100_90 <= 0.80 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SR_SF0J_80_81'] =       SR_SF0J_preselection + 'BDTDeltaM100_90 > 0.80 & BDTDeltaM100_90 <= 0.81 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SR_SF0J_81'] =             SR_SF0J_preselection + 'BDTDeltaM100_90 > 0.81 & BDTothersDeltaM100_90 <= 0.01'


fit_DM100=['SR_DF0J_81_8125','SR_DF0J_8125_815','SR_DF0J_815_8175','SR_DF0J_8175_82','SR_DF0J_82_8225','SR_DF0J_8225_825','SR_DF0J_825_8275','SR_DF0J_8275_83',
'SR_DF0J_83_8325','SR_DF0J_8325_835','SR_DF0J_835_8375','SR_DF0J_8375_84','SR_DF0J_84_845','SR_DF0J_845_85','SR_DF0J_85_86','SR_DF0J_86',
'SR_SF0J_77_775','SR_SF0J_775_78','SR_SF0J_78_785','SR_SF0J_785_79','SR_SF0J_79_795','SR_SF0J_795_80','SR_SF0J_80_81','SR_SF0J_81']

fit_SF0J_DM100=['SR_SF0J_77_775','SR_SF0J_775_78','SR_SF0J_78_785','SR_SF0J_785_79','SR_SF0J_79_795','SR_SF0J_795_80','SR_SF0J_80_81','SR_SF0J_81']




######## fit dm30
configMgr.cutsDict['SR_DF1J_0_DM30'] = 'lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.40 & BDTDeltaM30 <= 0.45'
configMgr.cutsDict['SR_DF1J_1_DM30'] = 'lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.45 & BDTDeltaM30 <= 0.50'
configMgr.cutsDict['SR_DF1J_2_DM30'] = 'lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.50 & BDTDeltaM30 <= 0.55'
configMgr.cutsDict['SR_DF1J_3_DM30'] = 'lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.55 & BDTDeltaM30 <= 0.60'
configMgr.cutsDict['SR_DF1J_4_DM30'] = 'lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.60 & BDTDeltaM30 <= 0.65'
configMgr.cutsDict['SR_DF1J_5_DM30'] = 'lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.65 & BDTDeltaM30 <= 0.70'
configMgr.cutsDict['SR_DF1J_6_DM30'] = 'lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.70 & BDTDeltaM30 <= 0.75'
configMgr.cutsDict['SR_DF1J_7_DM30'] = 'lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.75 & BDTDeltaM30 <= 0.80'
configMgr.cutsDict['SR_DF1J_8_DM30'] = 'lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.80'

configMgr.cutsDict['SR_SF1J_0_DM30'] = 'lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.40 & BDTDeltaM30 <= 0.45'
configMgr.cutsDict['SR_SF1J_1_DM30'] = 'lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.45 & BDTDeltaM30 <= 0.50'
configMgr.cutsDict['SR_SF1J_2_DM30'] = 'lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.50 & BDTDeltaM30 <= 0.55'
configMgr.cutsDict['SR_SF1J_3_DM30'] = 'lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.55 & BDTDeltaM30 <= 0.60'
configMgr.cutsDict['SR_SF1J_4_DM30'] = 'lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.60 & BDTDeltaM30 <= 0.65'
configMgr.cutsDict['SR_SF1J_5_DM30'] = 'lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.65 & BDTDeltaM30 <= 0.70'
configMgr.cutsDict['SR_SF1J_6_DM30'] = 'lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.70 & BDTDeltaM30 <= 0.75'
configMgr.cutsDict['SR_SF1J_7_DM30'] = 'lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.75 & BDTDeltaM30 <= 0.80'
configMgr.cutsDict['SR_SF1J_8_DM30'] = 'lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.80'

fit_DM30=['SR_DF1J_0_DM30','SR_DF1J_1_DM30','SR_DF1J_2_DM30','SR_DF1J_3_DM30','SR_DF1J_4_DM30','SR_DF1J_5_DM30','SR_DF1J_6_DM30','SR_DF1J_7_DM30','SR_DF1J_8_DM30','SR_SF1J_0_DM30','SR_SF1J_1_DM30','SR_SF1J_2_DM30','SR_SF1J_3_DM30','SR_SF1J_4_DM30','SR_SF1J_5_DM30','SR_SF1J_6_DM30','SR_SF1J_7_DM30','SR_SF1J_8_DM30']


############################################################################
#      CRs
############################################################################

#DM100_90 standard
configMgr.cutsDict['CR_Dib']= 'njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && BDTDeltaM100_90 > 0.2 && BDTDeltaM100_90 <= 0.65 &&  BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1 && ( ( isSF == 0) ||  ( isSF == 1 && BDTothersDeltaM100_90 < 0.01 )   ) '
configMgr.cutsDict['CR_Top']= 'njet==1 && nbjet==1 && METsig > 8 && MT2 > 50 && ( (isSF==0 && BDTDeltaM100_90 > 0.5 && BDTDeltaM100_90 <= 0.7) || (isSF==1 && BDTDeltaM100_90 > 0.7 && BDTDeltaM100_90 <= 0.75 && BDTothersDeltaM100_90 < 0.01) )'



####DM30
configMgr.cutsDict['CR_VV_DF1J_DM30']= 'isSF==0  &&  njet==1  &&  nbjet==0  && BDTDeltaM30 > 0.05 && BDTDeltaM30 <= 0.2 && METsig > 8 && BDTVVDeltaM30 > 0.5'
configMgr.cutsDict['CR_Top_DF_DM30']= 'isSF==0  &&  njet==1  && nbjet==1  &&  BDTDeltaM30 > 0.05 && BDTDeltaM30 <= 0.2 && METsig > 8 && BDTtopDeltaM30 > 0.5'


############################################################################
#      VRs
############################################################################

#DM100
configMgr.cutsDict['VR_Dib_DF0J']= 'njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && isSF == 0 && BDTDeltaM100_90 > 0.65 && BDTDeltaM100_90 <= 0.81 && BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1'
configMgr.cutsDict['VR_Dib_SF0J']= 'njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && isSF == 1 && BDTDeltaM100_90 > 0.65 && BDTDeltaM100_90 <= 0.77 && BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1 && BDTothersDeltaM100_90 < 0.01'

configMgr.cutsDict['VR_Top_DF1J']= 'njet==1 && nbjet==1 && METsig > 8 && MT2 > 50 && isSF== 0 && BDTDeltaM100_90 > 0.7  && BDTDeltaM100_90 <= 1.0'
configMgr.cutsDict['VR_Top_SF1J']= 'njet==1 && nbjet==1 && METsig > 8 && MT2 > 50 && isSF== 1 && BDTDeltaM100_90 > 0.75 && BDTDeltaM100_90 <= 1.0 && BDTothersDeltaM100_90 < 0.01'

configMgr.cutsDict['VR_Top_DF0J']= 'njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && isSF== 0 && BDTDeltaM100_90 > 0.5 && BDTDeltaM100_90 <= 0.81 && BDTVVDeltaM100_90 <= 0.15'
configMgr.cutsDict['VR_Top_SF0J']= 'njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && isSF== 1 && BDTDeltaM100_90 > 0.5 && BDTDeltaM100_90 <= 0.77 && BDTVVDeltaM100_90 <= 0.15 && BDTothersDeltaM100_90 < 0.01'

configMgr.cutsDict['VR_DF0J_reversedMETsig']= 'isSF==0 && njet==0 && nbjet==0 && METsig < 8 && MT2 > 50'
configMgr.cutsDict['VR_SF0J_reversedMETsig']= 'isSF==1 && njet==0 && nbjet==0 && METsig < 8 && MT2 > 50  && BDTothersDeltaM100_90 < 0.01'

configMgr.cutsDict['DF0J_preselection']= 'isSF==0 && njet==0 && nbjet==0'
configMgr.cutsDict['SF0J_preselection']= 'isSF==1 && njet==0 && nbjet==0'

####DM30
configMgr.cutsDict['VR_VV_DF1J_DM30']= 'lep1pT > 27 && isSF==0  &&  njet==1  &&  nbjet==0  && BDTDeltaM30 > 0.2 && BDTDeltaM30 <= 0.4 && METsig > 8 && BDTVVDeltaM30 > 0.5'
configMgr.cutsDict['VR_VV_SF1J_DM30']= 'isSF==  &&  njet==1  &&  nbjet==0  && BDTDeltaM30 > 0.2 && BDTDeltaM30 <= 0.4 && METsig > 8 && BDTVVDeltaM30 > 0.5 && BDTothersDeltaM30>0.023'

## AUX material
configMgr.cutsDict['VR_Dib_DF0J_SR_DF0J']= 'njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && ( (isSF == 0 && BDTDeltaM100_90 > 0.65 && BDTDeltaM100_90 <= 0.81 && BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1)  || (isSF == 0 && BDTDeltaM100_90 > 0.81) )'
configMgr.cutsDict['VR_Dib_SF0J_SR_SF0J']= 'njet==0 && nbjet==0 && METsig > 8 && MT2 > 50 && ( (isSF == 1 && BDTDeltaM100_90 > 0.65 && BDTDeltaM100_90 <= 0.77 && BDTVVDeltaM100_90 > 0.2 && BDTtopDeltaM100_90 < 0.1 && BDTothersDeltaM100_90 < 0.01) || (isSF == 1 && BDTDeltaM100_90 > 0.77 && BDTothersDeltaM100_90 < 0.01) )'


############################################################################
#                      SYSTEMATICS
############################################################################

TestSystematic = Systematic("ucb", None, 1.2, 0.8, "user", "userOverallSys") #fake syst, 'None' can be switched to 'configMgr.weights' (nothing happens)

TestSystematic_14corr = Systematic("TestSystematic_14corr", None, 1.14, 0.86, "user", "shapeSys") #fake syst 
TestSystematic_14uncorr = Systematic("TestSystematic_14uncorr", None, 1.14, 0.86, "user", "overallSys") #fake syst 


######################### Theorethical systematics ################
# theory syst # VV 
thSyst_ckkw_VV = Systematic("CKKW_VV","_TRUTH","_TRUTH_ckkwUp","_TRUTH_ckkwDown","tree","normHistoSys")
thSyst_fac_VV = Systematic("Factorization_VV","_TRUTH","_TRUTH_facUp","_TRUTH_facDown","tree","normHistoSys")
thSyst_qsf_VV = Systematic("Resummation_VV","_TRUTH","_TRUTH_qsfUp","_TRUTH_qsfDown","tree","normHistoSys")
thSyst_ren_VV = Systematic("Renormalization_VV","_TRUTH","_TRUTH_renUp","_TRUTH_renDown","tree","normHistoSys")
thSyst_ren_fac_VV = Systematic("RenormalizationFactorization_VV","_TRUTH","_TRUTH_ren_fac_Up","_TRUTH_ren_fac_Down","tree","normHistoSys")
thSyst_PDF = Systematic("PDF_VV","_TRUTH","_TRUTH_PDF_Up","_TRUTH_PDF_Down","tree","normHistoSys")
thSystListVV=[thSyst_fac_VV,thSyst_ren_VV,thSyst_qsf_VV,thSyst_ckkw_VV,thSyst_ren_fac_VV,thSyst_PDF]
for syst in thSystListVV:
  syst.differentNominalTreeWeight = True 

# theory syst # ttbar
thSyst_fac_ttbar = Systematic("Factorization_ttbar","_TRUTH","_TRUTH_facUp","_TRUTH_facDown","tree","normHistoSys")
thSyst_gen_ttbar = Systematic("Generator_ttbar","_TRUTH","_TRUTH_gen","_TRUTH","tree","normHistoSysOneSide")
thSyst_rad_ttbar = Systematic("Radiation_ttbar","_TRUTH","_TRUTH_radUp","_TRUTH_radDown","tree","normHistoSys")
thSyst_ren_ttbar = Systematic("Renormalization_ttbar","_TRUTH","_TRUTH_renUp","_TRUTH_renDown","tree","normHistoSys")
thSyst_show_ttbar = Systematic("Shower_ttbar","_TRUTH","_TRUTH_show","_TRUTH","tree","normHistoSysOneSide")
thSystListTtbar=[thSyst_fac_ttbar,thSyst_gen_ttbar,thSyst_rad_ttbar,thSyst_ren_ttbar,thSyst_show_ttbar]
for syst in thSystListTtbar:
  syst.differentNominalTreeWeight = True

# theory syst # Wt
thSyst_DS_Wt = Systematic("DS_Singletop","_TRUTH","_TRUTH_DS","_TRUTH","tree","normHistoSysOneSide")
thSystListWt=[thSyst_DS_Wt]
for syst in thSystListWt:
  syst.differentNominalTreeWeight = True

# theory syst # Z+Jets
thSyst_ckkw_zjet = Systematic("CKKW_Zjets","_TRUTH","_TRUTH_ckkwUp","_TRUTH_ckkwDown","tree","histoSys")
thSyst_fac_zjet = Systematic("Factorization_Zjets","_TRUTH","_TRUTH_facUp","_TRUTH_facDown","tree","histoSys")
thSyst_qsf_zjet = Systematic("Resummation_Zjets","_TRUTH","_TRUTH_qsfUp","_TRUTH_qsfDown","tree","histoSys")
thSyst_ren_zjet = Systematic("Renormalization_Zjets","_TRUTH","_TRUTH_renUp","_TRUTH_renDown","tree","histoSys")
thSystListzjet=[thSyst_ckkw_zjet,thSyst_fac_zjet,thSyst_qsf_zjet,thSyst_ren_zjet]
for syst in thSystListzjet:
  syst.differentNominalTreeWeight = True





######################### Experimental systematics ################
systype="histoSys"
systype_oneSide="histoSysOneSideSym"
systype_norm="normHistoSys"
systype_normOneSide="normHistoSysOneSideSym"


def replaceWeight(oldList,oldWeight,newWeight):   
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

# Weights replacement
JVT_JET_JvtEfficiency_UP=replaceWeight(weights,"WeightEventsJVT","WeightEventsJVT_JET_JvtEfficiency__1up")
JVT_JET_JvtEfficiency_DOWN=replaceWeight(weights,"WeightEventsJVT","WeightEventsJVT_JET_JvtEfficiency__1down")
PileUp_UP=replaceWeight(weights,"WeightEventsPU","WeightEventsPU_PRW_DATASF__1up")
PileUp_DOWN=replaceWeight(weights,"WeightEventsPU","WeightEventsPU_PRW_DATASF__1down")
bTag_FT_EFF_B_systematics_UP=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_B_systematics__1up")
bTag_FT_EFF_C_systematics_UP=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_C_systematics__1up")
bTag_FT_EFF_Light_systematics_UP=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_Light_systematics__1up")
bTag_FT_EFF_extrapolation_from_charm_UP=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_extrapolation_from_charm__1up")
bTag_FT_EFF_extrapolation_UP=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_extrapolation__1up")
bTag_FT_EFF_B_systematics_DOWN=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_B_systematics__1down")
bTag_FT_EFF_C_systematics_DOWN=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_C_systematics__1down")
bTag_FT_EFF_Light_systematics_DOWN=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_Light_systematics__1down")
bTag_FT_EFF_extrapolation_DOWN=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_extrapolation__1down")
bTag_FT_EFF_extrapolation_from_charm_DOWN=replaceWeight(weights,"WeightEventsbTag","WeightEventsbTag_FT_EFF_extrapolation_from_charm__1down")
EL_EFF_Charge_UP=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up")
EL_EFF_ID_UP=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
EL_EFF_Iso_UP=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
EL_EFF_Reco_UP=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
EL_EFF_Trigger_UP=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up")
EL_EFF_Triggereff_UP=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up")
EL_EFF_Charge_DOWN=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down")
EL_EFF_ID_DOWN=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")
EL_EFF_Iso_DOWN=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")
EL_EFF_Reco_DOWN=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")
EL_EFF_Trigger_DOWN=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down")
EL_EFF_Triggereff_DOWN=replaceWeight(weights,"WeightEventselSF","WeightEventselSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down")
MUON_EFF_BADMUON_SYS_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_BADMUON_SYS__1up")
MUON_EFF_RECO_STAT_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_RECO_STAT__1up")
MUON_EFF_RECO_STAT_LOWPT_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_RECO_STAT_LOWPT__1up")
MUON_EFF_RECO_SYS_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_RECO_SYS__1up")
MUON_EFF_RECO_SYS_LOWPT_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_RECO_SYS_LOWPT__1up")
MUON_EFF_TTVA_STAT_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_TTVA_STAT__1up")
MUON_EFF_TTVA_SYS_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_TTVA_SYS__1up")
MUON_EFF_TrigStatUncertainty_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_TrigStatUncertainty__1up")
MUON_EFF_TrigSystUncertainty_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_TrigSystUncertainty__1up")
MUON_EFF_ISO_STAT_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_ISO_STAT__1up")
MUON_EFF_ISO_SYS_UP=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_ISO_SYS__1up")
MUON_EFF_BADMUON_SYS_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_BADMUON_SYS__1down")
MUON_EFF_RECO_STAT_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_RECO_STAT__1down")
MUON_EFF_RECO_STAT_LOWPT_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_RECO_STAT_LOWPT__1down")
MUON_EFF_RECO_SYS_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_RECO_SYS__1down")
MUON_EFF_RECO_SYS_LOWPT_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_RECO_SYS_LOWPT__1down")
MUON_EFF_TTVA_STAT_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_TTVA_STAT__1down")
MUON_EFF_TTVA_SYS_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_TTVA_SYS__1down")
MUON_EFF_TrigStatUncertainty_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_TrigStatUncertainty__1down")
MUON_EFF_TrigSystUncertainty_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_TrigSystUncertainty__1down")
MUON_EFF_ISO_STAT_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_ISO_STAT__1down")
MUON_EFF_ISO_SYS_DOWN=replaceWeight(weights,"WeightEventsmuSF","WeightEventsmuSF_MUON_EFF_ISO_SYS__1down")

FNP_TOTAL_UP=replaceWeight(weights,"WeightEvents","FNP_TOTAL_UP")
FNP_TOTAL_DOWN=replaceWeight(weights,"WeightEvents","FNP_TOTAL_DOWN")

'''
FNP_STATUP =  replaceWeight(weights,"WeightEvents","FNP_STATUP")
FNP_STATDOWN =  replaceWeight(weights,"WeightEvents","FNP_STATDW")
FNP_WEIGHTUP =  replaceWeight(weights,"WeightEvents","FNP_WEIGHTUP")
FNP_WEIGHTDOWN =  replaceWeight(weights,"WeightEvents","FNP_WEIGHTDW")
FNP_XSECUP =  replaceWeight(weights,"WeightEvents","FNP_XSECUP")
FNP_XSECDOWN =  replaceWeight(weights,"WeightEvents","FNP_XSECDW")
FNP_EFFTOTALUP =  replaceWeight(weights,"WeightEvents","FNP_EFF_TOTAL_1up")
FNP_EFFTOTALDOWN =  replaceWeight(weights,"WeightEvents","FNP_EFF_TOTAL_1down")
FNP_EFFTRIGTOTALUP =  replaceWeight(weights,"WeightEvents","FNP_EFF_TRIG_TOTAL_1up")
FNP_EFFTRIGTOTALDOWN =  replaceWeight(weights,"WeightEvents","FNP_EFF_TRIG_TOTAL_1down")
FNP_WEIGHTSYSUP =  replaceWeight(weights,"WeightEvents","FNP_WEIGHTSYSTUP")
FNP_WEIGHTSYSDOWN =  replaceWeight(weights,"WeightEvents","FNP_WEIGHTSYSTDW")
'''

############# Systematics for NOT normalized backgrounds ##################
EG_Res = Systematic("EG_RES","","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree",systype)
EG_Scale=Systematic("EG_SCALE","","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree",systype)

JET_EtaIntercalibration_highE=Systematic("JET_EtaIntercalibration_highE","","_JET_EtaIntercalibration_NonClosure_highE__1up","_JET_EtaIntercalibration_NonClosure_highE__1down","tree",systype)
JET_EtaIntercalibration_negEta=Systematic("JET_EtaIntercalibration_negEta","","_JET_EtaIntercalibration_NonClosure_negEta__1up","_JET_EtaIntercalibration_NonClosure_negEta__1down","tree",systype)
JET_EtaIntercalibration_posEta=Systematic("JET_EtaIntercalibration_posEta","","_JET_EtaIntercalibration_NonClosure_posEta__1up","_JET_EtaIntercalibration_NonClosure_posEta__1down","tree",systype)

JET_Flavor_Response=Systematic("JET_Flavor_Response","","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree",systype)
JET_Flavor_Composition=Systematic("JET_Flavor_Composition","","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree",systype)
jer_DataVsMC=Systematic("JER_DataVsMC","","_JET_JER_DataVsMC_MC16__1up","_JET_JER_DataVsMC_MC16__1down","tree",systype_oneSide)
jer_DataVsMC_AFII=Systematic("JER_DataVsMC","","_JET_JER_DataVsMC_AFII__1up","_JET_JER_DataVsMC_AFII__1down","tree",systype_oneSide)

jerNP1=Systematic("JER1","","_JET_JER_EffectiveNP_1__1up","_JET_JER_EffectiveNP_1__1down","tree",systype_oneSide)
jerNP2=Systematic("JER2","","_JET_JER_EffectiveNP_2__1up","_JET_JER_EffectiveNP_2__1down","tree",systype_oneSide)
jerNP3=Systematic("JER3","","_JET_JER_EffectiveNP_3__1up","_JET_JER_EffectiveNP_3__1down","tree",systype_oneSide)
jerNP4=Systematic("JER4","","_JET_JER_EffectiveNP_4__1up","_JET_JER_EffectiveNP_4__1down","tree",systype_oneSide)
jerNP5=Systematic("JER5","","_JET_JER_EffectiveNP_5__1up","_JET_JER_EffectiveNP_5__1down","tree",systype_oneSide)
jerNP6=Systematic("JER6","","_JET_JER_EffectiveNP_6__1up","_JET_JER_EffectiveNP_6__1down","tree",systype_oneSide)
jerNP7=Systematic("JER7","","_JET_JER_EffectiveNP_7restTerm__1up","_JET_JER_EffectiveNP_7restTerm__1down","tree",systype_oneSide)

met_Para=Systematic("MET_SoftTrk_ResoPara","","_MET_SoftTrk_ResoPara","_MET_SoftTrk_ResoPara","tree",systype_oneSide)
met_Perp=Systematic("MET_SoftTrk_ResoPerp","","_MET_SoftTrk_ResoPerp","_MET_SoftTrk_ResoPerp","tree",systype_oneSide)
met_Scale=Systematic("MET_SoftTrk_Scale","","_MET_SoftTrk_Scale__1up","_MET_SoftTrk_Scale__1down","tree",systype)

MUON_ID=Systematic("MUON_ID","","_MUON_ID__1up","_MUON_ID__1down","tree",systype)
MUON_MS=Systematic("MUON_MS","","_MUON_MS__1up","_MUON_MS__1down","tree",systype)
MUON_SAGITTA_RESBIAS=Systematic("MUON_SAGITTA_RESBIAS","","_MUON_SAGITTA_RESBIAS__1up","_MUON_SAGITTA_RESBIAS__1down","tree",systype)
MUON_SAGITTA_RHO=Systematic("MUON_SAGITTA_RHO","","_MUON_SAGITTA_RHO__1up","_MUON_SAGITTA_RHO__1down","tree",systype)
MUON_Scale=Systematic("MUON_SCALE","","_MUON_SCALE__1up","_MUON_SCALE__1down","tree",systype)

JET_PILEUP_OffsetMu=Systematic("JET_Pileup_OffsetMu","","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree",systype)
JET_PILEUP_OffsetNPV=Systematic("JET_Pileup_OffsetNPV","","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree",systype)
JET_PILEUP_PtTerm=Systematic("JET_Pileup_PtTerm","","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree",systype)
JET_PILEUP_RhoTopology=Systematic("JET_Pileup_RhoTopology","","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree",systype)
JET_PunchThrough_MC16=Systematic("JET_PunchThrough_MC16","","_JET_PunchThrough_MC16__1up","_JET_PunchThrough_MC16__1down","tree",systype)
JET_PunchThrough_AFII=Systematic("JET_PunchThrough_MC16","","_JET_PunchThrough_AFII__1up","_JET_PunchThrough_AFII__1down","tree",systype)

JET_Detector1=Systematic("JET_EffectiveNP_Detector1","","_JET_EffectiveNP_Detector1__1up","_JET_EffectiveNP_Detector1__1down","tree",systype)
JET_Detector2=Systematic("JET_EffectiveNP_Detector2","","_JET_EffectiveNP_Detector2__1up","_JET_EffectiveNP_Detector2__1down","tree",systype)
JET_Mixed1=Systematic("JET_EffectiveNP_Mixed1","","_JET_EffectiveNP_Mixed1__1up","_JET_EffectiveNP_Mixed1__1down","tree",systype)
JET_Mixed2=Systematic("JET_EffectiveNP_Mixed2","","_JET_EffectiveNP_Mixed2__1up","_JET_EffectiveNP_Mixed2__1down","tree",systype)
JET_Mixed3=Systematic("JET_EffectiveNP_Mixed3","","_JET_EffectiveNP_Mixed3__1up","_JET_EffectiveNP_Mixed3__1down","tree",systype)
JET_Modelling1=Systematic("JET_EffectiveNP_Modelling1","","_JET_EffectiveNP_Modelling1__1up","_JET_EffectiveNP_Modelling1__1down","tree",systype)
JET_Modelling2=Systematic("JET_EffectiveNP_Modelling2","","_JET_EffectiveNP_Modelling2__1up","_JET_EffectiveNP_Modelling2__1down","tree",systype)
JET_Modelling3=Systematic("JET_EffectiveNP_Modelling3","","_JET_EffectiveNP_Modelling3__1up","_JET_EffectiveNP_Modelling3__1down","tree",systype)
JET_Modelling4=Systematic("JET_EffectiveNP_Modelling4","","_JET_EffectiveNP_Modelling4__1up","_JET_EffectiveNP_Modelling4__1down","tree",systype)
JET_Statistical1=Systematic("JET_EffectiveNP_Statistical1","","_JET_EffectiveNP_Statistical1__1up","_JET_EffectiveNP_Statistical1__1down","tree",systype)
JET_Statistical2=Systematic("JET_EffectiveNP_Statistical2","","_JET_EffectiveNP_Statistical2__1up","_JET_EffectiveNP_Statistical2__1down","tree",systype)
JET_Statistical3=Systematic("JET_EffectiveNP_Statistical3","","_JET_EffectiveNP_Statistical3__1up","_JET_EffectiveNP_Statistical3__1down","tree",systype)
JET_Statistical4=Systematic("JET_EffectiveNP_Statistical4","","_JET_EffectiveNP_Statistical4__1up","_JET_EffectiveNP_Statistical4__1down","tree",systype)
JET_Statistical5=Systematic("JET_EffectiveNP_Statistical5","","_JET_EffectiveNP_Statistical5__1up","_JET_EffectiveNP_Statistical5__1down","tree",systype)
JET_Statistical6=Systematic("JET_EffectiveNP_Statistical6","","_JET_EffectiveNP_Statistical6__1up","_JET_EffectiveNP_Statistical6__1down","tree",systype)

pileup=Systematic("PILEUP",weights,PileUp_UP,PileUp_DOWN,"weight",systype)
ft_EFF_B=Systematic("FT_EFF_B",weights,bTag_FT_EFF_B_systematics_UP,bTag_FT_EFF_B_systematics_DOWN,"weight",systype)
ft_EFF_C=Systematic("FT_EFF_C",weights,bTag_FT_EFF_C_systematics_UP,bTag_FT_EFF_C_systematics_DOWN,"weight",systype)
ft_EFF_extr=Systematic("FT_EFF_extrapolation",weights,bTag_FT_EFF_extrapolation_UP,bTag_FT_EFF_extrapolation_DOWN,"weight",systype)
ft_EFF_extrFromCharm=Systematic("FT_EFF_extrFromCharm",weights,bTag_FT_EFF_extrapolation_from_charm_UP,bTag_FT_EFF_extrapolation_from_charm_DOWN,"weight",systype)
ft_EFF_Light=Systematic("FT_EFF_Light",weights,bTag_FT_EFF_Light_systematics_UP,bTag_FT_EFF_Light_systematics_DOWN,"weight",systype)
JVT=Systematic("JVT",weights,JVT_JET_JvtEfficiency_UP,JVT_JET_JvtEfficiency_DOWN,"weight",systype)
EL_EFF_Charge=Systematic("EL_EFF_Charge", weights,EL_EFF_Charge_UP,EL_EFF_Charge_DOWN , "weight", systype)
EL_EFF_ID=Systematic("EL_EFF_ID", weights,EL_EFF_ID_UP,EL_EFF_ID_DOWN , "weight", systype)
EL_EFF_Iso=Systematic("EL_EFF_Iso", weights,EL_EFF_Iso_UP,EL_EFF_Iso_DOWN , "weight", systype)
EL_EFF_Reco=Systematic("EL_EFF_Reco", weights,EL_EFF_Reco_UP,EL_EFF_Reco_DOWN , "weight", systype)
EL_EFF_Trigger=Systematic("EL_EFF_Trigger",weights,EL_EFF_Trigger_UP,EL_EFF_Trigger_DOWN , "weight", systype)
EL_EFF_Triggereff=Systematic("EL_EFF_Triggereff",weights,EL_EFF_Triggereff_UP,EL_EFF_Triggereff_DOWN ,"weight",systype)
MUON_EFF_BADMUON_SYS=Systematic( "MUON_EFF_BADMUON_SYS",weights,MUON_EFF_BADMUON_SYS_UP,MUON_EFF_BADMUON_SYS_DOWN,"weight",systype)
MUON_EFF_RECO_STAT=Systematic( "MUON_EFF_RECO_STAT",weights,MUON_EFF_RECO_STAT_UP,MUON_EFF_RECO_STAT_DOWN,"weight",systype)
MUON_EFF_RECO_STAT_LOWPT=Systematic(  "MUON_EFF_RECO_STAT_LOWPT",weights,MUON_EFF_RECO_STAT_LOWPT_UP,MUON_EFF_RECO_STAT_LOWPT_DOWN,"weight",systype)
MUON_EFF_RECO_SYS=Systematic( "MUON_EFF_RECO_SYS",weights,MUON_EFF_RECO_SYS_UP,MUON_EFF_RECO_SYS_DOWN,"weight",systype)
MUON_EFF_RECO_SYS_LOWPT=Systematic(  "MUON_EFF_RECO_SYS_LOWPT",weights,MUON_EFF_RECO_SYS_LOWPT_UP,MUON_EFF_RECO_SYS_LOWPT_DOWN,"weight",systype)
MUON_EFF_TTVA_STAT=Systematic(  "MUON_EFF_TTVA_STAT",weights,MUON_EFF_TTVA_STAT_UP,MUON_EFF_TTVA_STAT_DOWN,"weight",systype)
MUON_EFF_TTVA_SYS=Systematic(  "MUON_EFF_TTVA_SYS",weights,MUON_EFF_TTVA_SYS_UP,MUON_EFF_TTVA_SYS_DOWN,"weight",systype)
MUON_EFF_TrigStatUncertainty=Systematic(  "MUON_EFF_TrigStatUncertainty",weights,MUON_EFF_TrigStatUncertainty_UP,MUON_EFF_TrigStatUncertainty_DOWN,"weight",systype)
MUON_EFF_TrigSystUncertainty=Systematic(  "MUON_EFF_TrigSystUncertainty",weights,MUON_EFF_TrigSystUncertainty_UP,MUON_EFF_TrigSystUncertainty_DOWN,"weight",systype)
MUON_EFF_ISO_STAT=Systematic(  "MUON_EFF_ISO_STAT",weights,MUON_EFF_ISO_STAT_UP,MUON_EFF_ISO_STAT_DOWN,"weight",systype)
MUON_EFF_ISO_SYS=Systematic( "MUON_EFF_ISO_SYS",weights,MUON_EFF_ISO_SYS_UP,MUON_EFF_ISO_SYS_DOWN,"weight",systype)

#FNP_TOTAL_SYS=Systematic( "FNP_TOTAL_SYS",weights,FNP_TOTAL_UP,FNP_TOTAL_DOWN,"weight",systype)
'''
FNP_STAT            = Systematic("FNP_STAT",          weights, FNP_STATUP,                FNP_STATDOWN,              "weight","histoSys")
FNP_WEIGHT       = Systematic("FNP_WEIGHT",      weights, FNP_WEIGHTUP,          FNP_WEIGHTDOWN,          "weight","histoSys")
FNP_XSEC           = Systematic("FNP_XSEC",          weights, FNP_XSECUP,               FNP_XSECDOWN,              "weight","histoSys")
FNP_EFF              = Systematic("FNP_EFF",            weights, FNP_EFFTOTALUP,        FNP_EFFTOTALDOWN,       "weight","histoSys")
FNP_EFF_TRIG     = Systematic("FNP_EFF_TRIG",    weights, FNP_EFFTRIGTOTALUP, FNP_EFFTRIGTOTALDOWN, "weight","histoSys")
FNP_WEIGHTSYS = Systematic("FNP_WEIGHTSYS", weights, FNP_WEIGHTSYSUP,    FNP_WEIGHTSYSDOWN,     "weight","histoSys")
'''


##################### Systematics for normalized backgrounds ####################
EG_Res_norm=Systematic("EG_RES","","_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree",systype_norm)
EG_Scale_norm=Systematic("EG_SCALE","","_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree",systype_norm)

JET_EtaIntercalibration_highE_norm=Systematic("JET_EtaIntercalibration_highE","","_JET_EtaIntercalibration_NonClosure_highE__1up","_JET_EtaIntercalibration_NonClosure_highE__1down","tree",systype_norm)
JET_EtaIntercalibration_negEta_norm=Systematic("JET_EtaIntercalibration_negEta","","_JET_EtaIntercalibration_NonClosure_negEta__1up","_JET_EtaIntercalibration_NonClosure_negEta__1down","tree",systype_norm)
JET_EtaIntercalibration_posEta_norm=Systematic("JET_EtaIntercalibration_posEta","","_JET_EtaIntercalibration_NonClosure_posEta__1up","_JET_EtaIntercalibration_NonClosure_posEta__1down","tree",systype_norm)

JET_Flavor_Response_norm=Systematic("JET_Flavor_Response","","_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree",systype_norm)
JET_Flavor_Composition_norm=Systematic("JET_Flavor_Composition","","_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree",systype_norm)
jer_DataVsMC_norm=Systematic("JER_DataVsMC","","_JET_JER_DataVsMC_MC16__1up","_JET_JER_DataVsMC_MC16__1down","tree",systype_normOneSide)
jerNP1_norm=Systematic("JER1","","_JET_JER_EffectiveNP_1__1up","_JET_JER_EffectiveNP_1__1down","tree",systype_normOneSide)
jerNP2_norm=Systematic("JER2","","_JET_JER_EffectiveNP_2__1up","_JET_JER_EffectiveNP_2__1down","tree",systype_normOneSide)
jerNP3_norm=Systematic("JER3","","_JET_JER_EffectiveNP_3__1up","_JET_JER_EffectiveNP_3__1down","tree",systype_normOneSide)
jerNP4_norm=Systematic("JER4","","_JET_JER_EffectiveNP_4__1up","_JET_JER_EffectiveNP_4__1down","tree",systype_normOneSide)
jerNP5_norm=Systematic("JER5","","_JET_JER_EffectiveNP_5__1up","_JET_JER_EffectiveNP_5__1down","tree",systype_normOneSide)
jerNP6_norm=Systematic("JER6","","_JET_JER_EffectiveNP_6__1up","_JET_JER_EffectiveNP_6__1down","tree",systype_normOneSide)
jerNP7_norm=Systematic("JER7","","_JET_JER_EffectiveNP_7restTerm__1up","_JET_JER_EffectiveNP_7restTerm__1down","tree",systype_normOneSide)

met_Para_norm=Systematic("MET_SoftTrk_ResoPara","","_MET_SoftTrk_ResoPara","_MET_SoftTrk_ResoPara","tree",systype_normOneSide)
met_Perp_norm=Systematic("MET_SoftTrk_ResoPerp","","_MET_SoftTrk_ResoPerp","_MET_SoftTrk_ResoPerp","tree",systype_normOneSide)
met_Scale_norm=Systematic("MET_SoftTrk_Scale","","_MET_SoftTrk_Scale__1up","_MET_SoftTrk_Scale__1down","tree",systype_norm)

MUON_ID_norm=Systematic("MUON_ID","","_MUON_ID__1up","_MUON_ID__1down","tree",systype_norm)
MUON_MS_norm=Systematic("MUON_MS","","_MUON_MS__1up","_MUON_MS__1down","tree",systype_norm)
MUON_Scale_norm=Systematic("MUON_SCALE","","_MUON_SCALE__1up","_MUON_SCALE__1down","tree",systype_norm)
MUON_SAGITTA_RESBIAS_norm=Systematic("MUON_SAGITTA_RESBIAS","","_MUON_SAGITTA_RESBIAS__1up","_MUON_SAGITTA_RESBIAS__1down","tree",systype_norm)
MUON_SAGITTA_RHO_norm=Systematic("MUON_SAGITTA_RHO","","_MUON_SAGITTA_RHO__1up","_MUON_SAGITTA_RHO__1down","tree",systype_norm)

JET_PILEUP_OffsetMu_norm=Systematic("JET_Pileup_OffsetMu","","_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree",systype_norm)
JET_PILEUP_OffsetNPV_norm=Systematic("JET_Pileup_OffsetNPV","","_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree",systype_norm)
JET_PILEUP_PtTerm_norm=Systematic("JET_Pileup_PtTerm","","_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree",systype_norm)
JET_PILEUP_RhoTopology_norm=Systematic("JET_Pileup_RhoTopology","","_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree",systype_norm)
JET_PunchThrough_MC16_norm=Systematic("JET_PunchThrough_MC16","","_JET_PunchThrough_MC16__1up","_JET_PunchThrough_MC16__1down","tree",systype_norm)

JET_Detector1_norm=Systematic("JET_EffectiveNP_Detector1","","_JET_EffectiveNP_Detector1__1up","_JET_EffectiveNP_Detector1__1down","tree",systype_norm)
JET_Detector2_norm=Systematic("JET_EffectiveNP_Detector2","","_JET_EffectiveNP_Detector2__1up","_JET_EffectiveNP_Detector2__1down","tree",systype_norm)
JET_Mixed1_norm=Systematic("JET_EffectiveNP_Mixed1","","_JET_EffectiveNP_Mixed1__1up","_JET_EffectiveNP_Mixed1__1down","tree",systype_norm)
JET_Mixed2_norm=Systematic("JET_EffectiveNP_Mixed2","","_JET_EffectiveNP_Mixed2__1up","_JET_EffectiveNP_Mixed2__1down","tree",systype_norm)
JET_Mixed3_norm=Systematic("JET_EffectiveNP_Mixed3","","_JET_EffectiveNP_Mixed3__1up","_JET_EffectiveNP_Mixed3__1down","tree",systype_norm)
JET_Modelling1_norm=Systematic("JET_EffectiveNP_Modelling1","","_JET_EffectiveNP_Modelling1__1up","_JET_EffectiveNP_Modelling1__1down","tree",systype_norm)
JET_Modelling2_norm=Systematic("JET_EffectiveNP_Modelling2","","_JET_EffectiveNP_Modelling2__1up","_JET_EffectiveNP_Modelling2__1down","tree",systype_norm)
JET_Modelling3_norm=Systematic("JET_EffectiveNP_Modelling3","","_JET_EffectiveNP_Modelling3__1up","_JET_EffectiveNP_Modelling3__1down","tree",systype_norm)
JET_Modelling4_norm=Systematic("JET_EffectiveNP_Modelling4","","_JET_EffectiveNP_Modelling4__1up","_JET_EffectiveNP_Modelling4__1down","tree",systype_norm)
JET_Statistical1_norm=Systematic("JET_EffectiveNP_Statistical1","","_JET_EffectiveNP_Statistical1__1up","_JET_EffectiveNP_Statistical1__1down","tree",systype_norm)
JET_Statistical2_norm=Systematic("JET_EffectiveNP_Statistical2","","_JET_EffectiveNP_Statistical2__1up","_JET_EffectiveNP_Statistical2__1down","tree",systype_norm)
JET_Statistical3_norm=Systematic("JET_EffectiveNP_Statistical3","","_JET_EffectiveNP_Statistical3__1up","_JET_EffectiveNP_Statistical3__1down","tree",systype_norm)
JET_Statistical4_norm=Systematic("JET_EffectiveNP_Statistical4","","_JET_EffectiveNP_Statistical4__1up","_JET_EffectiveNP_Statistical4__1down","tree",systype_norm)
JET_Statistical5_norm=Systematic("JET_EffectiveNP_Statistical5","","_JET_EffectiveNP_Statistical5__1up","_JET_EffectiveNP_Statistical5__1down","tree",systype_norm)
JET_Statistical6_norm=Systematic("JET_EffectiveNP_Statistical6","","_JET_EffectiveNP_Statistical6__1up","_JET_EffectiveNP_Statistical6__1down","tree",systype_norm)

pileup_norm=Systematic("PILEUP",weights,PileUp_UP,PileUp_DOWN,"weight",systype_norm)
ft_EFF_B_norm=Systematic("FT_EFF_B",weights,bTag_FT_EFF_B_systematics_UP,bTag_FT_EFF_B_systematics_DOWN,"weight",systype_norm)
ft_EFF_C_norm=Systematic("FT_EFF_C",weights,bTag_FT_EFF_C_systematics_UP,bTag_FT_EFF_C_systematics_DOWN,"weight",systype_norm)
ft_EFF_extr_norm=Systematic("FT_EFF_extrapolation",weights,bTag_FT_EFF_extrapolation_UP,bTag_FT_EFF_extrapolation_DOWN,"weight",systype_norm)
ft_EFF_extrFromCharm_norm=Systematic("FT_EFF_extrFromCharm",weights,bTag_FT_EFF_extrapolation_from_charm_UP,bTag_FT_EFF_extrapolation_from_charm_DOWN,"weight",systype_norm)
ft_EFF_Light_norm=Systematic("FT_EFF_Light",weights,bTag_FT_EFF_Light_systematics_UP,bTag_FT_EFF_Light_systematics_DOWN,"weight",systype_norm)
JVT_norm=Systematic("JVT",weights,JVT_JET_JvtEfficiency_UP,JVT_JET_JvtEfficiency_DOWN,"weight",systype_norm)
EL_EFF_Charge_norm=Systematic("EL_EFF_Charge",weights,EL_EFF_Charge_UP,EL_EFF_Charge_DOWN ,"weight",systype_norm)
EL_EFF_ID_norm=Systematic("EL_EFF_ID", weights,EL_EFF_ID_UP,EL_EFF_ID_DOWN , "weight", systype_norm)
EL_EFF_Iso_norm=Systematic("EL_EFF_Iso", weights,EL_EFF_Iso_UP,EL_EFF_Iso_DOWN , "weight", systype_norm)
EL_EFF_Reco_norm=Systematic("EL_EFF_Reco",weights,EL_EFF_Reco_UP,EL_EFF_Reco_DOWN ,"weight",systype_norm)
EL_EFF_Trigger_norm=Systematic("EL_EFF_Trigger",weights,EL_EFF_Trigger_UP,EL_EFF_Trigger_DOWN ,"weight",systype_norm)
EL_EFF_Triggereff_norm=Systematic("EL_EFF_Triggereff",weights,EL_EFF_Triggereff_UP,EL_EFF_Triggereff_DOWN ,"weight",systype_norm)
MUON_EFF_BADMUON_SYS_norm=Systematic( "MUON_EFF_BADMUON_SYS",weights,MUON_EFF_BADMUON_SYS_UP,MUON_EFF_BADMUON_SYS_DOWN,"weight",systype_norm)
MUON_EFF_RECO_STAT_norm=Systematic( "MUON_EFF_RECO_STAT",weights,MUON_EFF_RECO_STAT_UP,MUON_EFF_RECO_STAT_DOWN,"weight",systype_norm)
MUON_EFF_RECO_STAT_LOWPT_norm=Systematic(  "MUON_EFF_RECO_STAT_LOWPT",weights,MUON_EFF_RECO_STAT_LOWPT_UP,MUON_EFF_RECO_STAT_LOWPT_DOWN,"weight",systype_norm)
MUON_EFF_RECO_SYS_norm=Systematic( "MUON_EFF_RECO_SYS",weights,MUON_EFF_RECO_SYS_UP,MUON_EFF_RECO_SYS_DOWN,"weight",systype_norm)
MUON_EFF_RECO_SYS_LOWPT_norm=Systematic(  "MUON_EFF_RECO_SYS_LOWPT",weights,MUON_EFF_RECO_SYS_LOWPT_UP,MUON_EFF_RECO_SYS_LOWPT_DOWN,"weight",systype_norm)
MUON_EFF_TTVA_STAT_norm=Systematic(  "MUON_EFF_TTVA_STAT",weights,MUON_EFF_TTVA_STAT_UP,MUON_EFF_TTVA_STAT_DOWN,"weight",systype_norm)
MUON_EFF_TTVA_SYS_norm=Systematic(  "MUON_EFF_TTVA_SYS",weights,MUON_EFF_TTVA_SYS_UP,MUON_EFF_TTVA_SYS_DOWN,"weight",systype_norm)
MUON_EFF_TrigStatUncertainty_norm=Systematic(  "MUON_EFF_TrigStatUncertainty",weights,MUON_EFF_TrigStatUncertainty_UP,MUON_EFF_TrigStatUncertainty_DOWN,"weight",systype_norm)
MUON_EFF_TrigSystUncertainty_norm=Systematic(  "MUON_EFF_TrigSystUncertainty",weights,MUON_EFF_TrigSystUncertainty_UP,MUON_EFF_TrigSystUncertainty_DOWN,"weight",systype_norm)
MUON_EFF_ISO_STAT_norm=Systematic(  "MUON_EFF_ISO_STAT",weights,MUON_EFF_ISO_STAT_UP,MUON_EFF_ISO_STAT_DOWN,"weight",systype_norm)
MUON_EFF_ISO_SYS_norm=Systematic( "MUON_EFF_ISO_SYS",weights,MUON_EFF_ISO_SYS_UP,MUON_EFF_ISO_SYS_DOWN,"weight",systype_norm)


# Background syst - removed MUON_EFF_BADMUON_SYS
syst_list=[EG_Res,EG_Scale,
JET_EtaIntercalibration_highE,JET_EtaIntercalibration_negEta,JET_EtaIntercalibration_posEta,JET_Flavor_Response,JET_Flavor_Composition,
jer_DataVsMC,jerNP1,jerNP2,jerNP3,jerNP4,jerNP5,jerNP6,jerNP7,
met_Para,met_Perp,met_Scale,
MUON_ID,MUON_MS,MUON_SAGITTA_RESBIAS,MUON_SAGITTA_RHO,MUON_Scale,
JET_PILEUP_OffsetMu,JET_PILEUP_OffsetNPV,JET_PILEUP_PtTerm,JET_PILEUP_RhoTopology,JET_PunchThrough_MC16,
JET_Detector1,JET_Detector2,JET_Mixed1,JET_Mixed2,JET_Mixed3,JET_Modelling1,JET_Modelling2,JET_Modelling3,JET_Modelling4,
JET_Statistical1,JET_Statistical2,JET_Statistical3,JET_Statistical4,JET_Statistical5,JET_Statistical6,
pileup,ft_EFF_B,ft_EFF_C,ft_EFF_extr,ft_EFF_extrFromCharm,ft_EFF_Light,JVT,
EL_EFF_Charge,EL_EFF_ID,EL_EFF_Iso,EL_EFF_Reco,EL_EFF_Trigger,EL_EFF_Triggereff,
MUON_EFF_RECO_STAT,MUON_EFF_RECO_STAT_LOWPT,MUON_EFF_RECO_SYS,MUON_EFF_RECO_SYS_LOWPT,MUON_EFF_TTVA_STAT,MUON_EFF_TTVA_SYS,MUON_EFF_TrigStatUncertainty,MUON_EFF_TrigSystUncertainty,MUON_EFF_ISO_STAT,MUON_EFF_ISO_SYS]

# Signal syst - instead of jer_DataVsMC we use jer_DataVsMC_AFII, instead of JET_PunchThrough_MC16 we use JET_PunchThrough_AFII
syst_signal_list=[EG_Res,EG_Scale,
JET_EtaIntercalibration_highE,JET_EtaIntercalibration_negEta,JET_EtaIntercalibration_posEta,JET_Flavor_Response,JET_Flavor_Composition,
jer_DataVsMC_AFII,jerNP1,jerNP2,jerNP3,jerNP4,jerNP5,jerNP6,jerNP7,
met_Para,met_Perp,met_Scale,
MUON_ID,MUON_MS,MUON_SAGITTA_RESBIAS,MUON_SAGITTA_RHO,MUON_Scale,
JET_PILEUP_OffsetMu,JET_PILEUP_OffsetNPV,JET_PILEUP_PtTerm,JET_PILEUP_RhoTopology,JET_PunchThrough_AFII,
JET_Detector1,JET_Detector2,JET_Mixed1,JET_Mixed2,JET_Mixed3,JET_Modelling1,JET_Modelling2,JET_Modelling3,JET_Modelling4,
JET_Statistical1,JET_Statistical2,JET_Statistical3,JET_Statistical4,JET_Statistical5,JET_Statistical6,
pileup,ft_EFF_B,ft_EFF_C,ft_EFF_extr,ft_EFF_extrFromCharm,ft_EFF_Light,JVT,
EL_EFF_Charge,EL_EFF_ID,EL_EFF_Iso,EL_EFF_Reco,EL_EFF_Trigger,EL_EFF_Triggereff,
MUON_EFF_RECO_STAT,MUON_EFF_RECO_STAT_LOWPT,MUON_EFF_RECO_SYS,MUON_EFF_RECO_SYS_LOWPT,MUON_EFF_TTVA_STAT,MUON_EFF_TTVA_SYS,MUON_EFF_TrigStatUncertainty,MUON_EFF_TrigSystUncertainty,MUON_EFF_ISO_STAT,MUON_EFF_ISO_SYS]

# Normalized background list
systnorm_list=[EG_Res_norm,EG_Scale_norm,
JET_EtaIntercalibration_highE_norm,JET_EtaIntercalibration_negEta_norm,JET_EtaIntercalibration_posEta_norm,JET_Flavor_Response_norm,JET_Flavor_Composition_norm,
jer_DataVsMC_norm,jerNP1_norm,jerNP2_norm,jerNP3_norm,jerNP4_norm,jerNP5_norm,jerNP6_norm,jerNP7_norm,
met_Para_norm,met_Perp_norm,met_Scale_norm,
MUON_ID_norm,MUON_MS_norm,MUON_SAGITTA_RESBIAS_norm,MUON_SAGITTA_RHO_norm,MUON_Scale_norm,
JET_PILEUP_OffsetMu_norm,JET_PILEUP_OffsetNPV_norm,JET_PILEUP_PtTerm_norm,JET_PILEUP_RhoTopology_norm,JET_PunchThrough_MC16_norm,
JET_Detector1_norm,JET_Detector2_norm,JET_Mixed1_norm,JET_Mixed2_norm,JET_Mixed3_norm,JET_Modelling1_norm,JET_Modelling2_norm,JET_Modelling3_norm,JET_Modelling4_norm,
JET_Statistical1_norm,JET_Statistical2_norm,JET_Statistical3_norm,JET_Statistical4_norm,JET_Statistical5_norm,JET_Statistical6_norm,
pileup_norm,ft_EFF_B_norm,ft_EFF_C_norm,ft_EFF_extr_norm,ft_EFF_extrFromCharm_norm,ft_EFF_Light_norm,JVT_norm,
EL_EFF_Charge_norm,EL_EFF_ID_norm,EL_EFF_Iso_norm,EL_EFF_Reco_norm,EL_EFF_Trigger_norm,EL_EFF_Triggereff_norm,
MUON_EFF_RECO_STAT_norm,MUON_EFF_RECO_STAT_LOWPT_norm,MUON_EFF_RECO_SYS_norm,MUON_EFF_RECO_SYS_LOWPT_norm,MUON_EFF_TTVA_STAT_norm,MUON_EFF_TTVA_SYS_norm,MUON_EFF_TrigStatUncertainty_norm,MUON_EFF_TrigSystUncertainty_norm,MUON_EFF_ISO_STAT_norm,MUON_EFF_ISO_SYS_norm]


# Ad-hoc systematics by Discovery fit in SF0J channels, to be manually added for specific samples in specific channels
EG_Res_SF0J_12 = Systematic("EG_RES", None, 1.0063, 0.9937, "user", systype)                        # +- 0.63%
met_Scale_SF0J_12 = Systematic("MET_SoftTrk_Scale", None, 1.333, 0.667, "user", systype)       # +- 33.3%
met_Perp_SF0J_12 = Systematic("MET_SoftTrk_ResoPerp", None, 1., 1., "user", systype)               # +- 0.0% no contribution!


#################################################################
#      Samples
#################################################################
top = Sample("ttbar",kAzure+1)
# there's always a split in this config... whatever the fit is!
if split=='100':
    top.setNormFactor("mu_top",1.,0.,5.)
    top.setNormRegions([('CR_Dib','cuts'),('CR_Top','cuts')])
elif split=='30' and doDiscovery != True:
    top.setNormFactor("mu_top",1.,0.,5.)
    top.setNormRegions([('CR_Dib_DM30','cuts'),('CR_Top_DM30','cuts')])
else: # by default, if split is 0
    top.setNormByTheory()
if doSyst == True:
    for syst in thSystListTtbar:
        top.addSystematic(syst)
    for syst in systnorm_list:    
        top.addSystematic(syst)
if doTestSystematics == True:
    top.addSystematic(TestSystematic_14corr)
    top.addSystematic(TestSystematic_14uncorr)
top.setStatConfig(useStat)

singletop = Sample("Wt",kOrange-9)
if split=='100':
    singletop.setNormFactor("mu_top",1.,0.,5.)
    singletop.setNormRegions([('CR_Dib','cuts'),('CR_Top','cuts')])
elif split=='30' and doDiscovery != True:
    singletop.setNormFactor("mu_top",1.,0.,5.)
    singletop.setNormRegions([('CR_Dib_DM30','cuts'),('CR_Top_DM30','cuts')])
else: # by default, if split is 0
    singletop.setNormByTheory()
if doSyst == True:
    for syst in thSystListWt:
        singletop.addSystematic(syst)
    for syst in syst_list:    
        singletop.addSystematic(syst)
if doTestSystematics == True:
    singletop.addSystematic(TestSystematic_14corr)
    singletop.addSystematic(TestSystematic_14uncorr)
singletop.setStatConfig(useStat)

Zjets = Sample("Zjets",kSpring+1)
Zjets.setNormByTheory()
if doSyst == True:
    for syst in thSystListzjet:
        Zjets.addSystematic(syst)
    for syst in syst_list:    
        Zjets.addSystematic(syst)
if doTestSystematics == True:
    Zjets.addSystematic(TestSystematic_14corr)
    Zjets.addSystematic(TestSystematic_14uncorr)
Zjets.setStatConfig(useStat)

Zttjets = Sample("Zttjets",kSpring+5)
Zttjets.setNormByTheory()
if doSyst == True:
    for syst in syst_list:    
        Zttjets.addSystematic(syst)
if doTestSystematics == True:
    Zttjets.addSystematic(TestSystematic_14corr)
    Zttjets.addSystematic(TestSystematic_14uncorr)
Zttjets.setStatConfig(useStat)

VV = Sample("VV",kSpring-1)
if split=='100':
    VV.setNormFactor("mu_VV",1.,0.,5.)
    VV.setNormRegions([('CR_Dib','cuts'),('CR_Top','cuts')])
elif split=='30' and doDiscovery != True:
    VV.setNormFactor("mu_VV",1.,0.,5.)
    VV.setNormRegions([('CR_Dib_DM30','cuts'),('CR_Top_DM30','cuts')])
else: # by default, if split is 0
    VV.setNormByTheory()
if doSyst == True:
    for syst in thSystListVV:
        VV.addSystematic(syst)
    for syst in systnorm_list:    
        VV.addSystematic(syst)
if doTestSystematics == True:
    VV.addSystematic(TestSystematic_14corr)
    VV.addSystematic(TestSystematic_14uncorr)
VV.setStatConfig(useStat)

VVV = Sample("VVV",kViolet+2)
VVV.setNormByTheory()
if doSyst == True:
    for syst in syst_list:    
        VVV.addSystematic(syst)
if doTestSystematics == True:
    VVV.addSystematic(TestSystematic_14corr)
    VVV.addSystematic(TestSystematic_14uncorr)
VVV.setStatConfig(useStat)

others = Sample("other",kGray)
others.setNormByTheory()
if doSyst == True:
    for syst in syst_list:    
        others.addSystematic(syst)
if doTestSystematics == True:
    others.addSystematic(TestSystematic_14corr)
    others.addSystematic(TestSystematic_14uncorr)
others.setStatConfig(useStat)

FNP = Sample("FNP",kPink-3)
FNP.suffixTreeName = "_WEIGHTS"
FNP.setNormByTheory()
if doTestSystematics == True:
    FNP.addSystematic(TestSystematic_14corr)
    FNP.addSystematic(TestSystematic_14uncorr)
if doSyst == True:
    #FNP.addSystematic(FNP_TOTAL_SYS)
    '''
    FNP.addSystematic(FNP_STAT)
    FNP.addSystematic(FNP_WEIGHT)
    FNP.addSystematic(FNP_XSEC)
    FNP.addSystematic(FNP_EFF)
    FNP.addSystematic(FNP_EFF_TRIG)
    FNP.addSystematic(FNP_WEIGHTSYS)
    '''
    FNP.setStatConfig(useStat)
if doSyst == False:
    FNP.setStatConfig(useStat)

if doSyst == True:
    data = Sample("data",kBlack)
if doSyst == False:
    data = Sample("data",kBlack)
data.setData()

# This is a protection: if doBackupCache==True, inputs are hidden. 
# if doBackupCache==True and configMgr.forceNorm ==True normalized histos should be rebuild from inputs
if (doBackupCacheFile == False) or (doBackupCacheFile==True and configMgr.forceNorm ==True): 
    if doSyst == False:
        data.addInputs(   ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/no_syst/data.root"])
        FNP.addInputs(    ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/FNP.root"]) 
        others.addInputs(["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/other.root"])
        VVV.addInputs(    ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/VVV.root"])
        Zttjets.addInputs(["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/Zttjets.root"])
        top.addInputs(     ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/theoryExpCombined/ttbar_theoryExp.root"])
        singletop.addInputs(["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/theoryExpCombined/Wt_theoryExp.root"])
        VV.addInputs(      ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/theoryExpCombined/VV_theoryExp.root"])
        Zjets.addInputs(  ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/theoryExpCombined/Zjets_theoryExp.root"])
    elif doSyst == True:
        data.addInputs(   ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/no_syst/data.root"])
        FNP.addInputs(    ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/FNP.root"]) 
        others.addInputs(["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/other.root"])
        VVV.addInputs(    ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/VVV.root"])
        Zttjets.addInputs(["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/Zttjets.root"])
        top.addInputs(     ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/theoryExpCombined/ttbar_theoryExp.root"])
        singletop.addInputs(["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/theoryExpCombined/Wt_theoryExp.root"])
        VV.addInputs(      ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/theoryExpCombined/VV_theoryExp.root"])
        Zjets.addInputs(  ["/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/theoryExpCombined/Zjets_theoryExp.root"])

commonSamples = [data,others,VVV,FNP,top,singletop,Zjets,Zttjets,VV]
configMgr.plotColours = [kGreen,kBlack]




## Parameters of the Measurement
measName = "BasicMeasurement"
measLumi = 1.
measLumiError = 0.039

## Parameters of Channels
cutsNBins = 1
cutsBinLow = 0.
cutsBinHigh = 1.



####################### FNP negative yields fixes ######################
# from ./python/sample.py:242: def buildHisto(self, binValues, region, var, binLow=0.5, binWidth=1.)

FNP.buildHisto([0.01],"CR_Dib","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"CR_Top","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)

FNP.buildHisto([0.01],"VR_Dib_DF0J","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([7.33],"VR_Dib_SF0J","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"VR_Top_DF1J","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([4.23],"VR_Top_SF1J","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([20.47],"VR_Top_DF0J","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.05],"VR_Top_SF0J","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)


FNP.buildHisto([2.13],"SR_DF0J_81_8125","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([3.50],"SR_DF0J_8125_815","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"SR_DF0J_815_8175","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"SR_DF0J_8175_82","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([6.47],"SR_DF0J_82_8225","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([1.35],"SR_DF0J_8225_825","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([1.65],"SR_DF0J_825_8275","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"SR_DF0J_8275_83","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([2.32],"SR_DF0J_83_8325","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.43],"SR_DF0J_8325_835","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([4.26],"SR_DF0J_835_8375","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([6.05],"SR_DF0J_8375_84","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.93],"SR_DF0J_84_845","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([4.59],"SR_DF0J_845_85","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([2.15],"SR_DF0J_85_86","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.09],"SR_DF0J_86","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)


FNP.buildHisto([0.01],"SR_SF0J_77_775","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([1.41],"SR_SF0J_775_78","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"SR_SF0J_78_785","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"SR_SF0J_785_79","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([1.11],"SR_SF0J_79_795","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"SR_SF0J_795_80","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"SR_SF0J_80_81","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)
FNP.buildHisto([0.01],"SR_SF0J_81","cuts",cutsBinLow,cutsBinHigh-cutsBinLow)

if doDiscovery:

    FNP.buildHisto([25.52],"SRD_DF0J_81_SF0J_77","cuts",0.5,1.) # Different binning needed for discovery sample

    FNP.buildHisto([32.83],"SRD_DF0J_81","cuts",0.5,1.) # Different binning needed for discovery sample
    FNP.buildHisto([28.75],"SRD_DF0J_82","cuts",0.5,1.) # Different binning needed for discovery sample
    FNP.buildHisto([20.82],"SRD_DF0J_83","cuts",0.5,1.) # Different binning needed for discovery sample
    FNP.buildHisto([7.76],"SRD_DF0J_84","cuts",0.5,1.) # Different binning needed for discovery sample
    FNP.buildHisto([2.24],"SRD_DF0J_85","cuts",0.5,1.) # Different binning needed for discovery sample

    FNP.buildHisto([0.01],"SRD_SF0J_77","cuts",0.5,1.) # Different binning needed for discovery sample
    FNP.buildHisto([0.01],"SRD_SF0J_78","cuts",0.5,1.) # Different binning needed for discovery sample
    FNP.buildHisto([0.01],"SRD_SF0J_79","cuts",0.5,1.) # Different binning needed for discovery sample
    FNP.buildHisto([0.01],"SRD_SF0J_80","cuts",0.5,1.) # Different binning needed for discovery sample

##############################################



## Parameters of Validation Plots
PlotNBins = 30
PlotBinLow = 0.
PlotBinHigh = 3000
LogYbool = True

# Dictionary for plotting cosmetics
# Preselection
if PlotDictionary == 'preselection':
    Plots = {'MET'      : {'PlotNBins' : 10, 'PlotBinLow' : 0,  'PlotBinHigh' : 250,  'titleX' : 'E_{T}^{miss} [GeV]'},
             'METsig'   : {'PlotNBins' : 20, 'PlotBinLow' : 5,  'PlotBinHigh' : 25,   'titleX' : 'E_{T}^{miss} significance'},
             'lep1pT'   : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 200., 'titleX' : 'Leading lepton p_{T} [GeV]'},
             'lep2pT'   : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 200., 'titleX' : 'Subleading lepton p_{T} [GeV]'},
             'njet'     : {'PlotNBins' : 3,  'PlotBinLow' : 0., 'PlotBinHigh' : 3.,   'titleX' : 'Number of jets'},
             'nbjet'    : {'PlotNBins' : 3,  'PlotBinLow' : 0., 'PlotBinHigh' : 3.,   'titleX' : 'Number of b-jets'},
             'mll'      : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 200,  'titleX' : 'm(#font[12]{ll}) [GeV]'},
             'MT2'      : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 250., 'titleX' : 'm_{T2} [GeV]'},
             'cosTstar' : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,   'titleX' : '|cos \\theta*_{#font[12]{ll}}|'},
             'DPhib'    : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 3.1416,'titleX' : '|\\Delta\\phi_{boost}|'},
             'dPhiMETL1'    : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 3.1416,'titleX' : '\\Delta\\phi(#font[12]{l_{1}},E_{T}^{miss})'},
             'dPhiMETL2'    : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 3.1416,'titleX' : '\\Delta\\phi(#font[12]{l_{2}},E_{T}^{miss})'},
             'BDTDeltaM100_90'      : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1., 'titleX' : 'BDT signal'},
             'BDTVVDeltaM100_90'    : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1., 'titleX' : 'BDT VV'},
             'BDTtopDeltaM100_90'   : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 0.5, 'titleX' : 'BDT top'},
             'BDTothersDeltaM100_90': {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 0.1, 'titleX' : 'BDT others'},
        }
# CR/VR
elif PlotDictionary == 'CRVR':
    Plots = {'MET'                   : {'PlotNBins' : 30, 'PlotBinLow' : 0,  'PlotBinHigh' : 300,    'titleX' : 'E_{T}^{miss} [GeV]'},
             'METsig'                : {'PlotNBins' : 17, 'PlotBinLow' : 8,  'PlotBinHigh' : 25,     'titleX' : 'E_{T}^{miss} significance'},
             'lep1pT'                : {'PlotNBins' : 30, 'PlotBinLow' : 0., 'PlotBinHigh' : 200.,   'titleX' : 'Leading lepton p_{T} [GeV]'},
             'lep2pT'                : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 200.,   'titleX' : 'Subleading lepton p_{T} [GeV]'},
             'njet'                  : {'PlotNBins' : 3,  'PlotBinLow' : 0., 'PlotBinHigh' : 3.,     'titleX' : 'Number of jets'},
             'nbjet'                 : {'PlotNBins' : 3,  'PlotBinLow' : 0., 'PlotBinHigh' : 3.,     'titleX' : 'Number of b-jets'},
             'mll'                   : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 300,    'titleX' : 'm(#font[12]{ll}) [GeV]'},
             'MT2'                   : {'PlotNBins' : 8, 'PlotBinLow' : 50., 'PlotBinHigh' : 250.,   'titleX' : 'm_{T2} [GeV]'},
             'cosTstar'              : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : '|cos \\theta_{#font[12]{ll}}*|'},
             'DPhib'                 : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 3.1416, 'titleX' : '|\\Delta\\phi_{boost}|'},
             'BDTDeltaM100_90'       : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : 'BDT signal'},
             'BDTVVDeltaM100_90'     : {'PlotNBins' : 20, 'PlotBinLow' : 0., 'PlotBinHigh' : 1.,     'titleX' : 'BDT VV'},
             'BDTtopDeltaM100_90'    : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 0.5,     'titleX' : 'BDT top'},
             'BDTothersDeltaM100_90' : {'PlotNBins' : 10, 'PlotBinLow' : 0., 'PlotBinHigh' : 0.1,     'titleX' : 'BDT others'},
            }
    if 'CR_Dib' in ChannelToPlot:
        Plots['BDTVVDeltaM100_90']['PlotNBins'] = 12
        Plots['BDTVVDeltaM100_90']['PlotBinLow'] = 0.2
        Plots['BDTVVDeltaM100_90']['PlotBinHigh'] = 0.8
    if 'CR_Top' in ChannelToPlot:
        Plots['BDTtopDeltaM100_90']['PlotNBins'] = 7
        Plots['BDTtopDeltaM100_90']['PlotBinLow'] = 0.
        Plots['BDTtopDeltaM100_90']['PlotBinHigh'] = 0.35
    if 'VR_Dib_DF0J' in ChannelToPlot:
        Plots['BDTVVDeltaM100_90']['PlotNBins'] = 5
        Plots['BDTVVDeltaM100_90']['PlotBinLow'] = 0.2
        Plots['BDTVVDeltaM100_90']['PlotBinHigh'] = 0.3
    if 'VR_Top_DF1J' in ChannelToPlot:
        Plots['BDTtopDeltaM100_90']['PlotNBins'] = 5
        Plots['BDTtopDeltaM100_90']['PlotBinLow'] = 0.
        Plots['BDTtopDeltaM100_90']['PlotBinHigh'] = 0.2
    if 'VR_Top_SF1J' in ChannelToPlot:
        Plots['cosTstar']['PlotNBins'] = 6
        Plots['cosTstar']['PlotBinLow'] = 0.
        Plots['cosTstar']['PlotBinHigh'] = 0.6
    if 'VR_Top_SF0J' in ChannelToPlot:
        Plots['BDTtopDeltaM100_90']['PlotNBins'] = 5
        Plots['BDTtopDeltaM100_90']['PlotBinLow'] = 0.05
        Plots['BDTtopDeltaM100_90']['PlotBinHigh'] = 0.3
elif PlotDictionary == 'SRDF':
    Plots = { 
             'BDTDeltaM100_90'       : {'PlotNBins' : 10, 'PlotBinLow' : 0.81, 'PlotBinHigh' : 0.86,     'titleX' : 'BDT-signal'},
            }
elif PlotDictionary == 'SRSF':
    Plots = { 
             'BDTDeltaM100_90'       : {'PlotNBins' : 8, 'PlotBinLow' : 0.77, 'PlotBinHigh' : 0.81,     'titleX' : 'BDT-signal'},
            }
elif PlotDictionary == 'VRSR':
    if 'VR_Dib_DF0J_SR_DF0J' in ChannelToPlot:
        Plots = { 
             'BDTDeltaM100_90'       : {'PlotNBins' : 22, 'PlotBinLow' : 0.65, 'PlotBinHigh' : 0.87,     'titleX' : 'BDT-signal'},
             'MT2'                   : {'PlotNBins' : 9, 'PlotBinLow' : 50., 'PlotBinHigh' : 140.,   'titleX' : 'm_{T2} [GeV]'},
            }
    elif 'VR_Dib_SF0J_SR_SF0J' in ChannelToPlot:
        Plots = { 
             'BDTDeltaM100_90'       : {'PlotNBins' : 17, 'PlotBinLow' : 0.65, 'PlotBinHigh' : 0.82,     'titleX' : 'BDT-signal'},
             'MT2'                   : {'PlotNBins' : 7, 'PlotBinLow' : 50., 'PlotBinHigh' : 120.,   'titleX' : 'm_{T2} [GeV]'},
            }

############################################################################
#      Bkg Only Fit
############################################################################

if doBkgOnly:
    configMgr.doExclusion=False
    myTopLvl = configMgr.addFitConfig("BkgOnly")
    myTopLvl.addSamples(commonSamples)
    if split=='100':
        CR_VV=myTopLvl.addChannel("cuts",['CR_Dib'],cutsNBins,cutsBinLow,cutsBinHigh)
        CR_top=myTopLvl.addChannel("cuts",['CR_Top'],cutsNBins,cutsBinLow,cutsBinHigh)

        if doSyst == True:

            for syst in thSystListzjet:
                CR_VV.getSample("Zjets").removeSystematic(syst)
            CR_VV.getSample("Zjets").addSystematic(Systematic("CKKW_Zjets", 1., 0.9965, 1.0024, "user", "histoSys"))
            CR_VV.getSample("Zjets").addSystematic(Systematic("Factorization_Zjets", 1., 0.9742, 1.0234, "user", "histoSys"))
            CR_VV.getSample("Zjets").addSystematic(Systematic("Resummation_Zjets", 1., 0.9998, 0.9999, "user", "histoSys"))
            CR_VV.getSample("Zjets").addSystematic(Systematic("Renormalization_Zjets", 1., 0.9097, 1.0992, "user", "histoSys"))

            CR_VV.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+2.69)/0.01,0., 'user', 'histoSys'))
            CR_VV.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.09)/0.01,0., 'user', 'histoSys'))
            CR_VV.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.58)/0.01,0., 'user', 'histoSys'))
            CR_VV.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.19)/0.01,0., 'user', 'histoSys'))
            CR_VV.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.22)/0.01,0., 'user', 'histoSys'))
            CR_VV.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.53)/0.01,0., 'user', 'histoSys'))
            CR_top.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+21.15)/0.01,0., 'user', 'histoSys'))
            CR_top.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+2.16)/0.01,0., 'user', 'histoSys'))
            CR_top.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+4.77)/0.01,0., 'user', 'histoSys'))
            CR_top.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+1.19)/0.01,0., 'user', 'histoSys'))
            CR_top.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+1.65)/0.01,0., 'user', 'histoSys'))
            CR_top.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+9.53)/0.01,0., 'user', 'histoSys'))

        myTopLvl.addBkgConstrainChannels([CR_VV,CR_top])

        if CRonlyFit == False:
            VR_VV_DF0J=myTopLvl.addChannel("cuts",['VR_Dib_DF0J'],cutsNBins,cutsBinLow,cutsBinHigh)
            VR_VV_SF0J=myTopLvl.addChannel("cuts",['VR_Dib_SF0J'],cutsNBins,cutsBinLow,cutsBinHigh)
            VR_top_DF1J=myTopLvl.addChannel("cuts",['VR_Top_DF1J'],cutsNBins,cutsBinLow,cutsBinHigh)
            VR_top_SF1J=myTopLvl.addChannel("cuts",['VR_Top_SF1J'],cutsNBins,cutsBinLow,cutsBinHigh)
            VR_top_DF0J=myTopLvl.addChannel("cuts",['VR_Top_DF0J'],cutsNBins,cutsBinLow,cutsBinHigh)
            VR_top_SF0J=myTopLvl.addChannel("cuts",['VR_Top_SF0J'],cutsNBins,cutsBinLow,cutsBinHigh)

            if doSyst == True:
                VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+4.31)/0.01,0., 'user', 'histoSys'))
                VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.11)/0.01,0., 'user', 'histoSys'))
                VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+1.07)/0.01,0., 'user', 'histoSys'))
                VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.30)/0.01,0., 'user', 'histoSys'))
                VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.38)/0.01,0., 'user', 'histoSys'))
                VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.85)/0.01,0., 'user', 'histoSys'))
                VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(7.33+3.50)/7.33,(7.33-3.08)/7.33, 'user', 'histoSys'))
                VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(7.33+1.29)/7.33,(7.33-0.82)/7.33, 'user', 'histoSys'))
                VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(7.33+0.08)/7.33,(7.33-0.34)/7.33, 'user', 'histoSys'))
                VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(7.33+0.22)/7.33,(7.33-0.10)/7.33, 'user', 'histoSys'))
                VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(7.33+0.27)/7.33,(7.33-0.34)/7.33, 'user', 'histoSys'))
                VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(7.33+2.40)/7.33,(7.33-1.80)/7.33, 'user', 'histoSys'))
                VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+9.44)/0.01,0., 'user', 'histoSys'))
                VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.56)/0.01,0., 'user', 'histoSys'))
                VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+1.37)/0.01,0., 'user', 'histoSys'))
                VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.45)/0.01,0., 'user', 'histoSys'))
                VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.65)/0.01,0., 'user', 'histoSys'))
                VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+2.85)/0.01,0., 'user', 'histoSys'))
                VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(4.23+0.85)/4.23,(4.23-0.78)/4.23, 'user', 'histoSys'))
                VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(4.23+0.27)/4.23,(4.23-0.26)/4.23, 'user', 'histoSys'))
                VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(4.23+0.41)/4.23,(4.23-0.29)/4.23, 'user', 'histoSys'))
                VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(4.23+0.04)/4.23,(4.23-0.04)/4.23, 'user', 'histoSys'))
                VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(4.23+0.04)/4.23,(4.23-0.04)/4.23, 'user', 'histoSys'))
                VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(4.23+0.90)/4.23,(4.23-0.78)/4.23, 'user', 'histoSys'))
                VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(20.47+5.20)/20.47,(20.47-4.82)/20.47, 'user', 'histoSys'))
                VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(20.47+0.15)/20.47,(20.47-0.15)/20.47, 'user', 'histoSys'))
                VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(20.47+6.85)/20.47,(20.47-6.86)/20.47, 'user', 'histoSys'))
                VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(20.47+0.14)/20.47,(20.47-0.13)/20.47, 'user', 'histoSys'))
                VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(20.47+0.29)/20.47,(20.47-0.37)/20.47, 'user', 'histoSys'))
                VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(20.47+0.53)/20.47,(20.47-0.32)/20.47, 'user', 'histoSys'))
                VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.05+0.14)/0.05,0., 'user', 'histoSys'))
                VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.05+0.02)/0.05,(0.05-0.02)/0.05, 'user', 'histoSys'))
                VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.05+0.17)/0.05,0., 'user', 'histoSys'))
                VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.05+0.01)/0.05,(0.05-0.01)/0.05, 'user', 'histoSys'))
                VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.05+0.00)/0.05,(0.05-0.01)/0.05, 'user', 'histoSys'))
                VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.05+0.02)/0.05,0., 'user', 'histoSys'))

            myTopLvl.addValidationChannels([VR_VV_DF0J,VR_VV_SF0J,VR_top_DF1J,VR_top_SF1J,VR_top_DF0J,VR_top_SF0J])
       
            for ch in fit_DM100:

                SR_channel=myTopLvl.addChannel("cuts",[ch],cutsNBins,cutsBinLow,cutsBinHigh)
                
                # Ad-hoc fixes for theorethical systs in SF0J channels
                if ch in fit_SF0J_DM100 and doSyst == True:
                    for syst in thSystListzjet:
                        SR_channel.getSample("Zjets").removeSystematic(syst)
                    SR_channel.getSample("Zjets").addSystematic(Systematic("CKKW_Zjets", 1., 1.0009863, 0.9953885, "user", "histoSys"))
                    SR_channel.getSample("Zjets").addSystematic(Systematic("Factorization_Zjets", 1., 0.98328, 1.015063, "user", "histoSys"))
                    SR_channel.getSample("Zjets").addSystematic(Systematic("Resummation_Zjets", 1., 0.994787, 1.00246892, "user", "histoSys"))
                    SR_channel.getSample("Zjets").addSystematic(Systematic("Renormalization_Zjets", 1., 1.007078, 1.001034, "user", "histoSys"))
                

                if ch == 'SR_DF0J_81_8125' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(2.13+0.24)/2.13,(2.13-0.22)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(2.13+0.04)/2.13,(2.13-0.04)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(2.13+0.08)/2.13,(2.13-0.08)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(2.13+0.01)/2.13,(2.13-0.00)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(2.13+0.01)/2.13,(2.13-0.01)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(2.13+0.15)/2.13,(2.13-0.05)/2.13, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_8125_815' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(3.50+0.28)/3.50,(3.50-0.27)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(3.50+0.01)/3.50,(3.50-0.01)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(3.50+0.19)/3.50,(3.50-0.20)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(3.50+0.01)/3.50,(3.50-0.01)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(3.50+0.01)/3.50,(3.50-0.01)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(3.50+0.14)/3.50,(3.50-0.08)/3.50, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_815_8175' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.06)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.10)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                elif ch == 'SR_DF0J_8175_82' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.18)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.29)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                elif ch == 'SR_DF0J_82_8225' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(6.47+0.31)/6.47,(6.47-0.29)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(6.47+0.00)/6.47,(6.47-0.00)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(6.47+0.60)/6.47,(6.47-0.60)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(6.47+0.01)/6.47,(6.47-0.01)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(6.47+0.01)/6.47,(6.47-0.00)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(6.47+0.03)/6.47,(6.47-0.02)/6.47, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_8225_825' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(1.35+0.18)/1.35,(1.35-0.17)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(1.35+0.02)/1.35,(1.35-0.02)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(1.35+0.45)/1.35,(1.35-0.44)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(1.35+0.01)/1.35,(1.35-0.01)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(1.35+0.01)/1.35,(1.35-0.01)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(1.35+0.05)/1.35,(1.35-0.07)/1.35, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_825_8275' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(1.65+0.25)/1.65,(1.65-0.24)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(1.65+0.02)/1.65,(1.65-0.02)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(1.65+0.50)/1.65,(1.65-0.53)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(1.65+0.00)/1.65,(1.65-0.00)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(1.65+0.01)/1.65,(1.65-0.01)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(1.65+0.08)/1.65,(1.65-0.11)/1.65, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_8275_83' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.14)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.34)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                elif ch == 'SR_DF0J_83_8325' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(2.32+0.19)/2.32,(2.32-0.18)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(2.32+0.00)/2.32,(2.32-0.00)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(2.32+1.25)/2.32,(2.32-1.24)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(2.32+0.01)/2.32,(2.32-0.01)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(2.32+0.01)/2.32,(2.32-0.01)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(2.32+0.04)/2.32,(2.32-0.00)/2.32, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_8325_835' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.43+0.17)/0.43,(0.43-0.16)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.43+0.01)/0.43,(0.43-0.01)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.43+0.40)/0.43,(0.43-0.41)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.43+0.00)/0.43,(0.43-0.00)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.43+0.01)/0.43,(0.43-0.01)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.43+0.15)/0.43,(0.43-0.03)/0.43, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_835_8375' and doSyst == True:  ######## CKKW fix #########
                    SR_channel.getSample("VV").removeSystematic(thSyst_ckkw_VV)
                    SR_channel.getSample('VV').addSystematic(Systematic('CKKW_VV','',1.0450,0.8142, 'user', systype_norm))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(4.26+0.31)/4.26,(4.26-0.29)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(4.26+0.08)/4.26,(4.26-0.07)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(4.26+1.12)/4.26,(4.26-1.14)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(4.26+0.00)/4.26,(4.26-0.00)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(4.26+0.01)/4.26,(4.26-0.01)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(4.26+0.26)/4.26,(4.26-0.19)/4.26, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_8375_84' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(6.05+0.23)/6.05,(6.05-0.22)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(6.05+0.07)/6.05,(6.05-0.07)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(6.05+0.35)/6.05,(6.05-0.33)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(6.05+0.01)/6.05,(6.05-0.01)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(6.05+0.00)/6.05,(6.05-0.00)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(6.05+0.20)/6.05,(6.05-0.22)/6.05, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_84_845' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.93+0.09)/0.93,(0.93-0.09)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.93+0.01)/0.93,(0.93-0.01)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.93+0.74)/0.93,(0.93-0.74)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.93+0.00)/0.93,(0.93-0.00)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.93+0.01)/0.93,(0.93-0.01)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.93+0.03)/0.93,(0.93-0.00)/0.93, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_845_85' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(4.59+0.32)/4.59,(4.59-0.30)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(4.59+0.06)/4.59,(4.59-0.06)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(4.59+0.47)/4.59,(4.59-0.47)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(4.59+0.01)/4.59,(4.59-0.01)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(4.59+0.01)/4.59,(4.59-0.01)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(4.59+0.22)/4.59,(4.59-0.08)/4.59, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_85_86' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(2.15+0.24)/2.15,(2.15-0.23)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(2.15+0.01)/2.15,(2.15-0.01)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(2.15+0.51)/2.15,(2.15-0.51)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(2.15+0.00)/2.15,(2.15-0.00)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(2.15+0.01)/2.15,(2.15-0.01)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(2.15+0.03)/2.15,(2.15-0.04)/2.15, 'user', 'histoSys'))
                elif ch == 'SR_DF0J_86' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.09+0.23)/0.09,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.09+0.02)/0.09,(0.09-0.02)/0.09, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.09+0.48)/0.09,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.09+0.00)/0.09,(0.09-0.00)/0.09, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.09+0.02)/0.09,(0.09-0.02)/0.09, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.09+0.13)/0.09,(0.09-0.05)/0.09, 'user', 'histoSys'))
                elif ch == 'SR_SF0J_77_775' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',1.0772,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.03)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.12)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.36)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.25)/0.01,0., 'user', 'histoSys'))
                elif ch == 'SR_SF0J_775_78' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.6088,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(1.41+0.19)/1.41,(1.41-0.18)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(1.41+0.04)/1.41,(1.41-0.01)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(1.41+0.08)/1.41,(1.41-0.07)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(1.41+0.00)/1.41,(1.41-0.00)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(1.41+0.01)/1.41,(1.41-0.01)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(1.41+0.26)/1.41,(1.41-0.05)/1.41, 'user', 'histoSys'))
                elif ch == 'SR_SF0J_78_785' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.6852,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.09)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.03)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.15)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.09)/0.01,0., 'user', 'histoSys'))
                elif ch == 'SR_SF0J_785_79' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.1803,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.04)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.22)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.06)/0.01,0., 'user', 'histoSys'))
                elif ch == 'SR_SF0J_79_795' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.5094,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(1.11+0.04)/1.11,(1.11-0.04)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(1.11+0.06)/1.11,(1.11-0.04)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(1.11+0.15)/1.11,(1.11-0.15)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(1.11+0.00)/1.11,(1.11-0.00)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(1.11+0.01)/1.11,(1.11-0.01)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(1.11+0.09)/1.11,(1.11-0.11)/1.11, 'user', 'histoSys'))
                elif ch == 'SR_SF0J_795_80' and doSyst == True:  ######## CKKW fix #########
                    SR_channel.getSample("VV").removeSystematic(thSyst_ckkw_VV)
                    SR_channel.getSample('VV').addSystematic(Systematic('CKKW_VV','',0.7794,0.6955, 'user', systype_norm))

                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',2.1465,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.02)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.04)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.06)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.10)/0.01,0., 'user', 'histoSys'))
                elif ch == 'SR_SF0J_80_81' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.2895,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('ttbar').removeSystematic(thSyst_rad_ttbar)
                    SR_channel.getSample('ttbar').addSystematic(Systematic('Radiation_ttbar','',1.862,0.864, 'user', 'histoSys'))
                    SR_channel.getSample('ttbar').removeSystematic(thSyst_show_ttbar)
                    SR_channel.getSample('ttbar').addSystematic(Systematic('Shower_ttbar','',1.05,0.95, 'user', 'histoSys'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.02)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.22)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.09)/0.01,0., 'user', 'histoSys'))
                elif ch == 'SR_SF0J_81' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',2.,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.07)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.06)/0.01,0., 'user', 'histoSys'))

                myTopLvl.addSignalChannels(SR_channel)
        
        if doValidationPlots:
            for variable in VariableToPlot:
                for ch in ChannelToPlot:
                    plot = myTopLvl.addChannel(variable,[ch],Plots[variable]['PlotNBins'],Plots[variable]['PlotBinLow'],Plots[variable]['PlotBinHigh'])
                    plot.hasB = True
                    plot.hasBQCD = False
                    plot.useOverflowBin=True
                    plot.logY = LogYbool
                    plot.titleX = Plots[variable]['titleX']
                    plot.titleY = 'Events'
                    myTopLvl.addValidationChannels(plot)

    if split=='30':
        CR_VV_DF1J=myTopLvl.addChannel("cuts",['CR_Dib_DF1J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
        CR_VV_SF1J=myTopLvl.addChannel("cuts",['CR_Dib_SF1J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
        CR_top_DF=myTopLvl.addChannel("cuts",['CR_Top_DF_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
        CR_top_SF=myTopLvl.addChannel("cuts",['CR_Top_SF_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
        myTopLvl.addBkgConstrainChannels([CR_VV_DF1J,CR_VV_SF1J,CR_top_DF,CR_top_SF])
        
        VR_VV_DF1J=myTopLvl.addChannel("cuts",['VR_Dib_DF1J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
        VR_VV_SF1J=myTopLvl.addChannel("cuts",['VR_Dib_SF1J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
        VR_top_DF=myTopLvl.addChannel("cuts",['VR_Top_DF_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
        VR_top_SF=myTopLvl.addChannel("cuts",['VR_Top_SF_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
        myTopLvl.addValidationChannels([VR_VV_DF1J,VR_VV_SF1J,VR_top_DF,VR_top_SF])
        
        for ch in fit_DM30:
            SR_channel=myTopLvl.addChannel("cuts",[ch],cutsNBins,cutsBinLow,cutsBinHigh)
            myTopLvl.addSignalChannels(SR_channel)

        if doValidationPlots:
            for variable in VariableToPlot:
                for ch in ChannelToPlot:
                    plot = myTopLvl.addChannel(variable,[ch],Plots[variable]['PlotNBins'],Plots[variable]['PlotBinLow'],Plots[variable]['PlotBinHigh'])
                    plot.hasB = True
                    plot.hasBQCD = False
                    plot.useOverflowBin=True
                    plot.logY = LogYbool
                    plot.titleX = Plots[variable]['titleX']
                    plot.titleY = 'Events'
                    myTopLvl.addValidationChannels(plot)

    if split=='0':
        CR_VV_DF0J=myTopLvl.addChannel("cuts",['CR_Dib_DF0J_DM100_90'],cutsNBins,cutsBinLow,cutsBinHigh)
        myTopLvl.addBkgConstrainChannels([CR_VV_DF0J])

    meas = myTopLvl.addMeasurement(measName,measLumi,measLumiError)
    meas.addPOI("mu_SIG")



#####################################################################
#         Exclusion fit
#####################################################################

# Channels where the fit is actually performed
channels_DUMMY=['SR_DUMMY']
channels_EB=['SR_DF0J_EB_0','SR_DF0J_EB_1','SR_DF0J_EB_2','SR_SF0J_EB_0','SR_SF0J_EB_1','SR_SF0J_EB_2','SR_DF1J_EB_0','SR_DF1J_EB_1','SR_DF1J_EB_2','SR_SF1J_EB_0','SR_SF1J_EB_1','SR_SF1J_EB_2']
channels_EB_compare=['SR_DF0J_0']
sigSamples_test=['125_25']


if whichPoint != '0':
    sigSamples_to_run = [whichPoint]
elif whichPoint == '0': #no -point provided 
    sigSamples_to_run = sigSamples_grid

if doExclusion:
    configMgr.doExclusion=True
    for sig in sigSamples_to_run:
        # definition of an exclusion fit
        myTopLvl = configMgr.addFitConfig("C1C1_%s"%sig)
        myTopLvl.addSamples(commonSamples)
        meas = myTopLvl.addMeasurement(measName,measLumi,measLumiError)
        meas.addPOI("mu_SIG")

 
        sigSample = Sample("C1C1_"+sig,kPink)
        if whichPoint in sigSamples_BRtoCorrect:
            sigSample.addSampleSpecificWeight("0.107")
        if doSyst == False:
            sigSample.addInput("/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/c1c1_29032022.root")
        elif doSyst == True:
            sigSample.addInput("/storage/ballaben/EWKTuplesSecondWave/C1C1_WW/Trees_v16_291121/exp_syst/c1c1_29032022.root")
        sigSample.setStatConfig(useStat)
        if doSyst == True:
            for syst in syst_signal_list:    
                sigSample.addSystematic(syst)
            if configMgr.fixSigXSec==True :
                if whichPoint in sigSamples_C1C1_100:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['100'],1-XsecUnc['100'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_110:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['110'],1-XsecUnc['110'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_125:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['125'],1-XsecUnc['125'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_130:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['130'],1-XsecUnc['130'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_140:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['140'],1-XsecUnc['140'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_150:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['150'],1-XsecUnc['150'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_160:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['160'],1-XsecUnc['160'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_175:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['175'],1-XsecUnc['175'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_200:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['200'],1-XsecUnc['200'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_225:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['225'],1-XsecUnc['225'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_250:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['250'],1-XsecUnc['250'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_300:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['300'],1-XsecUnc['300'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_350:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['350'],1-XsecUnc['350'],"user","overallSys")
                elif whichPoint in sigSamples_C1C1_400:
                    sigXSSyst = Systematic("SigXSec",configMgr.weights,1+XsecUnc['400'],1-XsecUnc['400'],"user","overallSys")
                else:
                    raise Exception("BREAK")
                sigSample.addSystematic(sigXSSyst)
        if doTestSystematics == True:
            sigSample.addSystematic(TestSystematic_14corr)
            sigSample.addSystematic(TestSystematic_14uncorr)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG", 1.0, 0., 5.0)
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)


        if split=='30':

            CR_VV_DF0J=myTopLvl.addChannel("cuts",['CR_Dib_DF1J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
            CR_VV_SF0J=myTopLvl.addChannel("cuts",['CR_Dib_SF1J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
            CR_top_DF=myTopLvl.addChannel("cuts",['CR_Top_DF_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
            CR_top_SF=myTopLvl.addChannel("cuts",['CR_Top_SF_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
            myTopLvl.addBkgConstrainChannels([CR_VV_DF0J,CR_VV_SF0J,CR_top_DF,CR_top_SF])
       
            if configMgr.doHypoTest == False:
                VR_VV_DF0J=myTopLvl.addChannel("cuts",['VR_Dib_DF0J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_VV_SF0J=myTopLvl.addChannel("cuts",['VR_Dib_SF0J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_top_DF1J=myTopLvl.addChannel("cuts",['VR_Top_DF1J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_top_SF1J=myTopLvl.addChannel("cuts",['VR_Top_SF1J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_top_DF0J=myTopLvl.addChannel("cuts",['VR_Top_DF0J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_top_SF0J=myTopLvl.addChannel("cuts",['VR_Top_SF0J_DM30'],cutsNBins,cutsBinLow,cutsBinHigh)
                myTopLvl.addValidationChannels([VR_VV_DF0J,VR_VV_SF0J,VR_top_DF1J,VR_top_SF1J,VR_top_DF0J,VR_top_SF0J])

            channels_to_run = fit_DM30
            for ch in channels_to_run:
                channel=myTopLvl.addChannel("cuts",[ch],cutsNBins,cutsBinLow,cutsBinHigh)
                myTopLvl.addSignalChannels(channel)

        else:

            CR_VV=myTopLvl.addChannel("cuts",['CR_Dib'],cutsNBins,cutsBinLow,cutsBinHigh)
            CR_top=myTopLvl.addChannel("cuts",['CR_Top'],cutsNBins,cutsBinLow,cutsBinHigh)

            if doSyst == True:

                for syst in thSystListzjet:
                    CR_VV.getSample("Zjets").removeSystematic(syst)
                CR_VV.getSample("Zjets").addSystematic(Systematic("CKKW_Zjets", 1., 0.9965, 1.0024, "user", "histoSys"))
                CR_VV.getSample("Zjets").addSystematic(Systematic("Factorization_Zjets", 1., 0.9742, 1.0234, "user", "histoSys"))
                CR_VV.getSample("Zjets").addSystematic(Systematic("Resummation_Zjets", 1., 0.9998, 0.9999, "user", "histoSys"))
                CR_VV.getSample("Zjets").addSystematic(Systematic("Renormalization_Zjets", 1., 0.9097, 1.0992, "user", "histoSys"))

                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+2.69)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.09)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.58)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.19)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.22)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.53)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+21.15)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+2.16)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+4.77)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+1.19)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+1.65)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+9.53)/0.01,0., 'user', 'histoSys'))

                if whichPoint in sigSamples_SignalTheorySyst125_25:
                    CR_VV.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1',1.,1.0354,1.0056, 'user', 'histoSys'))
                    CR_VV.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1',1.,0.9855,0.9720, 'user', 'histoSys'))
                    CR_VV.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1',1.,1.0000,1.0262, 'user', 'histoSys'))
                    CR_top.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1',1.,1.0161,0.9695, 'user', 'histoSys'))
                    CR_top.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1',1.,1.0409,1.0435, 'user', 'histoSys'))
                    CR_top.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1',1.,1.0000,1.0500, 'user', 'histoSys'))
                elif whichPoint in sigSamples_SignalTheorySyst150_60:
                    CR_VV.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1',1.,1.0257,1.0317, 'user', 'histoSys'))
                    CR_VV.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1',1.,1.0195,0.9855, 'user', 'histoSys'))
                    CR_VV.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1',1.,1.0000,1.0264, 'user', 'histoSys'))
                    CR_top.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1',1.,0.9956,0.9378, 'user', 'histoSys'))
                    CR_top.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1',1.,0.9604,0.9784, 'user', 'histoSys'))
                    CR_top.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1',1.,1.0000,1.0669, 'user', 'histoSys'))
                elif whichPoint in sigSamples_SignalTheorySyst175_50:
                    CR_VV.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1',1.,1.0062,0.9921, 'user', 'histoSys'))
                    CR_VV.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1',1.,0.9953,0.9857, 'user', 'histoSys'))
                    CR_VV.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1',1.,1.0000,1.0204, 'user', 'histoSys'))
                    CR_top.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1',1.,1.0290,1.0225, 'user', 'histoSys'))
                    CR_top.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1',1.,1.0214,1.0103, 'user', 'histoSys'))
                    CR_top.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1',1.,1.0000,1.0622, 'user', 'histoSys'))

            myTopLvl.addBkgConstrainChannels([CR_VV,CR_top])
            '''
            if configMgr.doHypoTest == False:
                VR_VV_DF0J=myTopLvl.addChannel("cuts",['VR_Dib_DF0J'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_VV_SF0J=myTopLvl.addChannel("cuts",['VR_Dib_SF0J'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_top_DF1J=myTopLvl.addChannel("cuts",['VR_Top_DF1J'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_top_SF1J=myTopLvl.addChannel("cuts",['VR_Top_SF1J'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_top_DF0J=myTopLvl.addChannel("cuts",['VR_Top_DF0J'],cutsNBins,cutsBinLow,cutsBinHigh)
                VR_top_SF0J=myTopLvl.addChannel("cuts",['VR_Top_SF0J'],cutsNBins,cutsBinLow,cutsBinHigh)

                if doSyst == True:
                    VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+4.31)/0.01,0., 'user', 'histoSys'))
                    VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.11)/0.01,0., 'user', 'histoSys'))
                    VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+1.07)/0.01,0., 'user', 'histoSys'))
                    VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.30)/0.01,0., 'user', 'histoSys'))
                    VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.38)/0.01,0., 'user', 'histoSys'))
                    VR_VV_DF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.85)/0.01,0., 'user', 'histoSys'))
                    VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(7.33+3.50)/7.33,(7.33-3.08)/7.33, 'user', 'histoSys'))
                    VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(7.33+1.29)/7.33,(7.33-0.82)/7.33, 'user', 'histoSys'))
                    VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(7.33+0.08)/7.33,(7.33-0.34)/7.33, 'user', 'histoSys'))
                    VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(7.33+0.22)/7.33,(7.33-0.10)/7.33, 'user', 'histoSys'))
                    VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(7.33+0.27)/7.33,(7.33-0.34)/7.33, 'user', 'histoSys'))
                    VR_VV_SF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(7.33+2.40)/7.33,(7.33-1.80)/7.33, 'user', 'histoSys'))
                    VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+9.44)/0.01,0., 'user', 'histoSys'))
                    VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.56)/0.01,0., 'user', 'histoSys'))
                    VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+1.37)/0.01,0., 'user', 'histoSys'))
                    VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.45)/0.01,0., 'user', 'histoSys'))
                    VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.65)/0.01,0., 'user', 'histoSys'))
                    VR_top_DF1J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+2.85)/0.01,0., 'user', 'histoSys'))
                    VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(4.23+0.85)/4.23,(4.23-0.78)/4.23, 'user', 'histoSys'))
                    VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(4.23+0.27)/4.23,(4.23-0.26)/4.23, 'user', 'histoSys'))
                    VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(4.23+0.41)/4.23,(4.23-0.29)/4.23, 'user', 'histoSys'))
                    VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(4.23+0.04)/4.23,(4.23-0.04)/4.23, 'user', 'histoSys'))
                    VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(4.23+0.04)/4.23,(4.23-0.04)/4.23, 'user', 'histoSys'))
                    VR_top_SF1J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(4.23+0.90)/4.23,(4.23-0.78)/4.23, 'user', 'histoSys'))
                    VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(20.47+5.20)/20.47,(20.47-4.82)/20.47, 'user', 'histoSys'))
                    VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(20.47+0.15)/20.47,(20.47-0.15)/20.47, 'user', 'histoSys'))
                    VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(20.47+6.85)/20.47,(20.47-6.86)/20.47, 'user', 'histoSys'))
                    VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(20.47+0.14)/20.47,(20.47-0.13)/20.47, 'user', 'histoSys'))
                    VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(20.47+0.29)/20.47,(20.47-0.37)/20.47, 'user', 'histoSys'))
                    VR_top_DF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(20.47+0.53)/20.47,(20.47-0.32)/20.47, 'user', 'histoSys'))
                    VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.05+0.14)/0.05,0., 'user', 'histoSys'))
                    VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.05+0.02)/0.05,(0.05-0.02)/0.05, 'user', 'histoSys'))
                    VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.05+0.17)/0.05,0., 'user', 'histoSys'))
                    VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.05+0.01)/0.05,(0.05-0.01)/0.05, 'user', 'histoSys'))
                    VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.05+0.00)/0.05,(0.05-0.01)/0.05, 'user', 'histoSys'))
                    VR_top_SF0J.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.05+0.02)/0.05,0., 'user', 'histoSys'))
                myTopLvl.addValidationChannels([VR_VV_DF0J,VR_VV_SF0J,VR_top_DF1J,VR_top_SF1J,VR_top_DF0J,VR_top_SF0J])
            '''
            channels_to_run = fit_DM100
            for ch in channels_to_run:

                SR_channel=myTopLvl.addChannel("cuts",[ch],cutsNBins,cutsBinLow,cutsBinHigh)
                
                # Ad-hoc fixes for theorethical systs in SF0J channels
                if ch in fit_SF0J_DM100 and doSyst == True:
                    for syst in thSystListzjet:
                        SR_channel.getSample("Zjets").removeSystematic(syst)
                    SR_channel.getSample("Zjets").addSystematic(Systematic("CKKW_Zjets", 1., 1.0009863, 0.9953885, "user", "histoSys"))
                    SR_channel.getSample("Zjets").addSystematic(Systematic("Factorization_Zjets", 1., 0.98328, 1.015063, "user", "histoSys"))
                    SR_channel.getSample("Zjets").addSystematic(Systematic("Resummation_Zjets", 1., 0.994787, 1.00246892, "user", "histoSys"))
                    SR_channel.getSample("Zjets").addSystematic(Systematic("Renormalization_Zjets", 1., 1.007078, 1.001034, "user", "histoSys"))

                if ch == 'SR_DF0J_81_8125' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(2.13+0.24)/2.13,(2.13-0.22)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(2.13+0.04)/2.13,(2.13-0.04)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(2.13+0.08)/2.13,(2.13-0.08)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(2.13+0.01)/2.13,(2.13-0.00)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(2.13+0.01)/2.13,(2.13-0.01)/2.13, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(2.13+0.15)/2.13,(2.13-0.05)/2.13, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0014,1.0320,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0352,1.0056,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0656,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9635,0.9995,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9432,1.0039,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0577,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0059,1.0367,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0300,0.9951,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0523,'user', 'histoSys'))

                elif ch == 'SR_DF0J_8125_815' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(3.50+0.28)/3.50,(3.50-0.27)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(3.50+0.01)/3.50,(3.50-0.01)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(3.50+0.19)/3.50,(3.50-0.20)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(3.50+0.01)/3.50,(3.50-0.01)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(3.50+0.01)/3.50,(3.50-0.01)/3.50, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(3.50+0.14)/3.50,(3.50-0.08)/3.50, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9908,0.9644,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0284,0.9945,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0324,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.8989,1.0271,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9312,0.9373,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1189,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0144,0.9939,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0144,1.0074,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0448,'user', 'histoSys'))

                elif ch == 'SR_DF0J_815_8175' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.06)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.10)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9603,1.0493,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9580,0.9817,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0902,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0183,1.0077,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9695,1.0214,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0551,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9865,0.9986,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9967,0.9780,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0289,'user', 'histoSys'))

                elif ch == 'SR_DF0J_8175_82' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.18)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.29)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9353,1.0055,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9959,0.9751,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0379,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9431,0.9420,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9118,0.8779,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1004,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0064,1.0061,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0393,1.0232,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0197,'user', 'histoSys'))

                elif ch == 'SR_DF0J_82_8225' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(6.47+0.31)/6.47,(6.47-0.29)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(6.47+0.00)/6.47,(6.47-0.00)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(6.47+0.60)/6.47,(6.47-0.60)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(6.47+0.01)/6.47,(6.47-0.01)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(6.47+0.01)/6.47,(6.47-0.00)/6.47, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(6.47+0.03)/6.47,(6.47-0.02)/6.47, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0237,1.0321,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0463,1.0022,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0488,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0347,1.0643,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0663,1.0234,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0448,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0257,0.9932,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9836,0.9719,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0514,'user', 'histoSys'))

                elif ch == 'SR_DF0J_8225_825' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(1.35+0.18)/1.35,(1.35-0.17)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(1.35+0.02)/1.35,(1.35-0.02)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(1.35+0.45)/1.35,(1.35-0.44)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(1.35+0.01)/1.35,(1.35-0.01)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(1.35+0.01)/1.35,(1.35-0.01)/1.35, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(1.35+0.05)/1.35,(1.35-0.07)/1.35, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9039,0.9657,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9358,0.9223,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1368,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0113,1.0050,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9819,0.9774,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0490,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0065,0.9929,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9996,0.9774,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0307,'user', 'histoSys'))

                elif ch == 'SR_DF0J_825_8275' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(1.65+0.25)/1.65,(1.65-0.24)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(1.65+0.02)/1.65,(1.65-0.02)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(1.65+0.50)/1.65,(1.65-0.53)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(1.65+0.00)/1.65,(1.65-0.00)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(1.65+0.01)/1.65,(1.65-0.01)/1.65, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(1.65+0.08)/1.65,(1.65-0.11)/1.65, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9732,1.0171,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9989,0.9249,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0490,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0122,1.0004,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0502,1.0092,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0613,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9982,0.9753,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0040,0.9772,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0507,'user', 'histoSys'))

                elif ch == 'SR_DF0J_8275_83' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.14)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.34)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9898,1.0072,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0164,0.9814,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0849,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9585,1.0296,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9834,1.0109,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0235,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0393,1.0176,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0124,1.0026,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0523,'user', 'histoSys'))

                elif ch == 'SR_DF0J_83_8325' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(2.32+0.19)/2.32,(2.32-0.18)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(2.32+0.00)/2.32,(2.32-0.00)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(2.32+1.25)/2.32,(2.32-1.24)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(2.32+0.01)/2.32,(2.32-0.01)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(2.32+0.01)/2.32,(2.32-0.01)/2.32, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(2.32+0.04)/2.32,(2.32-0.00)/2.32, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9726,1.0192,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0367,0.9240,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0174,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9952,0.9741,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0312,0.9915,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0308,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0081,1.0024,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9801,0.9964,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0391,'user', 'histoSys'))

                elif ch == 'SR_DF0J_8325_835' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.43+0.17)/0.43,(0.43-0.16)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.43+0.01)/0.43,(0.43-0.01)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.43+0.40)/0.43,(0.43-0.41)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.43+0.00)/0.43,(0.43-0.00)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.43+0.01)/0.43,(0.43-0.01)/0.43, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.43+0.15)/0.43,(0.43-0.03)/0.43, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9606,1.0348,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9763,0.9760,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1057,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0778,1.0115,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0776,1.0461,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1417,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9676,0.9928,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9913,0.9767,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0255,'user', 'histoSys'))


                elif ch == 'SR_DF0J_835_8375' and doSyst == True:  ######## CKKW fix #########
                    SR_channel.getSample("VV").removeSystematic(thSyst_ckkw_VV)
                    SR_channel.getSample('VV').addSystematic(Systematic('CKKW_VV','',1.0450,0.8142, 'user', systype_norm))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(4.26+0.31)/4.26,(4.26-0.29)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(4.26+0.08)/4.26,(4.26-0.07)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(4.26+1.12)/4.26,(4.26-1.14)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(4.26+0.00)/4.26,(4.26-0.00)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(4.26+0.01)/4.26,(4.26-0.01)/4.26, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(4.26+0.26)/4.26,(4.26-0.19)/4.26, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9886,1.0384,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0086,0.9400,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0703,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0000,0.9806,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9700,0.9615,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0681,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9995,1.0004,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0371,1.0078,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0415,'user', 'histoSys'))

                elif ch == 'SR_DF0J_8375_84' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(6.05+0.23)/6.05,(6.05-0.22)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(6.05+0.07)/6.05,(6.05-0.07)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(6.05+0.35)/6.05,(6.05-0.33)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(6.05+0.01)/6.05,(6.05-0.01)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(6.05+0.00)/6.05,(6.05-0.00)/6.05, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(6.05+0.20)/6.05,(6.05-0.22)/6.05, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9298,1.0039,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9499,0.9682,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1264,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9639,1.0131,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9303,0.9059,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0961,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0148,0.9938,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0466,0.9618,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0253,'user', 'histoSys'))

                elif ch == 'SR_DF0J_84_845' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.93+0.09)/0.93,(0.93-0.09)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.93+0.01)/0.93,(0.93-0.01)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.93+0.74)/0.93,(0.93-0.74)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.93+0.00)/0.93,(0.93-0.00)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.93+0.01)/0.93,(0.93-0.01)/0.93, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.93+0.03)/0.93,(0.93-0.00)/0.93, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9478,1.0056,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0012,0.9895,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0323,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9801,1.0056,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0272,0.9799,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0758,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0021,1.0029,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0152,0.9919,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0261,'user', 'histoSys'))

                elif ch == 'SR_DF0J_845_85' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(4.59+0.32)/4.59,(4.59-0.30)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(4.59+0.06)/4.59,(4.59-0.06)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(4.59+0.47)/4.59,(4.59-0.47)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(4.59+0.01)/4.59,(4.59-0.01)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(4.59+0.01)/4.59,(4.59-0.01)/4.59, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(4.59+0.22)/4.59,(4.59-0.08)/4.59, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9376,1.0069,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9866,0.9904,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0481,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9437,0.9394,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9495,0.9348,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1210,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9709,0.9901,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9709,0.9937,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0270,'user', 'histoSys'))

                elif ch == 'SR_DF0J_85_86' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(2.15+0.24)/2.15,(2.15-0.23)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(2.15+0.01)/2.15,(2.15-0.01)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(2.15+0.51)/2.15,(2.15-0.51)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(2.15+0.00)/2.15,(2.15-0.00)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(2.15+0.01)/2.15,(2.15-0.01)/2.15, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(2.15+0.03)/2.15,(2.15-0.04)/2.15, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9790,1.0622,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9835,0.9759,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0464,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0022,0.9803,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9793,0.9684,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0520,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0075,1.0037,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9838,0.9938,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0181,'user', 'histoSys'))

                elif ch == 'SR_DF0J_86' and doSyst == True:
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.09+0.23)/0.09,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.09+0.02)/0.09,(0.09-0.02)/0.09, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.09+0.48)/0.09,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.09+0.00)/0.09,(0.09-0.00)/0.09, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.09+0.02)/0.09,(0.09-0.02)/0.09, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.09+0.13)/0.09,(0.09-0.05)/0.09, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9292,1.0390,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9640,0.9800,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0432,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9831,0.9703,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0334,1.0073,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0448,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9886,0.9929,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9495,0.9786,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0479,'user', 'histoSys'))

                elif ch == 'SR_SF0J_77_775' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',1.0772,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.03)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.12)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.36)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.25)/0.01,0., 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0105,1.0226,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0357,0.9710,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0678,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0128,1.0209,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9456,0.9556,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0706,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0382,1.0456,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9939,0.9841,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0869,'user', 'histoSys'))

                elif ch == 'SR_SF0J_775_78' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.6088,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(1.41+0.19)/1.41,(1.41-0.18)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(1.41+0.04)/1.41,(1.41-0.01)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(1.41+0.08)/1.41,(1.41-0.07)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(1.41+0.00)/1.41,(1.41-0.00)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(1.41+0.01)/1.41,(1.41-0.01)/1.41, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(1.41+0.26)/1.41,(1.41-0.05)/1.41, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9961,0.9474,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0061,0.9839,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0397,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0445,1.0756,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0654,1.0144,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1239,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0351,1.0189,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0086,1.0262,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0796,'user', 'histoSys'))

                elif ch == 'SR_SF0J_78_785' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.6852,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.09)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.03)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.15)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.09)/0.01,0., 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9794,1.0095,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0716,1.0314,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0448,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9378,1.0053,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9526,0.9284,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0709,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0284,0.9609,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0123,1.0132,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0665,'user', 'histoSys'))

                elif ch == 'SR_SF0J_785_79' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.1803,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.04)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.22)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.06)/0.01,0., 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0433,1.0253,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9898,0.9913,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0697,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0010,1.0664,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9899,0.9164,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1051,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9906,1.0086,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0191,1.0194,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0391,'user', 'histoSys'))

                elif ch == 'SR_SF0J_79_795' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.5094,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(1.11+0.04)/1.11,(1.11-0.04)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(1.11+0.06)/1.11,(1.11-0.04)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(1.11+0.15)/1.11,(1.11-0.15)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(1.11+0.00)/1.11,(1.11-0.00)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(1.11+0.01)/1.11,(1.11-0.01)/1.11, 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(1.11+0.09)/1.11,(1.11-0.11)/1.11, 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9600,0.9536,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0300,1.0026,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0367,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9513,0.9350,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9854,0.8182,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.1961,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0123,0.9680,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0171,0.9868,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0512,'user', 'histoSys'))

                elif ch == 'SR_SF0J_795_80' and doSyst == True:  ######## CKKW fix #########
                    SR_channel.getSample("VV").removeSystematic(thSyst_ckkw_VV)
                    SR_channel.getSample('VV').addSystematic(Systematic('CKKW_VV','',0.7794,0.6955, 'user', systype_norm))

                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',2.1465,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.02)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.04)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.06)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.10)/0.01,0., 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9674,0.9487,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0029,1.0033,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0853,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0116,1.1002,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0492,0.9285,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0676,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0101,0.9590,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0209,0.9523,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0438,'user', 'histoSys'))

                elif ch == 'SR_SF0J_80_81' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',0.2895,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('ttbar').removeSystematic(thSyst_rad_ttbar)
                    SR_channel.getSample('ttbar').addSystematic(Systematic('Radiation_ttbar','',1.862,0.864, 'user', 'histoSys'))
                    SR_channel.getSample('ttbar').removeSystematic(thSyst_show_ttbar)
                    SR_channel.getSample('ttbar').addSystematic(Systematic('Shower_ttbar','',1.05,0.95, 'user', 'histoSys'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.02)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.22)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.09)/0.01,0., 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9434,0.9815,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,0.9068,0.9553,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0901,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0645,1.0643,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0596,1.0151,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0767,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0372,1.0347,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0023,1.0080,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0638,'user', 'histoSys'))

                elif ch == 'SR_SF0J_81' and doSyst == True:
                    SR_channel.getSample('Wt').removeSystematic(thSyst_DS_Wt)
                    SR_channel.getSample('Wt').addSystematic(Systematic('DS_Singletop','',2.,1., 'user', 'normHistoSysOneSide'))

                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.01)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.07)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.00)/0.01,0., 'user', 'histoSys'))
                    SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.06)/0.01,0., 'user', 'histoSys'))
                    if whichPoint in sigSamples_SignalTheorySyst125_25:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0041,0.9268,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0039,0.9758,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0609, 'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst150_60:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,0.9688,0.9944,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0466,0.9002,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0732,'user', 'histoSys'))
                    elif whichPoint in sigSamples_SignalTheorySyst175_50:
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Merging_C1C1', 1.,1.0606,1.0149,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('RenormalizationFactorization_C1C1', 1.,1.0159,0.9933,'user', 'histoSys'))
                        SR_channel.getSample("C1C1_"+sig).addSystematic(Systematic('Radiation_C1C1', 1.,1.0000,1.0484,'user', 'histoSys'))
                
                myTopLvl.addSignalChannels(SR_channel)





############################################################
#           Discovery fit
############################################################

configMgr.cutsDict['SRD_DF0J_81'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.81'
configMgr.cutsDict['SRD_DF0J_82'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.82'
configMgr.cutsDict['SRD_DF0J_83'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.83'
configMgr.cutsDict['SRD_DF0J_84'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.84'
configMgr.cutsDict['SRD_DF0J_85'] = SR_DF0J_preselection + 'BDTDeltaM100_90 > 0.85'

configMgr.cutsDict['SRD_SF0J_77'] = SR_SF0J_preselection  + 'BDTDeltaM100_90 > 0.77 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SRD_SF0J_78'] = SR_SF0J_preselection  + 'BDTDeltaM100_90 > 0.78 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SRD_SF0J_79'] = SR_SF0J_preselection  + 'BDTDeltaM100_90 > 0.79 & BDTothersDeltaM100_90 <= 0.01'
configMgr.cutsDict['SRD_SF0J_80'] = SR_SF0J_preselection  + 'BDTDeltaM100_90 > 0.80 & BDTothersDeltaM100_90 <= 0.01'

configMgr.cutsDict['SRD_DF0J_81_SF0J_77'] = '( ' + SR_DF0J_preselection + ' BDTDeltaM100_90 > 0.81' + ' ) || ( ' + SR_SF0J_preselection  + ' BDTDeltaM100_90 > 0.77 & BDTothersDeltaM100_90 <= 0.01' + ' )'
configMgr.cutsDict['SRD_DF0J_84_SF0J_79'] = '( ' + SR_DF0J_preselection + ' BDTDeltaM100_90 > 0.84' + ' ) || ( ' + SR_SF0J_preselection  + ' BDTDeltaM100_90 > 0.79 & BDTothersDeltaM100_90 <= 0.01' + ' )'

DiscoveryFit_DM100=['SRD_DF0J_81','SRD_SF0J_77']

fit_SF0J_Discovery=['SRD_SF0J_77','SRD_SF0J_78','SRD_SF0J_79','SRD_SF0J_80','SRD_DF0J_81_SF0J_77','SRD_DF0J_84_SF0J_79']


configMgr.cutsDict['SRD_DF1J_0_DM30'] = 'mll>20 &&  lep1pT > 27 && isSF==0 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.40'
configMgr.cutsDict['SRD_SF1J_0_DM30'] = 'mll>20 &&  lep1pT > 27 && isSF==1 & njet==1 & nbjet==0 & METsig > 8 & BDTDeltaM30 > 0.40'
DiscoveryFit_DM30=['SRD_DF1J_0_DM30','SRD_SF1J_0_DM30']

channels_dummy_Discovery=['SRD_DF0J_0']

if doDiscovery:
    configMgr.doExclusion=False
    #discoveryFitConfig.getSample("A").addSystematic(weightSys)
    #discoveryFitConfig.addSystematic(treeSys)
    if split=='100':
        channels_to_run_Discovery = DiscoveryFit_DM100
    elif split=='30':
        channels_to_run_Discovery = DiscoveryFit_DM30
    elif split=='0':
        channels_to_run_Discovery = channels_dummy_Discovery
    if discoveryRegion!='':
        channels_to_run_Discovery = discoveryRegion
    for ch in channels_to_run_Discovery:    
        discoveryFitConfig = configMgr.addFitConfig("Discovery_%s"%ch)
        meas = discoveryFitConfig.addMeasurement(name="NormalMeasurement", lumi=1.0, lumiErr =0.039)
        meas.addPOI("mu_SIG_%s"%ch) # changed from meas.addPOI("mu_SIG")
        discoveryFitConfig.addSamples(commonSamples)

        if split=='100':
            CR_VV=discoveryFitConfig.addChannel("cuts",['CR_Dib'],cutsNBins,cutsBinLow,cutsBinHigh)
            CR_top=discoveryFitConfig.addChannel("cuts",['CR_Top'],cutsNBins,cutsBinLow,cutsBinHigh)

            if doSyst == True:

                for syst in thSystListzjet:
                    CR_VV.getSample("Zjets").removeSystematic(syst)
                CR_VV.getSample("Zjets").addSystematic(Systematic("CKKW_Zjets", 1., 0.9965, 1.0024, "user", "histoSys"))
                CR_VV.getSample("Zjets").addSystematic(Systematic("Factorization_Zjets", 1., 0.9742, 1.0234, "user", "histoSys"))
                CR_VV.getSample("Zjets").addSystematic(Systematic("Resummation_Zjets", 1., 0.9998, 0.9999, "user", "histoSys"))
                CR_VV.getSample("Zjets").addSystematic(Systematic("Renormalization_Zjets", 1., 0.9097, 1.0992, "user", "histoSys"))

                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+2.69)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+0.09)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+0.58)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+0.19)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+0.22)/0.01,0., 'user', 'histoSys'))
                CR_VV.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+0.53)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(0.01+21.15)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(0.01+2.16)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(0.01+4.77)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(0.01+1.19)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(0.01+1.65)/0.01,0., 'user', 'histoSys'))
                CR_top.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(0.01+9.53)/0.01,0., 'user', 'histoSys'))

            discoveryFitConfig.addBkgConstrainChannels([CR_VV,CR_top])

        SR_channel = discoveryFitConfig.addChannel("cuts", [ch], 1., 0.5, 1.5)

        if ch in fit_SF0J_Discovery and doSyst == True:
            for syst in thSystListzjet:
                SR_channel.getSample("Zjets").removeSystematic(syst)
            SR_channel.getSample("Zjets").addSystematic(Systematic("CKKW_Zjets", 1., 1.0009863, 0.9953885, "user", "histoSys"))
            SR_channel.getSample("Zjets").addSystematic(Systematic("Factorization_Zjets", 1., 0.98328, 1.015063, "user", "histoSys"))
            SR_channel.getSample("Zjets").addSystematic(Systematic("Resummation_Zjets", 1., 0.994787, 1.00246892, "user", "histoSys"))
            SR_channel.getSample("Zjets").addSystematic(Systematic("Renormalization_Zjets", 1., 1.007078, 1.001034, "user", "histoSys"))

        if ch == 'SRD_DF0J_81_SF0J_77' and doSyst == True:
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(25.52+3.79)/25.52,(25.52-3.55)/25.52, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(25.52+0.01)/25.52,(25.52-0.06)/25.52, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(25.52+3.61)/25.52,(25.52-3.69)/25.52, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(25.52+0.11)/25.52,(25.52-0.11)/25.52, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(25.52+0.20)/25.52,(25.52-0.24)/25.52, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(25.52+1.15)/25.52,(25.52-0.36)/25.52, 'user', 'histoSys'))
        elif ch == 'SRD_DF0J_81' and doSyst == True:
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(32.83+3.42)/32.83,(32.83-3.22)/32.83, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(32.83+0.24)/32.83,(32.83-0.23)/32.83, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(32.83+4.92)/32.83,(32.83-5.02)/32.83, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(32.83+0.09)/32.83,(32.83-0.09)/32.83, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(32.83+0.15)/32.83,(32.83-0.17)/32.83, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(32.83+1.27)/32.83,(32.83-0.66)/32.83, 'user', 'histoSys'))
        elif ch == 'SRD_DF0J_82' and doSyst == True:
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(28.75+2.66)/28.75,(28.75-2.50)/28.75, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(28.75+0.20)/28.75,(28.75-0.20)/28.75, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(28.75+5.01)/28.75,(28.75-5.09)/28.75, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(28.75+0.07)/28.75,(28.75-0.07)/28.75, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(28.75+0.11)/28.75,(28.75-0.12)/28.75, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(28.75+0.99)/28.75,(28.75-0.54)/28.75, 'user', 'histoSys'))
        elif ch == 'SRD_DF0J_83' and doSyst == True:
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(20.82+1.78)/20.82,(20.82-1.67)/20.82, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(20.82+0.21)/20.82,(20.82-0.20)/20.82, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(20.82+3.13)/20.82,(20.82-3.19)/20.82, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(20.82+0.04)/20.82,(20.82-0.04)/20.82, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(20.82+0.08)/20.82,(20.82-0.08)/20.82, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(20.82+0.92)/20.82,(20.82-0.52)/20.82, 'user', 'histoSys'))
        elif ch == 'SRD_DF0J_84' and doSyst == True:
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(7.76+0.87)/7.76,(7.76-0.82)/7.76, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(7.76+0.06)/7.76,(7.76-0.06)/7.76, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(7.76+0.71)/7.76,(7.76-0.72)/7.76, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(7.76+0.01)/7.76,(7.76-0.00)/7.76, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(7.76+0.05)/7.76,(7.76-0.06)/7.76, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(7.76+0.35)/7.76,(7.76-0.08)/7.76, 'user', 'histoSys'))
        elif ch == 'SRD_DF0J_85' and doSyst == True:
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(2.24+0.47)/2.24,(2.24-0.44)/2.24, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(2.24+0.01)/2.24,(2.24-0.01)/2.24, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(2.24+0.99)/2.24,(2.24-0.99)/2.24, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(2.24+0.00)/2.24,(2.24-0.00)/2.24, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(2.24+0.03)/2.24,(2.24-0.03)/2.24, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(2.24+0.10)/2.24,(2.24-0.01)/2.24, 'user', 'histoSys'))
        elif ch == 'SRD_SF0J_77' and doSyst == True:
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(27.67+6.30)/27.67,(27.67-5.86)/27.67, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(27.67+0.20)/27.67,(27.67-0.20)/27.67, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(27.67+5.86)/27.67,(27.67-5.95)/27.67, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(27.67+0.19)/27.67,(27.67-0.19)/27.67, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(27.67+0.37)/27.67,(27.67-0.43)/27.67, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(27.67+1.22)/27.67,(27.67-0.43)/27.67, 'user', 'histoSys'))
        elif ch == 'SRD_SF0J_78' and doSyst == True:
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(37.78+5.81)/37.78,(37.78-5.43)/37.78, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(37.78+0.32)/37.78,(37.78-0.32)/37.78, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(37.78+5.82)/37.78,(37.78-5.92)/37.78, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(37.78+0.16)/37.78,(37.78-0.16)/37.78, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(37.78+0.31)/37.78,(37.78-0.35)/37.78, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(37.78+1.67)/37.78,(37.78-0.79)/37.78, 'user', 'histoSys'))
        elif ch == 'SRD_SF0J_79' and doSyst == True:
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(39.00+5.07)/39.00,(39.00-4.74)/39.00, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(39.00+0.30)/39.00,(39.00-0.29)/39.00, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(39.00+6.32)/39.00,(39.00-6.43)/39.00, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(39.00+0.14)/39.00,(39.00-0.13)/39.00, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(39.00+0.25)/39.00,(39.00-0.28)/39.00, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(39.00+1.40)/39.00,(39.00-0.68)/39.00, 'user', 'histoSys'))
        elif ch == 'SRD_SF0J_80' and doSyst == True:
            SR_channel.getSample('ttbar').removeSystematic(thSyst_rad_ttbar)
            SR_channel.getSample('ttbar').addSystematic(Systematic('Radiation_ttbar','',1.862,0.864, 'user', 'histoSys'))
            SR_channel.getSample('ttbar').removeSystematic(thSyst_show_ttbar)
            SR_channel.getSample('ttbar').addSystematic(Systematic('Shower_ttbar','',1.05,0.95, 'user', 'histoSys'))

            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_STAT','',(37.23+4.19)/37.23,(37.23-3.93)/37.23, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHT','',(37.23+0.35)/37.23,(37.23-0.34)/37.23, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_XSEC','',(37.23+4.32)/37.23,(37.23-4.44)/37.23, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TOTAL','',(37.23+0.11)/37.23,(37.23-0.11)/37.23, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_EFF_TRIG_TOTAL','',(37.23+0.20)/37.23,(37.23-0.23)/37.23, 'user', 'histoSys'))
            SR_channel.getSample('FNP').addSystematic(Systematic('FNP_WEIGHTSYST','',(37.23+1.68)/37.23,(37.23-0.88)/37.23, 'user', 'histoSys'))

        discoveryFitConfig.addSignalChannels([SR_channel])
        SR_channel.addDiscoverySamples(["SIG_%s"%ch], [1.], [0.], [100.], [kMagenta])
