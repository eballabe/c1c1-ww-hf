import os
import argparse

parser = argparse.ArgumentParser(description='Input parser')
parser.add_argument('-r','--region', help='Region',required='True')
args = parser.parse_args()
region = args.region

doAsymptotic = True

if doAsymptotic == True:
    calculator = "-a"
elif doAsymptotic == False:
    calculator = "-n 3000"



regions = ['SRD_DF0J_81_SF0J_77','SRD_DF0J_81','SRD_DF0J_82','SRD_DF0J_83','SRD_DF0J_84','SRD_DF0J_85','SRD_SF0J_77','SRD_SF0J_78','SRD_SF0J_79','SRD_SF0J_80']

if region == 'SRD_DF0J_81_SF0J_77':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 220 -N 20 " + calculator + " > log_upperlimit_" + region + ".out &")
elif region == 'SRD_DF0J_81':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 200 -N 20 " + calculator + " > log_upperlimit_" + region + ".out &")
elif region == 'SRD_DF0J_82':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 120 -N 20 " + calculator + " > log_upperlimit_" + region + ".out &")
elif region == 'SRD_DF0J_83':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 100 -N 20 " + calculator + " > log_upperlimit_" + region + ".out &")
elif region == 'SRD_DF0J_84':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 70 -N 20 " + calculator + " > log_upperlimit_" + region + ".out &")
elif region == 'SRD_DF0J_85':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 50 -N 20 " + calculator + " > log_upperlimit_" + region + ".out &")
elif region == 'SRD_SF0J_77':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 100 -N 100 " + calculator + " > log_upperlimit_" + region + ".out &")
elif region == 'SRD_SF0J_78':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 80 -N 100 " + calculator + " > log_upperlimit_" + region + ".out &")
elif region == 'SRD_SF0J_79':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 50 -N 100 " + calculator + " > log_upperlimit_" + region + ".out &")
elif region == 'SRD_SF0J_80':
    os.system("nohup UpperLimitTable2.py -c " + region + " -w results/Discovery_withSyst_0/Discovery_" + region + "_combined_NormalMeasurement_model.root -l 138.95 -p mu_SIG_" + region + " -o UpperLimitTable_" + region + ".tex -R 30 -N 100 " + calculator + " > log_upperlimit_" + region + ".out &")
else:
    raise Exception("Which region is this?")
