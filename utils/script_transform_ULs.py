#/usr/bin/pyhton

import json
with open('json/combined_upperlimit__1_harvest_list.json') as f:
  data = json.load(f)


XSec = {
    100 : 11611.9,
    110 : 8156.42,
    125 : 5090.52,
    130 : 4407.44,
    140 : 3359.83,
    150 : 2612.31,
    160 : 2062.22,
    175 : 1482.42,
    200 : 902.569,
    225 : 579.564,
    250 : 387.534,
}


for i in data:

    print('m_Chargino = {}, m_Neutralino = {}, UL_on_mu = {}'.format(i['m0'], i['m12'], i['upperLimit']))

    data[data.index(i)]['upperLimit']                  = data[data.index(i)]['upperLimit']*XSec[i['m0']]
    data[data.index(i)]['expectedUpperLimit']          = data[data.index(i)]['expectedUpperLimit']*XSec[i['m0']]
    data[data.index(i)]['expectedUpperLimitPlus1Sig']  = data[data.index(i)]['expectedUpperLimitPlus1Sig']*XSec[i['m0']]
    data[data.index(i)]['expectedUpperLimitMinus1Sig'] = data[data.index(i)]['expectedUpperLimitMinus1Sig']*XSec[i['m0']]
    data[data.index(i)]['expectedUpperLimitPlus2Sig']  = data[data.index(i)]['expectedUpperLimitPlus2Sig']*XSec[i['m0']]
    data[data.index(i)]['expectedUpperLimitMinus2Sig'] = data[data.index(i)]['expectedUpperLimitMinus2Sig']*XSec[i['m0']]

    print('m_Chargino = {}, m_Neutralino = {}, UL_on_xsec = {}'.format(i['m0'], i['m12'], data[data.index(i)]['upperLimit']))

json_object = json.dumps(data, indent=4)

with open("sample.json", "w") as outfile:
    outfile.write(json_object)
