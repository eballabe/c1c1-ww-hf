#!/usr/bin/python3
# This macro modifies latex Yields/Syst tables from HistFitter to be inserted into the ATLAS INT note

import fileinput
import argparse

parser = argparse.ArgumentParser(description='Parser')
parser.add_argument('-i','--input',  default='YieldsTable.tex', action='store', type=str, help='YieldsTable.tex/SystTable.tex')
args = parser.parse_args()
inputfile = args.input


fields = {}
fields["CR\_Dib"] = "CR\_VV"
fields["VR\_Dib\_DF0J"] = "VR\_VV\_DF0J"
fields["VR\_Dib\_SF0J"] = "VR\_VV\_SF0J"


if "YieldsTable" in inputfile: # these changes are needed for the Yields Tables
    fields["\\centering"] = "\\begin{center}"
    fields["\\small"] = "\\setlength{\\tabcolsep}{0.0pc} {\\tiny"
    fields["\\end{tabular*}"] = "\\end{tabular*}} \\end{center}"

if "SystTable" in inputfile: # these changes are needed for the Syst Tables
    fields["\\small"] = " \\tiny \\scalebox{0.65}{"
    fields["\\textbf{Uncertainty of channel}         "] = "\\noalign{\\smallskip}\\hline\\noalign{\\smallskip} {Uncertainty of channel}" 
    fields["\\toprule"] = "" 
    fields["\\end{tabular*}"] = "\\end{tabular*}}"


for line in fileinput.input(inputfile, inplace=True):
    line = line.rstrip()
    for field in fields:
        if field in line:
            line = line.replace(field, fields[field])

    print (line)
