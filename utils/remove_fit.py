import os
import argparse

parser = argparse.ArgumentParser(description='Remove failed fits :( sliced in DM')
parser.add_argument('--DM', type=int, help='DM')
args = parser.parse_args()

DM = args.DM

cmd1 = 'echo Removing DM {} points'.format(DM)
cmd2 = 'rm -rf results/Exclusion_withSyst_{}* config/Exclusion_withSyst_{}* data/Exclusion_withSyst_{}*'.format(DM,DM,DM)

os.system(cmd1) 
os.system(cmd2)
