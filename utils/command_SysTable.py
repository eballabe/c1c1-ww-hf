import os

workspace = 'results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root'

CRVR_names = 'CR_Dib,CR_Top,VR_Dib_DF0J,VR_Dib_SF0J,VR_Top_DF1J,VR_Top_SF1J,VR_Top_DF0J,VR_Top_SF0J'
SRDF0J_1_names = 'SR_DF0J_81_8125,SR_DF0J_8125_815,SR_DF0J_815_8175,SR_DF0J_8175_82,SR_DF0J_82_8225,SR_DF0J_8225_825,SR_DF0J_825_8275,SR_DF0J_8275_83'
SRDF0J_2_names = 'SR_DF0J_83_8325,SR_DF0J_8325_835,SR_DF0J_835_8375,SR_DF0J_8375_84,SR_DF0J_84_845,SR_DF0J_845_85,SR_DF0J_85_86,SR_DF0J_86'
SRSF0J_1_names = 'SR_SF0J_77_775,SR_SF0J_775_78,SR_SF0J_78_785,SR_SF0J_785_79,SR_SF0J_79_795,SR_SF0J_795_80,SR_SF0J_80_81,SR_SF0J_81'


label_CRVR = 'Table:SystTable-CRVR'
label_SRDF0J_1 = 'Table:SystTable-SRDF0J_1'
label_SRDF0J_2 = 'Table:SystTable-SRDF0J_2'
label_SRSF0J_1 = 'Table:SystTable-SRSF0J_1'

cmd_CRVR = 'SysTable.py -c ' + CRVR_names + ' -w ' + workspace + ' -o SystTable_CRVR.tex  -L \"' + label_CRVR + '\" -%'
cmd_SRDF0J_1 = 'SysTable.py -c ' + SRDF0J_1_names +' -w ' + workspace + ' -o SystTable_DF0J_1.tex  -L \"' + label_SRDF0J_1 + '\" -%'
cmd_SRDF0J_2 = 'SysTable.py -c ' + SRDF0J_2_names +' -w ' + workspace + ' -o SystTable_DF0J_2.tex  -L \"' + label_SRDF0J_2 + '\" -%'
cmd_SRSF0J_1 = 'SysTable.py -c ' + SRSF0J_1_names +' -w ' + workspace + ' -o SystTable_SF0J_1.tex  -L \"' + label_SRSF0J_1 + '\" -%'

os.system(cmd_CRVR)
os.system(cmd_SRDF0J_1)
os.system(cmd_SRDF0J_2)
os.system(cmd_SRSF0J_1)


cmd_merge = 'cat SystTable_CRVR.tex SystTable_DF0J_1.tex SystTable_DF0J_2.tex SystTable_SF0J_1.tex  > SystTable.tex'
cmd_remove = 'rm SystTable_CRVR.* SystTable_DF0J_1.* SystTable_DF0J_2.* SystTable_SF0J_1.* '

os.system(cmd_merge)
os.system(cmd_remove)

'''
#For a specific sample

#samples = 'FNP,Wt,Zjets,Zttjets,VV,other,VVV,ttbar'
samples = 'VV'

cmd_CRVR = 'SysTable.py -s ' + samples + ' -c ' + CRVR_names + ' -w ' + workspace + ' -o SystTable_CRVR.tex  -L \"' + label_CRVR + '\" -%'
cmd_SRDF0J_1 = 'SysTable.py -s ' + samples + ' -c ' + SRDF0J_1_names +' -w ' + workspace + ' -o SystTable_DF0J_1.tex  -L \"' + label_SRDF0J_1 + '\" -%'
cmd_SRDF0J_2 = 'SysTable.py -s ' + samples + ' -c ' + SRDF0J_2_names +' -w ' + workspace + ' -o SystTable_DF0J_2.tex  -L \"' + label_SRDF0J_2 + '\" -%'
cmd_SRSF0J_1 = 'SysTable.py -s ' + samples + ' -c ' + SRSF0J_1_names +' -w ' + workspace + ' -o SystTable_SF0J_1.tex  -L \"' + label_SRSF0J_1 + '\" -%'
'''
