import os

samples = 'VV,ttbar,Wt,FNP,Zjets,Zttjets,VVV,other'
workspace = 'results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root'


CRVR_names = 'CR_Dib,CR_Top,VR_Dib_DF0J,VR_Dib_SF0J,VR_Top_DF1J,VR_Top_SF1J,VR_Top_DF0J,VR_Top_SF0J'
SRDF0J_1_names = 'SR_DF0J_81_8125,SR_DF0J_8125_815,SR_DF0J_815_8175,SR_DF0J_8175_82,SR_DF0J_82_8225,SR_DF0J_8225_825,SR_DF0J_825_8275,SR_DF0J_8275_83'
SRDF0J_2_names = 'SR_DF0J_83_8325,SR_DF0J_8325_835,SR_DF0J_835_8375,SR_DF0J_8375_84,SR_DF0J_84_845,SR_DF0J_845_85,SR_DF0J_85_86,SR_DF0J_86'
SRSF0J_1_names = 'SR_SF0J_77_775,SR_SF0J_775_78,SR_SF0J_78_785,SR_SF0J_785_79,SR_SF0J_79_795,SR_SF0J_795_80,SR_SF0J_80_81,SR_SF0J_81'

caption_CRVR = 'Event yields pre (bottom) and post (top) fit in the exclusive CRs and VRs. The uncertainty includes both statistical and systematical contributions.'
caption_SR = 'Event yields pre (bottom) and post (top) fit in the exclusive SRs. The uncertainty includes both statistical and systematical contributions.'

label_CRVR = 'Table:YieldsTable-CRVR'
label_SRDF0J_1 = 'Table:YieldsTable-SRDF0J_1'
label_SRDF0J_2 = 'Table:YieldsTable-SRDF0J_2'
label_SRSF0J_1 = 'Table:YieldsTable-SRSF0J_1'

cmd_CRVR = 'YieldsTable.py -c ' + CRVR_names + ' -s ' + samples +' -w ' + workspace + ' -o YieldsTable_CRVR.tex -b  -C \" ' + caption_CRVR + '\" -L \"' + label_CRVR + '\" '
cmd_SRDF0J_1 = 'YieldsTable.py -c ' + SRDF0J_1_names + ' -s ' + samples +' -w ' + workspace + ' -o YieldsTable_DF0J_1.tex -b  -C \"' + caption_SR + '\" -L \"' + label_SRDF0J_1 + '\" '
cmd_SRDF0J_2 = 'YieldsTable.py -c ' + SRDF0J_2_names + ' -s ' + samples +' -w ' + workspace + ' -o YieldsTable_DF0J_2.tex -b  -C \"' + caption_SR + '\" -L \"' + label_SRDF0J_2 + '\" '
cmd_SRSF0J_1 = 'YieldsTable.py -c ' + SRSF0J_1_names + ' -s ' + samples +' -w ' + workspace + ' -o YieldsTable_SF0J_1.tex -b   -C \"' + caption_SR + '\" -L \"' + label_SRSF0J_1 + '\" '

os.system(cmd_CRVR)
os.system(cmd_SRDF0J_1)
os.system(cmd_SRDF0J_2)
os.system(cmd_SRSF0J_1)


cmd_merge = 'cat YieldsTable_CRVR.tex YieldsTable_DF0J_1.tex YieldsTable_DF0J_2.tex YieldsTable_SF0J_1.tex  > YieldsTable.tex'
cmd_remove = 'rm YieldsTable_CRVR.* YieldsTable_DF0J_1.* YieldsTable_DF0J_2.* YieldsTable_SF0J_1.* '

os.system(cmd_merge)
os.system(cmd_remove)



'''
MERGED SAMPLES

CRS

YieldsTable.py -c CR_Dib_cuts,CR_Top_cuts -s VV,ttbar,Wt,FNP,[Zjets,Zttjets,VVV,other] -w results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o Yields_CR_PAPER.tex -b -C "Event yields pre (bottom) and post (top) fit in the exclusive CRs. The uncertainty includes both statistical and systematical contributions." -L "Table:YieldsTable-CR"

YieldsTable.py -c VR_Dib_DF0J_cuts,VR_Dib_SF0J_cuts,VR_Top_DF1J_cuts,VR_Top_SF1J_cuts,VR_Top_DF0J_cuts,VR_Top_SF0J_cuts -s VV,ttbar,Wt,FNP,[Zjets,Zttjets,VVV,other] -w results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o Yields_VR_PAPER.tex -b -C "Event yields pre (bottom) and post (top) fit in the exclusive VRs. The uncertainty includes both statistical and systematical contributions." -L "Table:YieldsTable-VR"

'''



