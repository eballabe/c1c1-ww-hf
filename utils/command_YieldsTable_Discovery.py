import os
import os
import argparse

parser = argparse.ArgumentParser(description='Discovery fit')
parser.add_argument('--tabletype', type=str, help='YieldsTable/SysTable')
parser.add_argument('--region', type=str, help='region')
args = parser.parse_args()

tabletype = args.tabletype
region = args.region


samples = 'DiscoveryMode_SIG_'+ region +',VV,ttbar,Wt,FNP,Zjets,Zttjets,VVV,other'
workspace = 'results/Discovery_withSyst_0/Discovery_' + region + '_combined_NormalMeasurement_model_afterFit.root'


caption = 'Event yields pre (bottom) and post (top) fit in the inclusive ' + region.replace("_","\_") +' SR. The uncertainty includes both statistical and systematical contributions.'
label = 'Table:YieldsTable-' + region

if tabletype == 'YieldsTable':
    cmd = 'YieldsTable.py -c ' + region + ' -s ' + samples +' -w ' + workspace + ' -o YieldsTable.tex -b  -C \" ' + caption + '\" -L \"' + label + '\" '

if tabletype == 'YieldsTable_COMPLETE':
    cmd = 'YieldsTable.py -c CR_Dib,CR_Top,' + region + ' -s ' + samples +' -w ' + workspace + ' -o YieldsTable_COMPLETE.tex -b  -C \" ' + caption + '\" -L \"' + label + '\" '


if tabletype == 'SysTable_BEFORE':
    cmd = 'SysTable.py -c ' + region + ' -w ' + workspace + ' -o SystematicTable_BEFORE.tex  -b -%'
if tabletype == 'SysTable_AFTER':
    cmd = 'SysTable.py -c ' + region + ' -w ' + workspace + ' -o SystematicTable_AFTER.tex  -%'

if tabletype == 'SysTable_BEFORE_COMPLETE':
    cmd = 'SysTable.py -c CR_Dib,CR_Top,' + region + ' -w ' + workspace + ' -o SystematicTable_BEFORE_COMPLETE.tex  -b -%'
if tabletype == 'SysTable_AFTER_COMPLETE':
    cmd = 'SysTable.py -c CR_Dib,CR_Top,' + region + ' -w ' + workspace + ' -o SystematicTable_AFTER_COMPLETE.tex  -%'

os.system(cmd)


# cat SRD_DF0J_81_SF0J_77/YieldsTable.tex SRD_DF0J_81/YieldsTable.tex SRD_DF0J_82/YieldsTable.tex SRD_DF0J_83/YieldsTable.tex SRD_DF0J_84/YieldsTable.tex SRD_DF0J_85/YieldsTable.tex SRD_SF0J_77/YieldsTable.tex SRD_SF0J_78/YieldsTable.tex SRD_SF0J_79/YieldsTable.tex SRD_SF0J_80/YieldsTable.tex > YieldsTable.tex

# cat SRD_DF0J_81_SF0J_77/SystematicTable_AFTER.tex SRD_DF0J_81/SystematicTable_AFTER.tex SRD_DF0J_82/SystematicTable_AFTER.tex SRD_DF0J_83/SystematicTable_AFTER.tex SRD_DF0J_84/SystematicTable_AFTER.tex SRD_DF0J_85/SystematicTable_AFTER.tex SRD_SF0J_77/SystematicTable_AFTER.tex SRD_SF0J_78/SystematicTable_AFTER.tex SRD_SF0J_79/SystematicTable_AFTER.tex SRD_SF0J_80/SystematicTable_AFTER.tex > SystematicTable.tex
