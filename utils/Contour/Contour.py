#!/usr/bin/env python

# contourPlotterExample.py #################
#
# Example for using contourPlotter.py
#
# See README for details
#
# By: Larry Lee - Dec 2017

import ROOT

import contourPlotter
import argparse

parser = argparse.ArgumentParser(description='Parser')
parser.add_argument('-i','--input',  default='outputGraphs.root', action='store', type=str, help='input outputGraphs.root')
parser.add_argument('-iUL','--inputUL',  default='outputGraphs_withULs_lesspoints.root', action='store', type=str, help='input outputGraphs_UL.root')
parser.add_argument('-g','--gridType',  default='none', action='store', type=str, help='gridType: observed or expected')
args = parser.parse_args()
input_file = args.input
input_file_ULs = args.inputUL
grid_type = args.gridType


f = ROOT.TFile(input_file)
f.ls()

drawTheorySysts = True

plot = contourPlotter.contourPlotter("contourPlotterExample",800,600)

plot.processLabel = "#tilde{#chi}_{1}^{+}#tilde{#chi}_{1}^{-} #rightarrow W^{+}#tilde{#chi}_{1}^{0}W^{-}#tilde{#chi}_{1}^{0}"
plot.ATLASLabel = "  #it{ATLAS} #bf{Internal}"
plot.lumiLabel = "  #bf{#sqrt{s}=13 TeV, 139 fb^{-1}}"
plot.limitLabel = "  #bf{All limits at 95% CL}"

## Axes

plot.drawAxes( [100,1,180,80] )

## Other limits to draw
inFile_13TeV = ROOT.TFile.Open("/users2/ballaben/c1c1-ww-hf/utils/Contour/ContourATLAS13TeV.root" ,"READ")
mygraph = inFile_13TeV.Get("Graph1D_y1")
plot.drawShadedRegion( mygraph, color=ROOT.kGray, alpha=1., title="ATLAS 13 TeV, arXiv:1908.08215" )


inFile_8TeV = ROOT.TFile.Open("/users2/ballaben/c1c1-ww-hf/utils/Contour/ContourATLAS8TeV.root" ,"READ")
mygraph8Tev = inFile_8TeV.Get("Graph1D_y1")
plot.drawShadedRegion( mygraph8Tev, color=ROOT.kAzure-4, alpha=1., title="ATLAS 8 TeV, arXiv:1403.5294" )


## Main Result
if grid_type=='observed':
    plot.drawTextFromTGraph2D( f.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")
elif grid_type=='expected':
    plot.drawTextFromTGraph2D( f.Get("CLsexp_gr")  , angle=30 , title = "Grey Numbers Represent Expected CLs Value")
elif grid_type=='none':
    print("no grid")

plot.drawOneSigmaBand(  f.Get("Band_1s_0")   )
plot.drawExpected(      f.Get("Exp_0")       )
plot.drawObserved(      f.Get("Obs_0"), title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed Limit")


## Draw Lines
# plot.drawLine(  coordinates = [0,0,800,800], label = "Kinematically Forbidden or blah", style = 7, angle = 30 )

## Axis Labels

plot.setXAxisLabel( "m(#tilde{#chi}_{1}^{#pm}) [GeV]" )
plot.setYAxisLabel( "m(#tilde{#chi}_{1}^{0}) [GeV]"  )

if drawTheorySysts:
	plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up") )
	plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down") )
	# coordinate in NDC
	plot.drawTheoryLegendLines( xyCoord=(0.264,0.677), length=0.06 )


inFile_LEP = ROOT.TFile.Open("/users2/ballaben/c1c1-ww-hf/utils/Contour/ContourLEP208GeV.root" ,"READ")
mygraphLEP = inFile_LEP.Get("Graph1D_y1")
plot.drawShadedRegion( mygraphLEP, color=96, alpha=1., title="LEP excluded" )


if grid_type=='UL':
    f_UL = ROOT.TFile(input_file_ULs)
    plot.drawTextFromTGraph2D( f_UL.Get("upperLimit_gr")  , angle=40 , title = "Grey Numbers Represent Observed ULs on Cross Sections [fb]" , format = "%.1f")


plot.drawLine(coordinates= [100.35, 20., 160.35, 80], label='m(#tilde{#chi}_{1}^{#pm}) - m(#tilde{#chi}_{1}^{0}) = m(W)', labelLocation=[146, 62], angle=40)
#plot.drawLine(coordinates= [50.,  50., 200, 200.], label='m(#tilde{#chi}_{1}^{#pm}) < m(#tilde{#chi}_{1}^{0})', labelLocation=[165, 170], angle=40)

plot.createLegend(shape=(0.25,0.53,0.60,0.76) ).Draw()
plot.decorateCanvas( )
if grid_type=='observed':
    plot.writePlot("observedGridType.pdf")
elif grid_type=='expected':
    plot.writePlot("expectedGridType.pdf")
elif grid_type=='UL':
    plot.writePlot("ULGridType.pdf")
elif grid_type=='none':
    plot.writePlot("pdf")
