#!/usr/bin/env python

import os, sys, argparse, json

"""
adds the mass splitting deltaM to the json files so the usual visualization
deltaM vs mSUSY can also be used with the HistFitter interpolation scripts
"""

def add_info(data):
    for entry in data:
        mSUSY = entry["m0"]
        mLSP  = entry["m12"]
        deltaM = mSUSY - mLSP
        entry["deltaM"] = deltaM

def process_file(filename):
    print(" >> augmenting information in file " + filename)
    f = open(filename, "r")
    data = json.load(f)
    f.close()

    # add info to dictionary
    add_info(data)
    # overwrite file with augmented dictionary
    with open(filename, "w") as f:
        f.write( json.dumps(data, indent=4) )

parser = argparse.ArgumentParser()
parser.add_argument("--inputFile","-i",  type = str, help="input json file")

args = parser.parse_args()

# start with processing the given file
process_file(args.inputFile)

# try to augment information also for variations of signal crosssections
if os.path.exists(args.inputFile.replace("Nominal", "Up")):
    process_file(args.inputFile.replace("Nominal", "Up"))
if os.path.exists(args.inputFile.replace("Nominal", "Down")):
    process_file(args.inputFile.replace("Nominal", "Down"))
