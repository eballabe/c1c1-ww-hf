#!/usr/bin/env python

import os, sys, argparse, csv

import ROOT

"""
creates csv files for plotting from the root file from harvestToContours.py
"""

parser = argparse.ArgumentParser()
parser.add_argument("--inputFile","-i",  type = str, help="input json file")
parser.add_argument("--outputFile","-o",  type = str, help="output csv file")
parser.add_argument("--name","-n",  type = str, help="name of graph to retrieve")
parser.add_argument("--logY",  help="if interpolation was done in logY, specify this option to make it back to dM", default=False,action="store_true")

args = parser.parse_args()

if not os.path.exists(args.inputFile):
    sys.exit("input file {} does not exist!".format(args.inputFile))

# retrieve the TGraph from the file
t = ROOT.TFile(args.inputFile)
graph = t.Get(args.name)

print("extracting contour " + args.name)

# write contour to csv file
with open(args.outputFile, 'w') as f:
    print("saving contour to " + args.outputFile)
    output = csv.writer(f)
    # header row
    output.writerow(["mSUSY", "deltaM"])
    # points of contour
    for i in range(graph.GetN()):
      x = round(graph.GetX()[i], 4)
      y = round(graph.GetY()[i], 4)
      if args.logY: y = round(10**y, 4)
      output.writerow([x, y])
