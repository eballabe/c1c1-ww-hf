#!/usr/bin/env python

# contourPlotterExample.py #################
#
# Example for using contourPlotter.py
#
# See README for details
#
# By: Larry Lee - Dec 2017

import ROOT

import contourPlotter
import argparse

parser = argparse.ArgumentParser(description='Parser')
parser.add_argument('-i','--input',  default='outputGraphs.root', action='store', type=str, help='input outputGraphs.root')
parser.add_argument('-g','--gridType',  default='none', action='store', type=str, help='gridType: observed or expected')
args = parser.parse_args()
input_file = args.input
grid_type = args.gridType


f = ROOT.TFile(input_file)
f.ls()

drawTheorySysts = True

plot = contourPlotter.contourPlotter("contourPlotterExample",800,600)

plot.processLabel = "#tilde{#chi}_{1}^{+}#tilde{#chi}_{1}^{-} #rightarrow W^{+}#tilde{#chi}_{1}^{0}W^{-}#tilde{#chi}_{1}^{0}"
plot.ATLASLabel = "#it{ATLAS} #bf{Internal}"
plot.lumiLabel = "#bf{#sqrt{s}=13 TeV, 139 fb^{-1}}"


## Axes
plot.drawAxes( [100,70,180,200] )


x_new = lambda x,y : x
y_new = lambda x,y : x - y


## Other limits to draw
inFile_13TeV = ROOT.TFile.Open("/users2/ballaben/c1c1-ww-hf/utils/Contour/ContourATLAS13TeV.root" ,"READ")
mygraph13Tev_compressed = plot.rotateTGraph(inFile_13TeV.Get("Graph1D_y1"), x_new, y_new)
plot.drawShadedRegion( mygraph13Tev_compressed, color=ROOT.kGray, alpha=1., title="ATLAS 13 TeV Observed limit (arXiv:1908.08215)" )


inFile_8TeV = ROOT.TFile.Open("/users2/ballaben/c1c1-ww-hf/utils/Contour/ContourATLAS8TeV.root" ,"READ")
mygraph8TeV_compressed = plot.rotateTGraph(inFile_8TeV.Get("Graph1D_y1"), x_new, y_new)
plot.drawShadedRegion( mygraph8TeV_compressed, color=ROOT.kAzure-4, alpha=1., title="ATLAS 8 TeV Observed limit (arXiv:1403.5294)" )


inFile_LEP = ROOT.TFile.Open("/users2/ballaben/c1c1-ww-hf/utils/Contour/ContourLEP208GeV.root" ,"READ")
mygraphLEP_compressed = plot.rotateTGraph(inFile_LEP.Get("Graph1D_y1"), x_new, y_new)
plot.drawShadedRegion( mygraphLEP_compressed, color=96, alpha=1., title="LEP Observed Limit" )


expected_contour = plot.rotateTGraph(f.Get("Exp_0"), x_new, y_new)
expected_OneSignaBand =  plot.rotateTGraph(f.Get("Band_1s_0"), x_new, y_new)
observed_contour = plot.rotateTGraph(f.Get("Obs_0"), x_new, y_new)
if drawTheorySysts:
    TheoryUncertaintyCurveUp = plot.rotateTGraph(f.Get("Obs_0_Up"), x_new, y_new)
    TheoryUncertaintyCurveDown = plot.rotateTGraph(f.Get("Obs_0_Down"), x_new, y_new)

plot.drawExpected(     expected_contour       )
plot.drawOneSigmaBand(  expected_OneSignaBand   )
plot.drawObserved(   observed_contour   , title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed Limit")

## Main Result
if grid_type=='observed':
    observed_grid = plot.rotateTGraph2D(f.Get("CLs_gr"), x_new, y_new)
    plot.drawTextFromTGraph2D( observed_grid  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")
elif grid_type=='expected':
    expected_grid = plot.rotateTGraph2D(f.Get("CLsexp_gr"), x_new, y_new)
    plot.drawTextFromTGraph2D( expected_grid  , angle=30 , title = "Grey Numbers Represent Expected CLs Value")
elif grid_type=='none':
    print("no grid")


# plot.drawLine(  coordinates = [0,0,800,800], label = "Kinematically Forbidden or blah", style = 7, angle = 30 )

## Axis Labels
plot.setXAxisLabel( "m(#tilde{#chi}_{1}^{#pm}) [GeV]" )
plot.setYAxisLabel( "#Deltam(#tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0}) [GeV]"  )

plot.createLegend(shape=(0.22,0.58,0.55,0.77) ).Draw()

if drawTheorySysts:
    plot.drawTheoryUncertaintyCurve( TheoryUncertaintyCurveUp )
    plot.drawTheoryUncertaintyCurve( TheoryUncertaintyCurveDown )
    # coordinate in NDC
    plot.drawTheoryLegendLines( xyCoord=(0.234,0.699), length=0.057 )

plot.decorateCanvas( )
if grid_type=='observed':
    plot.writePlot("Compressed_observedGridType.pdf")
if grid_type=='expected':
    plot.writePlot("Compressed_expectedGridType.pdf")
elif grid_type=='none':
    plot.writePlot("Compressed.pdf")
