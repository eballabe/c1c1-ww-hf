#  Charginos through Ws 2nd wave analysis  (eric.ballabene@cern.ch)

import os
import argparse

samples=['100_10','100_20','100_30','100_40',
'125_25','125_35','125_45','125_55','125_65',
'150_25','150_50','150_60','150_70','150_80', 
'175_50','175_75','175_85','175_95',
'200_75','200_100',   
'225_100','225_125',
'250_125','250_150']
samples+=['100_1','125_1','150_1','175_1','200_1','175_25','200_25','200_50']
samples+=['110_20','130_20','140_20','140_40','150_40','160_1','160_30']

fixSigXSec = True 
doSyst = True

parser = argparse.ArgumentParser(description='Parser to control which step is executed')
parser.add_argument("--make_hypotests", action='store_true', default=False, help='1st step - compute p-values with -p option')
parser.add_argument("--make_upperlimits", action='store_true', default=False, help='1st step - compute upper limits with -l option')
parser.add_argument("--hadd_hypotests", action='store_true', default=False, help='2nd step - ROOT hadd the hypotests')
parser.add_argument("--hadd_upperlimits", action='store_true', default=False, help='2nd step - ROOT hadd the upper limits')
parser.add_argument("--generate_JSON_pval", action='store_true', default=False, help='3rd step - generate a combined_json.json from the hadded hypotests')
parser.add_argument("--generate_JSON_uls", action='store_true', default=False, help='3rd step - generate a combined_json.json from the hadded upper limits')
parser.add_argument("--make_contours", action='store_true', default=False, help='4th step - generate outputGraphs.root from combined_json.json')
parser.add_argument("--make_harvest_uls", action='store_true', default=False, help='4th step - generate outputGraphs.root from combined_json.json')
args = parser.parse_args()

make_hypotests = args.make_hypotests
make_upperlimits = args.make_upperlimits
hadd_hypotests = args.hadd_hypotests
hadd_upperlimits = args.hadd_upperlimits
generate_JSON_pval = args.generate_JSON_pval
generate_JSON_uls = args.generate_JSON_uls
make_contours = args.make_contours
make_harvest_uls = args.make_harvest_uls


if make_hypotests == True:
    if os.path.isdir("log_plimits") == False:
        os.system("mkdir log_plimits")
    for sample in samples:
        if doSyst == False:
            os.system("nohup HistFitter.py -p -F excl -u\"--doExclusion --point {}\" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_plimits/log_plimit_{}.out &".format(sample,sample))
        if doSyst == True:
            os.system("nohup HistFitter.py -p -F excl -u\"--doExclusion --doSyst --point {}\" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_plimits/log_plimit_{}.out &".format(sample,sample))


if make_upperlimits == True:
    if os.path.isdir("log_upper_limits") == False:
        os.system("mkdir log_upper_limits")
    for sample in samples:
        os.system("nohup HistFitter.py -l -F excl -u\"--doExclusion --doSyst --point {}\" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_upper_limits/log_upper_limits_{}.out &".format(sample,sample))



if hadd_hypotests == True:
    if fixSigXSec == False:
        if doSyst == False:
            os.system("hadd combined_hypotest.root results/Exclusion_noSyst_*_hypotest.root")
        if doSyst == True:
            os.system("hadd combined_hypotest.root results/Exclusion_withSyst_*_hypotest.root")
    elif fixSigXSec == True:  
        if doSyst == False: 
            os.system("hadd combined_hypotest_Nominal.root results/Exclusion_noSyst_*_fixSigXSecNominal_hypotest.root")
            os.system("hadd combined_hypotest_Up.root results/Exclusion_noSyst_*_fixSigXSecUp_hypotest.root")
            os.system("hadd combined_hypotest_Down.root results/Exclusion_noSyst_*_fixSigXSecDown_hypotest.root")
        if doSyst == True:
            os.system("hadd combined_hypotest_Nominal.root results/Exclusion_withSyst_*_fixSigXSecNominal_hypotest.root")
            os.system("hadd combined_hypotest_Up.root results/Exclusion_withSyst_*_fixSigXSecUp_hypotest.root")
            os.system("hadd combined_hypotest_Down.root results/Exclusion_withSyst_*_fixSigXSecDown_hypotest.root")

if hadd_upperlimits == True:
    if doSyst == False:
        os.system("hadd combined_upperlimit.root results/Exclusion_noSyst_*_upperlimit.root")
    if doSyst == True:
        os.system("hadd combined_upperlimit.root results/Exclusion_withSyst_*_upperlimit.root")




if generate_JSON_pval == True:
    if fixSigXSec == False:
        cmd = 'GenerateJSONOutput.py -i combined_hypotest.root -f hypo_C1C1_%f_%f'
        print(cmd)
        os.system(cmd)
        if os.path.isdir("json") == False:
            os.system("mkdir json")
        os.system("mv combined_hypotest__1_harvest_list.json json/.")
    elif fixSigXSec == True:
        cmd = 'GenerateJSONOutput.py -i combined_hypotest_Nominal.root -f hypo_C1C1_%f_%f' # this will pick also Up/Down hypotests
        print(cmd)
        os.system(cmd)
        if os.path.isdir("json") == False:
            os.system("mkdir json")
        os.system("mv combined_hypotest_*__1_harvest_list.json json/.")



if generate_JSON_uls == True:
        cmd = 'GenerateJSONOutput.py -i combined_upperlimit.root -f hypo_C1C1_%f_%f'
        print(cmd)
        os.system(cmd)
        if os.path.isdir("json") == False:
            os.system("mkdir json")
        os.system("mv combined_upperlimit__1_harvest_list.json json/.")


#export ALRB_rootVersion=6.22.00-x86_64-centos7-gcc8-opt
#source setupATLAS.sh 
#source histfitter_v1.0.0/setup.sh 
#lsetup "views LCG_98python3_ATLAS_1 x86_64-centos7-gcc8-opt"


if make_contours == True:
    if fixSigXSec == False:
        #os.system("harvestToContours.py -i json/combined_hypotest__1_harvest_list.json -x m0 -y m12 -l None")
        os.system("harvestToContours.py -i json/combined_hypotest__1_harvest_list.json -x m0 -y m12 -l None --xMin 100 --xMax 250 --yMin 1 --yMax 150")
    if fixSigXSec == True:
        os.system("harvestToContours.py -i json/combined_hypotest_Nominal__1_harvest_list.json -x m0 -y m12 -l None --xMin 100 --xMax 250 --yMin 1 --yMax 150") # this will pick also Up/Down hypotests


if make_harvest_uls == True:
        os.system("harvestToContours.py -i sample.json -x m0 -y m12 -l None --xMin 100 --xMax 250 --yMin 1 --yMax 150")


# 5 step - plot contour
# on MacOs laptop: python contourPlotterExample.py -i outputGraphs.root




'''
# step needed only to combined SRs (or points if not ROOT hadded) - multiplex JSON
if fixSigXSec == False:
    if doSyst == False:
        os.system("multiplexJSON.py -i json/Exclusion_noSyst_* -o json/combined_json.json --modelDef m0,m12")
    if doSyst == True:
        os.system("multiplexJSON.py -i json/Exclusion_withSyst_* -o json/combined_json.json --modelDef m0,m12")
'''
