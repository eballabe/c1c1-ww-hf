#!/usr/bin/env python

# contourPlotterExample.py #################
#
# Example for using contourPlotter.py
#
# See README for details
#
# By: Larry Lee - Dec 2017

import ROOT

import contourPlotter
import argparse

parser = argparse.ArgumentParser(description='Parser')
parser.add_argument('-i','--input',  default='outputGraphs.root', action='store', type=str, help='input outputGraphs.root')
parser.add_argument('-g','--gridType',  default='observed', action='store', type=str, help='gridType: observed or expected')
args = parser.parse_args()
input_file = args.input
grid_type = args.gridType

f = ROOT.TFile(input_file)
f.ls()

drawTheorySysts = True

plot = contourPlotter.contourPlotter("contourPlotterExample",800,600)

plot.processLabel = "#tilde{#chi}_{1}^{+}#tilde{#chi}_{1}^{-} #rightarrow W^{+}#tilde{#chi}_{1}^{0}W^{-}#tilde{#chi}_{1}^{0}"
plot.lumiLabel = "#sqrt{s}=13 TeV, 139 fb^{-1}"
plot.myLabel = "#it{#Deltam(#tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0}) = 100 GeV}"

## Axes

plot.drawAxes( [0,0.3,150,20] )
'''
## Other limits to draw
inFile_13TeV = ROOT.TFile.Open("/users2/ballaben/c1c1-ww-hf/utils/Contour/ContourATLAS13TeV.root" ,"READ")
mygraph = inFile_13TeV.Get("Graph1D_y1")
plot.drawShadedRegion( mygraph, color=ROOT.kRed, alpha=0.1, title="ATLAS 13 TeV Observed limit (arXiv:1908.08215)" )


inFile_8TeV = ROOT.TFile.Open("/users2/ballaben/c1c1-ww-hf/utils/Contour/ContourATLAS8TeV.root" ,"READ")
mygraph8Tev = inFile_8TeV.Get("Graph1D_y1")
plot.drawShadedRegion( mygraph8Tev, color=ROOT.kGray+2, alpha=0.1, title="ATLAS 8 TeV Observed limit (arXiv:1403.5294)" )


inFile_LEP = ROOT.TFile.Open("/users2/ballaben/c1c1-ww-hf/utils/Contour/ContourLEP208GeV.root" ,"READ")
mygraphLEP = inFile_LEP.Get("Graph1D_y1")
plot.drawShadedRegion( mygraphLEP, color=ROOT.kOrange+2, alpha=0.1, title="Combined LEP up to 208 GeV Observed Limit" )

plot.drawLine(coordinates= [80.35, 0., 250, 169.65], label='m(#tilde{#chi}_{1}^{#pm}) - m(#tilde{#chi}_{1}^{0}) = m(W)', labelLocation=[200, 125], angle=40)
plot.drawLine(coordinates= [50.,  50., 200, 200.], label='m(#tilde{#chi}_{1}^{#pm}) < m(#tilde{#chi}_{1}^{0})', labelLocation=[165, 170], angle=40)
## Main Result
if grid_type=='observed':
    plot.drawTextFromTGraph2D( f.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")
elif grid_type=='expected':
    plot.drawTextFromTGraph2D( f.Get("CLsexp_gr")  , angle=30 , title = "Grey Numbers Represent Expected CLs Value")

'''

plot.drawLine(coordinates= [0., 1., 150, 1.], label='', color=ROOT.kBlue, labelLocation=[200, 125], angle=0)

plot.drawOneSigmaBand(  f.Get("Band_1s_0")   )
plot.drawExpected(      f.Get("Exp_0")       )
plot.drawObserved(      f.Get("Obs_0"), title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed Limit")

## Draw Lines
# plot.drawLine(  coordinates = [0,0,800,800], label = "Kinematically Forbidden or blah", style = 7, angle = 30 )

## Axis Labels

plot.setXAxisLabel( "m(#tilde{#chi}_{1}^{0}) [GeV]" )
plot.setYAxisLabel( "95% CL Limit on #sigma/#sigma_{SUSY}"  )
plot.setYAxisLog()
plot.createLegend(shape=(0.22,0.65,0.55,0.79) ).Draw()

if drawTheorySysts:
	plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up") )
	plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down") )
	# coordinate in NDC
	plot.drawTheoryLegendLines( xyCoord=(0.234,0.672), length=0.057 )

plot.decorateCanvas( )
if grid_type=='observed':
    plot.writePlot("observedGridType.pdf" )
if grid_type=='expected':
    plot.writePlot("expectedGridType.pdf" )
