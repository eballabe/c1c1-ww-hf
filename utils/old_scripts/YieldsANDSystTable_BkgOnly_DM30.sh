#!/bin/bash

#Yields Table
YieldsTable.py -s MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM30_CR_SYST.txt -c CR_Dib_DF1J_DM30,CR_Dib_SF1J_DM30,CR_Top_DF_DM30,CR_Top_SF_DM30 -C "-" -L "-"

YieldsTable.py -s MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM30_VR_SYST.txt -c VR_Dib_DF1J_DM30,VR_Dib_SF1J_DM30,VR_Top_DF_DM30,VR_Top_SF_DM30 -C "-" -L "-"

YieldsTable.py -s MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM30_SR1_SYST.txt -c SR_DF1J_0_DM30,SR_DF1J_1_DM30,SR_DF1J_2_DM30,SR_DF1J_3_DM30,SR_DF1J_4_DM30 -C "-" -L "-"

YieldsTable.py -s MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM30_SR2_SYST.txt -c SR_DF1J_5_DM30,SR_DF1J_6_DM30,SR_DF1J_7_DM30,SR_DF1J_8_DM30 -C "-" -L "-"

YieldsTable.py -s MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM30_SR3_SYST.txt -c SR_SF1J_0_DM30,SR_SF1J_1_DM30,SR_SF1J_2_DM30,SR_SF1J_3_DM30,SR_SF1J_4_DM30 -C "-" -L "-"

YieldsTable.py -s MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM30_SR4_SYST.txt -c SR_SF1J_5_DM30,SR_SF1J_6_DM30,SR_SF1J_7_DM30,SR_SF1J_8_DM30 -C "-" -L "-"

cat YieldsTable_DM30_CR_* YieldsTable_DM30_VR_* YieldsTable_DM30_SR* > YieldsTable_DM30.txt
rm -rf YieldsTable_DM30_*


#Before fit
SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM30_CR_SYST_BEFORE.txt -c CR_Dib_DF1J_DM30,CR_Dib_SF1J_DM30,CR_Top_DF_DM30,CR_Top_SF_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM30_VR_SYST_BEFORE.txt -c VR_Dib_DF1J_DM30,VR_Dib_SF1J_DM30,VR_Top_DF_DM30,VR_Top_SF_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM30_SR1_SYST_BEFORE.txt -c SR_DF1J_0_DM30,SR_DF1J_1_DM30,SR_DF1J_2_DM30,SR_DF1J_3_DM30,SR_DF1J_4_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM30_SR2_SYST_BEFORE.txt -c SR_DF1J_5_DM30,SR_DF1J_6_DM30,SR_DF1J_7_DM30,SR_DF1J_8_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM30_SR3_SYST_BEFORE.txt -c SR_SF1J_0_DM30,SR_SF1J_1_DM30,SR_SF1J_2_DM30,SR_SF1J_3_DM30,SR_SF1J_4_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM30_SR4_SYST_BEFORE.txt -c SR_SF1J_5_DM30,SR_SF1J_6_DM30,SR_SF1J_7_DM30,SR_SF1J_8_DM30 -%

cat SystTable_DM30_CR_*BEFORE* SystTable_DM30_VR_*BEFORE* SystTable_DM30_SR*BEFORE* > SystTable-before_DM30.txt
rm -rf SystTable_DM30_*BEFORE*




#After fit
SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM30_CR_SYST_AFTER.txt -c CR_Dib_DF1J_DM30,CR_Dib_SF1J_DM30,CR_Top_DF_DM30,CR_Top_SF_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM30_VR_SYST_AFTER.txt -c VR_Dib_DF1J_DM30,VR_Dib_SF1J_DM30,VR_Top_DF_DM30,VR_Top_SF_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM30_SR1_SYST_AFTER.txt -c SR_DF1J_0_DM30,SR_DF1J_1_DM30,SR_DF1J_2_DM30,SR_DF1J_3_DM30,SR_DF1J_4_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM30_SR2_SYST_AFTER.txt -c SR_DF1J_5_DM30,SR_DF1J_6_DM30,SR_DF1J_7_DM30,SR_DF1J_8_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM30_SR3_SYST_AFTER.txt -c SR_SF1J_0_DM30,SR_SF1J_1_DM30,SR_SF1J_2_DM30,SR_SF1J_3_DM30,SR_SF1J_4_DM30 -%

SysTable.py -w ../results/BkgOnly_withSyst_0_30/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM30_SR4_SYST_AFTER.txt -c SR_SF1J_5_DM30,SR_SF1J_6_DM30,SR_SF1J_7_DM30,SR_SF1J_8_DM30 -%


cat SystTable_DM30_CR_*AFTER* SystTable_DM30_VR_*AFTER* SystTable_DM30_SR*AFTER* > SystTable-after_DM30.txt
rm -rf SystTable_DM30_*AFTER*
