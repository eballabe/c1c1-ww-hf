#!/bin/bash

samples="100_80"
for sample in $samples; do
    YieldsTable.py -s C1C1_$sample,MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/Exclusion_withSyst_$sample\/C1C1_$sample\_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_$sample\_CR_SYST.txt -c CR_Dib_DF1J_DM30,CR_Dib_SF1J_DM30,CR_Top_DF_DM30,CR_Top_SF_DM30 -C "-" -L "-"

    YieldsTable.py -s C1C1_$sample,MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/Exclusion_withSyst_$sample\/C1C1_$sample\_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_$sample\_VR_SYST.txt -c VR_Dib_DF1J_DM30,VR_Dib_SF1J_DM30,VR_Top_DF_DM30,VR_Top_SF_DM30 -C "-" -L "-"

    YieldsTable.py -s C1C1_$sample,MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/Exclusion_withSyst_$sample\/C1C1_$sample\_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_$sample\_SR1_SYST.txt -c SR_DF1J_0_DM30,SR_DF1J_1_DM30,SR_DF1J_2_DM30,SR_DF1J_3_DM30,SR_DF1J_4_DM30 -C "-" -L "-"

    YieldsTable.py -s C1C1_$sample,MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/Exclusion_withSyst_$sample\/C1C1_$sample\_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_$sample\_SR2_SYST.txt -c SR_DF1J_5_DM30,SR_DF1J_6_DM30,SR_DF1J_7_DM30,SR_DF1J_8_DM30 "-" -L "-"

    YieldsTable.py -s C1C1_$sample,MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/Exclusion_withSyst_$sample\/C1C1_$sample\_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_$sample\_SR3_SYST.txt -c SR_SF1J_0_DM30,SR_SF1J_1_DM30,SR_SF1J_2_DM30,SR_SF1J_3_DM30,SR_SF1J_4_DM30 -C "-" -L "-"

    YieldsTable.py -s C1C1_$sample,MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/Exclusion_withSyst_$sample\/C1C1_$sample\_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_$sample\_SR4_SYST.txt -c SR_SF1J_5_DM30,SR_SF1J_6_DM30,SR_SF1J_7_DM30,SR_SF1J_8_DM30 -C "-" -L "-"

    cat YieldsTable_$sample\_CR* YieldsTable_$sample\_VR* YieldsTable_$sample\_SR*> YieldsTable_$sample.txt
    rm -rf YieldsTable_$sample\_*
done
