#Tables DM100 Exclusion
#!/bin/bash
YieldsTable.py -s FNP,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM100_CRVR_SYST.txt -c CR_VV_DM100,CR_Top_DF_DM100,VR_VV_DM100,VR_Top_DF_DM100 -C "-" -L "-"

YieldsTable.py -s FNP,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM100_SR1_SYST.txt -c SR-DF0J-81-8125,SR-DF0J-8125-815,SR-DF0J-815-8175,SR-DF0J-8175-82,SR-DF0J-82-8225,SR-DF0J-8225-825 -C "-" -L "-"

YieldsTable.py -s FNP,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM100_SR2_SYST.txt -c SR-DF0J-825-8275,SR-DF0J-8275-83,SR-DF0J-83-8325,SR-DF0J-8325-8335,SR-DF0J-8335-8375,SR-DF0J-8375-84 -C "-" -L "-"

YieldsTable.py -s FNP,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM100_SR3_SYST.txt -c SR-DF0J-84-845,SR-DF0J-845-85,SR-DF0J-85-86,SR-DF0J-86-87,SR-DF0J-87 -C "-" -L "-"

YieldsTable.py -s FNP,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM100_SR4_SYST.txt -c SR-SF0J-77-7725,SR-SF0J-7725-775,SR-SF0J-775-7775,SR-SF0J-7775-778','SR-SF0J-778-7785,SR-SF0J-7785-779,SR-SF0J-779-7795 -C "-" -L "-"

YieldsTable.py -s FNP,VV,ttbar,Wt,Zjets,Zttjets,VVV,other -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o YieldsTable_DM100_SR5_SYST.txt -c SR-SF0J-7795-78,SR-SF0J-78-785,SR-SF0J-785-79,SR-SF0J-79-795,SR-SF0J-795-80,SR-SF0J-80-81,SR-SF0J-81 -C "-" -L "-"

cat YieldsTable_DM100_CRVR* YieldsTable_DM100_SR*> YieldsTable_DM100.txt
rm -rf YieldsTable_DM100_*

#Systematics Exclusion fit
#Before fit

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM100_CRVR_SYST_BEFORE.txt -c CR_VV_DM100,CR_Top_DF_DM100,VR_VV_DM100,VR_Top_DF_DM100 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM100_SR1_SYST_BEFORE.txt -c SR-DF0J-81-8125,SR-DF0J-8125-815,SR-DF0J-815-8175,SR-DF0J-8175-82,SR-DF0J-82-8225,SR-DF0J-8225-825 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM100_SR2_SYST_BEFORE.txt -c SR-DF0J-825-8275,SR-DF0J-8275-83,SR-DF0J-83-8325,SR-DF0J-8325-8335,SR-DF0J-8335-8375,SR-DF0J-8375-84 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM100_SR3_SYST_BEFORE.txt -c SR-DF0J-84-845,SR-DF0J-845-85,SR-DF0J-85-86,SR-DF0J-86-87,SR-DF0J-87 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM100_SR4_SYST_BEFORE.txt -c SR-SF0J-77-7725,SR-SF0J-7725-775,SR-SF0J-775-7775,SR-SF0J-7775-778','SR-SF0J-778-7785,SR-SF0J-7785-779,SR-SF0J-779-7795 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -b -o SystTable_DM100_SR5_SYST_BEFORE.txt -c SR-SF0J-7795-78,SR-SF0J-78-785,SR-SF0J-785-79,SR-SF0J-79-795,SR-SF0J-795-80,SR-SF0J-80-81,SR-SF0J-81 -%


cat SystTable_DM100_CRVR*BEFORE* SystTable_DM100_SR*BEFORE* > SystTable-before_DM100.txt
rm -rf SystTable_DM100_*BEFORE*




#After fit

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM100_CRVR_SYST_AFTER.txt -c CR_VV_DM100,CR_Top_DF_DM100,VR_VV_DM100,VR_Top_DF_DM100 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM100_SR1_SYST_AFTER.txt -c SR-DF0J-81-8125,SR-DF0J-8125-815,SR-DF0J-815-8175,SR-DF0J-8175-82,SR-DF0J-82-8225,SR-DF0J-8225-825 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM100_SR2_SYST_AFTER.txt -c SR-DF0J-825-8275,SR-DF0J-8275-83,SR-DF0J-83-8325,SR-DF0J-8325-8335,SR-DF0J-8335-8375,SR-DF0J-8375-84 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM100_SR3_SYST_AFTER.txt -c SR-DF0J-84-845,SR-DF0J-845-85,SR-DF0J-85-86,SR-DF0J-86-87,SR-DF0J-87 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM100_SR4_SYST_AFTER.txt -c SR-SF0J-77-7725,SR-SF0J-7725-775,SR-SF0J-775-7775,SR-SF0J-7775-778','SR-SF0J-778-7785,SR-SF0J-7785-779,SR-SF0J-779-7795 -%

SysTable.py -w ../results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root -o SystTable_DM100_SR5_SYST_AFTER.txt -c SR-SF0J-7795-78,SR-SF0J-78-785,SR-SF0J-785-79,SR-SF0J-79-795,SR-SF0J-795-80,SR-SF0J-80-81,SR-SF0J-81 -%

cat SystTable_DM100_CRVR*AFTER* SystTable_DM100_*AFTER* > SystTable-after_DM100.txt
rm -rf SystTable_DM100_*AFTER*
