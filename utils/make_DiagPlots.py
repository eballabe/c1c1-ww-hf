import os
import argparse

parser = argparse.ArgumentParser(description='Remove failed fits :( sliced in DM')
parser.add_argument('--Scenario', help='Scenario')
args = parser.parse_args()

Scenario = args.Scenario

str1 = 'Making latex code for diagnostic plots for {}'.format(Scenario)
print(str1)

if Scenario == 'LessCompressed':
    regions = ['CR_Dib','CR_Top','VR_Dib_DF0J','VR_Dib_SF0J','VR_Top_DF1J','VR_Top_SF1J','VR_Top_DF0J','VR_Top_SF0J','SR_DF0J_81_8125','SR_DF0J_8125_815','SR_DF0J_815_8175','SR_DF0J_8175_82','SR_DF0J_82_8225','SR_DF0J_8225_825','SR_DF0J_825_8275','SR_DF0J_8275_83',
'SR_DF0J_83_8325','SR_DF0J_8325_835','SR_DF0J_835_8375','SR_DF0J_8375_84','SR_DF0J_84_845','SR_DF0J_845_85','SR_DF0J_85_86','SR_DF0J_86',
'SR_SF0J_77_775','SR_SF0J_775_78','SR_SF0J_78_785','SR_SF0J_785_79','SR_SF0J_79_795','SR_SF0J_795_80','SR_SF0J_80_81','SR_SF0J_81']
    backgrounds = ["VV","ttbar","Wt","Zjets","Zttjets","VVV","other","FNP"]

    f = open("DiagnosticPlots.tex" , "w")
    f.write("\documentclass[a4paper,4pt]{book} \n\usepackage[margin=1cm]{geometry} \n\usepackage[utf8]{inputenc} \n\usepackage{adjustbox}  \n\usepackage{booktabs} \n\usepackage{lscape} \n\usepackage{placeins} \n\\begin{document} \n\\begin{chapter}*{Systematic Plots}")
    for region in regions:
        f.write("\n\\begin{minipage}[c]{\\textwidth} \n")
        for background in backgrounds:
            plot = "{}_AllSyst_{}.eps".format(background, region)
            f.write("\includegraphics[width=0.3\\textwidth]")
            f.write("{plots/"+plot+"} \n")
        f.write("\\end{minipage} \n")
    f.write("\\end{chapter} \n\\end{document}")
    f.close()
    os.system("pdflatex DiagnosticPlots.tex DiagnosticPlots.pdf")
            #f.write("\\ \includegraphics[width=1\textwidth]\{plots/" + plot " /n}")

if Scenario == 'MoreCompressed':
    regions = ["CR_Dib_DF1J_DM30","CR_Dib_SF1J_DM30","CR_Top_DF_DM30","CR_Top_SF_DM30","VR_Dib_DF1J_DM30","VR_Dib_SF1J_DM30","VR_Top_DF_DM30","VR_Top_SF_DM30","SR_DF1J_0_DM30","SR_DF1J_1_DM30","SR_DF1J_2_DM30","SR_DF1J_3_DM30","SR_DF1J_4_DM30","SR_DF1J_5_DM30","SR_DF1J_6_DM30","SR_DF1J_7_DM30","SR_DF1J_8_DM30","SR_SF1J_0_DM30","SR_SF1J_1_DM30","SR_SF1J_2_DM30","SR_SF1J_3_DM30","SR_SF1J_4_DM30","SR_SF1J_5_DM30","SR_SF1J_6_DM30","SR_SF1J_7_DM30","SR_SF1J_8_DM30"]
    #backgrounds = ["MCFakes","VV","ttbar","Wt","Zjets","Zttjets","VVV","other"]
    backgrounds = ["VV","ttbar","Wt","Zjets","Zttjets","VVV","other"]

    f = open("DiagnosticPlots_MoreCompressed.tex" , "w")
    for region in regions:
        for background in backgrounds:
            plot = "{}_AllSyst_{}.eps".format(background, region)
            f.write("\includegraphics[width=1\\textwidth]")
            f.write("{plots/"+plot+"} \n")

            #f.write("\\ \includegraphics[width=1\textwidth]\{plots/" + plot " /n}")


if Scenario == 'LessCompressed-note':
    regions = ['CR_Dib','CR_Top','VR_Dib_DF0J','VR_Dib_SF0J','VR_Top_DF1J','VR_Top_SF1J','VR_Top_DF0J','VR_Top_SF0J','SR_DF0J_81_8125','SR_DF0J_8125_815','SR_DF0J_815_8175','SR_DF0J_8175_82','SR_DF0J_82_8225','SR_DF0J_8225_825','SR_DF0J_825_8275','SR_DF0J_8275_83',
'SR_DF0J_83_8325','SR_DF0J_8325_835','SR_DF0J_835_8375','SR_DF0J_8375_84','SR_DF0J_84_845','SR_DF0J_845_85','SR_DF0J_85_86','SR_DF0J_86',
'SR_SF0J_77_775','SR_SF0J_775_78','SR_SF0J_78_785','SR_SF0J_785_79','SR_SF0J_79_795','SR_SF0J_795_80','SR_SF0J_80_81','SR_SF0J_81']
    backgrounds = ["VV","ttbar","Wt","Zjets","Zttjets","VVV","other","FNP"]

    f = open("DiagnosticPlots.tex" , "w")
    f.write("\documentclass[a4paper,4pt]{book} \n\usepackage[margin=1cm]{geometry} \n\usepackage[utf8]{inputenc} \n\usepackage{adjustbox}  \n\usepackage{booktabs} \n\usepackage{lscape} \n\usepackage{placeins} \n\\begin{document} \n\\begin{chapter}*{Systematic Plots}")
    for region in regions:
        f.write("\n\\begin{minipage}[c]{\\textwidth} \n")
        for background in backgrounds:
            plot = "{}_AllSyst_{}-eps-converted-to.pdf".format(background, region)
            f.write("\includegraphics[width=0.3\\textwidth]")
            f.write("{Eric/SystematicDiagnosticPlots/Background/"+plot+"} \n")
        f.write("\\end{minipage} \n")
    f.write("\\end{chapter} \n\\end{document}")
    f.close()
    
