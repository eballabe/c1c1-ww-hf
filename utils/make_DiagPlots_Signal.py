import os
import argparse

parser = argparse.ArgumentParser(description='Remove failed fits :( sliced in DM')
parser.add_argument('-a','--action', help='Action')
args = parser.parse_args()

action = args.action


samples=['100_10','100_20','100_30','100_40',
'125_25','125_35','125_45','125_55','125_65',
'150_25','150_50','150_60','150_70','150_80', 
'175_50','175_75','175_85','175_95',
'200_75','200_100',   
'225_100','225_125',
'250_125','250_150']
samples+=['100_1','125_1','150_1','175_1','200_1','175_25','200_25','200_50']

if action == 'MakePlots':
    for sample in samples:
        os.system("HistFitter.py -D systematics  -F excl -u\"--doExclusion --point {} --doSyst\" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_Systematic_{} &".format(sample, sample))


if action == 'MakeLatex':
    regions = ['CR_Dib','CR_Top','SR_DF0J_81_8125','SR_DF0J_8125_815','SR_DF0J_815_8175','SR_DF0J_8175_82','SR_DF0J_82_8225','SR_DF0J_8225_825','SR_DF0J_825_8275','SR_DF0J_8275_83',
'SR_DF0J_83_8325','SR_DF0J_8325_835','SR_DF0J_835_8375','SR_DF0J_8375_84','SR_DF0J_84_845','SR_DF0J_845_85','SR_DF0J_85_86','SR_DF0J_86',
'SR_SF0J_77_775','SR_SF0J_775_78','SR_SF0J_78_785','SR_SF0J_785_79','SR_SF0J_79_795','SR_SF0J_795_80','SR_SF0J_80_81','SR_SF0J_81']

    f = open("DiagnosticPlots.tex" , "w")
    f.write("\documentclass[a4paper,4pt]{book} \n\usepackage[margin=1cm]{geometry} \n\usepackage[utf8]{inputenc} \n\usepackage{adjustbox}  \n\usepackage{booktabs} \n\usepackage{lscape} \n\usepackage{placeins} \n\\begin{document} \n\\begin{chapter}*{Systematic Plots}")
    for sample in samples:
        f.write("\n\\begin{minipage}[c]{\\textwidth} \n")
        for region in regions:
            plot = "C1C1_{}_AllSyst_{}.eps".format(sample, region)
            f.write("\includegraphics[width=0.3\\textwidth]")
            f.write("{plots/"+plot+"} \n")
        f.write("\\end{minipage} \n")
    f.write("\\end{chapter} \n\\end{document}")
    f.close()
    os.system("pdflatex DiagnosticPlots.tex DiagnosticPlots.pdf")


if action == 'MakeLatex-note':
    regions = ['CR_Dib','CR_Top','SR_DF0J_81_8125','SR_DF0J_8125_815','SR_DF0J_815_8175','SR_DF0J_8175_82','SR_DF0J_82_8225','SR_DF0J_8225_825','SR_DF0J_825_8275','SR_DF0J_8275_83',
'SR_DF0J_83_8325','SR_DF0J_8325_835','SR_DF0J_835_8375','SR_DF0J_8375_84','SR_DF0J_84_845','SR_DF0J_845_85','SR_DF0J_85_86','SR_DF0J_86',
'SR_SF0J_77_775','SR_SF0J_775_78','SR_SF0J_78_785','SR_SF0J_785_79','SR_SF0J_79_795','SR_SF0J_795_80','SR_SF0J_80_81','SR_SF0J_81']

    f = open("DiagnosticPlots.tex" , "w")
    f.write("\documentclass[a4paper,4pt]{book} \n\usepackage[margin=1cm]{geometry} \n\usepackage[utf8]{inputenc} \n\usepackage{adjustbox}  \n\usepackage{booktabs} \n\usepackage{lscape} \n\usepackage{placeins} \n\\begin{document} \n\\begin{chapter}*{Systematic Plots}")
    for sample in samples:
        f.write("\n\\begin{minipage}[c]{\\textwidth} \n")
        for region in regions:
            plot = "C1C1_{}_AllSyst_{}-eps-converted-to.pdf".format(sample, region)
            f.write("\includegraphics[width=0.3\\textwidth]")
            f.write("{Eric/SystematicDiagnosticPlots/Signal/"+plot+"} \n")
        f.write("\\end{minipage} \n")
    f.write("\\end{chapter} \n\\end{document}")
    f.close()

    
