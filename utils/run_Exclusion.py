import os
import argparse

parser = argparse.ArgumentParser(description='Exclusion Fit with no Backup Cache File')
parser.add_argument('--point', type=str, help='C1C1-WW point')
args = parser.parse_args()

point = args.point

if os.path.isdir("log_exclusion") == False:
    os.system("mkdir log_exclusion")

cmd = "nohup HistFitter.py -twxf -F excl -u\"--doExclusion --doSyst --point {}\" /users2/ballaben/c1c1-ww-hf/C1C1-WW-fit.py > log_exclusion/log_Exclusion_withSyst_{} &".format(point,point)

print(cmd)
os.system(cmd)
