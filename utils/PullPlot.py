"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils
from pullPlotUtils import makePullPlot 

PlotsForPaper = True

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["CR_Dib"] = "CR-VV"
    myRegionDict["CR_Top"] = "CR-top"
    myRegionDict["VR_Dib_DF0J"] = "VR-VV-DF"
    myRegionDict["VR_Dib_SF0J"] = "VR-VV-SF"
    myRegionDict["VR_Top_DF1J"] = "VR-top-DF"
    myRegionDict["VR_Top_SF1J"] = "VR-top-SF"
    myRegionDict["VR_Top_DF0J"] = "VR-top0J-DF"
    myRegionDict["VR_Top_SF0J"] = "VR-top0J-SF"

    myRegionDict["SR_DF0J_81_8125"]   = "[0.81, 0.8125]"
    myRegionDict["SR_DF0J_8125_815"] = "[0.8125, 0.815]"
    myRegionDict["SR_DF0J_815_8175"] = "[0.815, 0.8175]"
    myRegionDict["SR_DF0J_8175_82"]   = "[0.8175, 0.82]"
    myRegionDict["SR_DF0J_82_8225"]   = "[0.82, 0.8225]"
    myRegionDict["SR_DF0J_8225_825"] = "[0.8225, 0.825]"
    myRegionDict["SR_DF0J_825_8275"] = "[0.825, 0.8275]"
    myRegionDict["SR_DF0J_8275_83"]   = "[0.8275, 0.83]"
    myRegionDict["SR_DF0J_83_8325"]   = "[0.83, 0.8325]"
    myRegionDict["SR_DF0J_8325_835"] = "[0.8325, 0.835]"
    myRegionDict["SR_DF0J_835_8375"] = "[0.835, 0.8375]"
    myRegionDict["SR_DF0J_8375_84"]   = "[0.8375, 0.84]"
    myRegionDict["SR_DF0J_84_845"]     = "[0.84, 0.845]"
    myRegionDict["SR_DF0J_845_85"]     = "[0.845, 0.85]"
    myRegionDict["SR_DF0J_85_86"]       = "[0.85, 0.86]"
    myRegionDict["SR_DF0J_86"]            = "[0.86, 1)"

    myRegionDict["SR_SF0J_77_775"]    = "[0.77, 0.775]"
    myRegionDict["SR_SF0J_775_78"]    = "[0.775, 0.78]"
    myRegionDict["SR_SF0J_78_785"]    = "[0.78, 0.785]"
    myRegionDict["SR_SF0J_785_79"]    = "[0.785, 0.79]"
    myRegionDict["SR_SF0J_79_795"]    = "[0.79, 0.795]"
    myRegionDict["SR_SF0J_795_80"]    = "[0.795, 0.80]"
    myRegionDict["SR_SF0J_80_81"]      = "[0.80, 0.81]"
    myRegionDict["SR_SF0J_81"]            = "[0.81, 1)"

    myRegionDict["ttbar"] = "t#bar{t}"
    myRegionDict["Zjets"] = "Z(ee/#mu#mu)+jets"
    myRegionDict["Zttjets"] = "Z(#tau#tau)+jets"

    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["CR_Dib","CR_Top","VR_Dib_DF0J","VR_Dib_SF0J","VR_Top_DF1J","VR_Top_SF1J","VR_Top_DF0J","VR_Top_SF0J"]
    regionList += ["SR_DF0J_81_8125","SR_DF0J_8125_815","SR_DF0J_815_8175","SR_DF0J_8175_82","SR_DF0J_82_8225","SR_DF0J_8225_825","SR_DF0J_825_8275","SR_DF0J_8275_83"]
    regionList +=["SR_DF0J_83_8325","SR_DF0J_8325_835","SR_DF0J_835_8375","SR_DF0J_8375_84","SR_DF0J_84_845","SR_DF0J_845_85","SR_DF0J_85_86","SR_DF0J_86"]
    regionList +=["SR_SF0J_77_775","SR_SF0J_775_78","SR_SF0J_78_785","SR_SF0J_785_79","SR_SF0J_79_795","SR_SF0J_795_80","SR_SF0J_80_81","SR_SF0J_81"]

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("SR") != -1: return kBlack

 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if sample == "FNP":        return TColor.GetColor('#dadaeb')
    if sample == "VV":         return TColor.GetColor('#fe9929')
    if sample == "ttbar":      return TColor.GetColor('#0868ac')
    if sample == "Wt":         return TColor.GetColor('#4e97fdfc')

    if PlotsForPaper == True:
        if sample == "Zjets":      return TColor.GetColor('#41ab5d')
        if sample == "Zttjets":    return TColor.GetColor('#41ab5d')
        if sample == "VVV":        return TColor.GetColor('#41ab5d')
        if sample == "other":      return TColor.GetColor('#41ab5d')
    elif PlotsForPaper == False:
        if sample == "Zjets":      return TColor.GetColor('#41ab5d')
        if sample == "Zttjets":    return TColor.GetColor('#c7e9c0')
        if sample == "VVV":        return TColor.GetColor('#fec49f')
        if sample == "other":      return TColor.GetColor('#9ecae1')
    else:
        print "cannot find color for sample (",sample,")"
    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils.getRegionColor = getRegionColor
    pullPlotUtils.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = "/users2/ballaben/c1c1-ww-hf/unblinded_results/BkgOnlyFit/results/BkgOnly_withSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root" # 

    # Where's the pickle file?
    pickleFilename = "/users2/ballaben/c1c1-ww-hf/unblinded_results/BkgOnlyFit/pullplot/MyYieldsTable.pickle"
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = ""

    # Samples to stack on top of eachother in each region
    samples = "VV,ttbar,Wt,FNP,Zjets,Zttjets,VVV,other"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    if not os.path.exists(pickleFilename):
        print "pickle filename %s does not exist" % pickleFilename
        print "will proceed to run yieldstable again"
        
        # Run YieldsTable.py with all regions and samples requested
        cmd = "YieldsTable.py -c %s -s %s -w %s -o MyYieldsTable.tex" % (",".join(regionList), samples, wsfilename)
        print cmd
        subprocess.call(cmd, shell=True)

    if not os.path.exists(pickleFilename):
        print "pickle filename %s still does not exist" % pickleFilename
        return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind, plotSignificance="atlas")

if __name__ == "__main__":
    main()
