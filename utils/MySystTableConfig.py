namemap = {
'Statistical' : ['gamma_stat_SRD_DF0J_81_SF0J_77_cuts_bin_0', 'Lumi'],
#'Statistical' : ['gamma_stat_SRD_DF0J_81_cuts_bin_0', 'Lumi'],
#'Statistical' : ['gamma_stat_SRD_SF0J_77_cuts_bin_0', 'Lumi'],
'mu_VV' : ['mu_VV'],
'mu_top' : ['mu_top'],
'VV_theory' : ['alpha_CKKW_VV','alpha_Factorization_VV','alpha_Resummation_VV','alpha_Renormalization_VV','alpha_RenormalizationFactorization_VV','alpha_PDF_VV'],
'ttbar_theory' : ['alpha_Factorization_ttbar','alpha_Generator_ttbar','alpha_Radiation_ttbar','alpha_Renormalization_ttbar','alpha_Shower_ttbar'],
'Wt_theory' : ['alpha_DS_Singletop'],
'top_theory' : ['alpha_Factorization_ttbar','alpha_Generator_ttbar','alpha_Radiation_ttbar','alpha_Renormalization_ttbar','alpha_Shower_ttbar', 'alpha_DS_Singletop'],
'Zjets_theory' : ['alpha_CKKW_Zjets','alpha_Factorization_Zjets','alpha_Resummation_Zjets','alpha_Renormalization_Zjets'],
'MET_Soft-Terms' : ['alpha_MET_SoftTrk_ResoPara','alpha_MET_SoftTrk_ResoPerp', 'alpha_MET_SoftTrk_Scale'],
'JER' : ['alpha_JER_DataVsMC','alpha_JER1', 'alpha_JER2', 'alpha_JER3', 'alpha_JER4', 'alpha_JER5', 'alpha_JER6', 'alpha_JER7'],
'JES' : ['alpha_JET_EtaIntercalibration_highE','alpha_JET_EtaIntercalibration_negEta', 'alpha_JET_EtaIntercalibration_posEta', 'alpha_JET_Flavor_Response','alpha_JET_Flavor_Composition','alpha_JET_Pileup_OffsetMu','alpha_JET_Pileup_OffsetNPV','alpha_JET_Pileup_PtTerm','alpha_JET_Pileup_RhoTopology','alpha_JET_PunchThrough_MC16','alpha_JET_EffectiveNP_Detector1','alpha_JET_EffectiveNP_Detector2','alpha_JET_EffectiveNP_Mixed1','alpha_JET_EffectiveNP_Mixed2','alpha_JET_EffectiveNP_Mixed3','alpha_JVT',
'alpha_JET_EffectiveNP_Modelling1','alpha_JET_EffectiveNP_Modelling2','alpha_JET_EffectiveNP_Modelling3','alpha_JET_EffectiveNP_Modelling4',
'alpha_JET_EffectiveNP_Statistical1','alpha_JET_EffectiveNP_Statistical2','alpha_JET_EffectiveNP_Statistical3','alpha_JET_EffectiveNP_Statistical4','alpha_JET_EffectiveNP_Statistical5','alpha_JET_EffectiveNP_Statistical6'],
'b-tagging' : ['alpha_FT_EFF_Light','alpha_FT_EFF_B','alpha_FT_EFF_C','alpha_FT_EFF_extrFromCharm','alpha_FT_EFF_extrapolation'],
'PILEUP' : ['alpha_PILEUP'],
'FNP' : ['alpha_FNP_STAT','alpha_FNP_WEIGHT','alpha_FNP_XSEC','alpha_FNP_EFF_TOTAL','alpha_FNP_EFF_TRIG_TOTAL','alpha_FNP_WEIGHTSYST'],
'Lepton_Calibration' : ['alpha_EG_RES','alpha_EG_SCALE','alpha_EL_EFF_ID','alpha_EL_EFF_Reco','alpha_EL_EFF_Iso','alpha_EL_EFF_Triggereff','alpha_EL_EFF_Charge','alpha_EL_EFF_Trigger','alpha_MUON_ID','alpha_MUON_EFF_ISO_STAT','alpha_MUON_SCALE','alpha_MUON_MS','alpha_MUON_EFF_ISO_SYS','alpha_MUON_EFF_RECO_SYS','alpha_MUON_EFF_RECO_SYS_LOWPT','alpha_MUON_EFF_RECO_STAT','alpha_MUON_EFF_RECO_STAT_LOWPT','alpha_MUON_EFF_TTVA_STAT','alpha_MUON_SAGITTA_RESBIAS','alpha_MUON_EFF_TTVA_SYS','alpha_MUON_EFF_TrigStatUncertainty','alpha_MUON_SAGITTA_RHO','alpha_MUON_EFF_TrigSystUncertainty']
}


# cd SRD_DF0J_81_SF0J_77
# SysTable.py -c SRD_DF0J_81_SF0J_77 -w results/Discovery_withSyst_0/Discovery_SRD_DF0J_81_SF0J_77_combined_NormalMeasurement_model_afterFit.root -o SystTable_AFTER_Reduced.tex -% /users2/ballaben/c1c1-ww-hf/utils/MySystTableConfig.py

# cd SRD_DF0J_81
# SysTable.py -c SRD_DF0J_81 -w results/Discovery_withSyst_0/Discovery_SRD_DF0J_81_combined_NormalMeasurement_model_afterFit.root -o SystTable_AFTER_Reduced.tex -% /users2/ballaben/c1c1-ww-hf/utils/MySystTableConfig.py

# cd SRD_SF0J_77
# SysTable.py -c SRD_SF0J_77 -w results/Discovery_withSyst_0/Discovery_SRD_SF0J_77_combined_NormalMeasurement_model_afterFit.root -o SystTable_AFTER_Reduced.tex -% /users2/ballaben/c1c1-ww-hf/utils/MySystTableConfig.py
